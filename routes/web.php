<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@index');
Route::post('/dashboard/registrasiCheck', 'AsesorController@checkDuplicate')->name('instansi_duplicate.check');
Route::post('/dashboard/unitCheck','UnitKompetensiController@unitCheck');
//Statistik Export
Route::get('/dashboard/export-rekap-instansi','HomeController@RekapInstansi');
Route::get('/dashboard/export-skema-kompeten','HomeController@SkemaKompeten');
Route::get('/dashboard/export-skema-non-kompeten','HomeController@SkemaNonKompeten');
Route::get('/dashboard/export-asesi-non-kompeten','HomeController@AsesiNonKompeten');
Route::get('/dashboard/export-unit-kompeten','HomeController@UnitKompeten');
Route::get('/dashboard/export-unit-non-kompeten','HomeController@UnitNonKompeten');


Route::group(['middleware' => ['auth', 'admin']], function() {
    /**Master Data */
    Route::resource('/dashboard/kota','KotaController');
    Route::match(['put', 'patch'], '/dashboard/kota/store-update/{id}','KotaController@update');
    Route::resource('/dashboard/instansi','InstansiController');
    Route::resource('/dashboard/provinsi','ProvinsiController');
    Route::post('/dashboard/instansiCheck', 'InstansiController@checkDuplicate')->name('instansi_duplicate.check');
    Route::resource('/dashboard/kategori','KategoriKomponenController');
    Route::resource('/dashboard/kegiatan','KegiatanController');
    /* User */
    Route::resource('/dashboard/user','UserController');
    Route::post('/cekoldpassword','UserController@checkOldPassword');
});
Route::get('/dashboard/getkota/{id}', 'InstansiController@getKota');
Route::get('/dashboard/getAsesiBySkema/{id_skema}', 'HasilUjikomController@getAsesiBySkema');
Route::get('/dashboard/getUnitBySkema/{id_skema}', 'HasilUjikomController@getUnitBySkema');
Route::post('/dashboard/getUnitSurveilans','SurveilansController@getUnitSurveilans');

Route::group(['middleware' => ['auth', 'admin_jabatan']], function() {
    /* Asesi */
    Route::resource('/dashboard/asesi','AsesiController');
    Route::post('/dashboard/asesi/post', 'AsesiController@store');
    Route::resource('/dashboard/apl02','APL02Controller');
    Route::get('/dashboard/getAsesiSkemaAPL02/{id}','APL02Controller@getAsesiSkemaAPL02');
    Route::get('/dashboard/getSkemaUnitPertanyaan/{id}','APL02Controller@getSkemaUnitPertanyaan');
    /* Asesor */
    Route::resource('/dashboard/asesor','AsesorController');
    Route::post('/dashboard/asesor/post', 'AsesorController@store');
    Route::get('/dashboard/penugasan-asesor','PenugasanAsesorController@index');
    Route::get('/dashboard/penugasan-asesor/create/{id}','PenugasanAsesorController@create');
    Route::get('/dashboard/penugasan-asesor/show/{id}','PenugasanAsesorController@show');
    Route::post('/dashboard/penugasan-asesor/store','PenugasanAsesorController@store');
    Route::delete('/dashboard/penugasan-asesor/destroy/{id}','PenugasanAsesorController@destroy');
    
    /* Feedback */
    Route::resource('/dashboard/feedback','FeedbackController');
    Route::get('/dashboard/feedback/masukan/{id}','FeedbackController@create');
    Route::resource('/dashboard/komponen-feedback','KomponenFeedbackController');
    Route::get('/dashboard/feedback-asesi/{id}','FeedbackController@showAsesi');
    
    /* Ujikom */
    Route::resource('/dashboard/ujikom','UjikomController');
    Route::resource('/dashboard/unit-ujikom','UnitKompetensiController');
    
    Route::get('/dashboard/ujikom-hasil','HasilUjikomController@index');
    Route::get('/dashboard/ujikom-hasil/masukan/{id}','HasilUjikomController@create');
    Route::get('/dashboard/ujikom-hasil/detail/{id}','HasilUjikomController@show');
    Route::post('/dashboard/ujikom-hasil/post','HasilUjikomController@store');
   
    
    Route::resource('/dashboard/skema-ujikom','SkemaController');
    Route::post('/dashboard/insert-unit-skema', 'SkemaController@insertUnitSkema');
    Route::delete('/dashboard/destroy-unit-skema/{id}','SkemaController@destroyUnitSkema');
    
    Route::get('/dashboard/ujikom-skema-set/{id}','UjikomController@setUjikomSkema');
    Route::post('/dashboard/ujikom-skema-store/','UjikomController@storeUjikomSkema');
    Route::delete('/dashboard/ujikom-skema-destroy/{id}','UjikomController@destroyUjikomSkema');
    
    Route::get('/dashboard/ujikom-add-asesor/{id}','UjikomController@AddAsesor');
    Route::post('/dashboard/ujikom-add-asesor/post','UjikomController@storeAddAsesor');
    Route::get('/dashboard/ujikom-add-asesi/{id}','UjikomController@AddAsesi');
    Route::post('/dashboard/ujikom-add-asesi/post','UjikomController@storeAddAsesi');
    
    Route::get('/dashboard/unit-pertanyaan/{id}','PertanyaanUnitController@index');
    Route::post('/dashboard/unit-pertanyaan/store','PertanyaanUnitController@store');
    Route::delete('/dashboard/unit-pertanyaan/destroy/{id}','PertanyaanUnitController@destroy');

    Route::resource('/dashboard/ujikom-banding','BandingMAK02Controller');
    Route::resource('/dashboard/ujikom-persetujuan','PersetujuanController');
    Route::resource('/dashboard/asesmen-peninjauan','PeninjauanAsesmenController');
    Route::resource('/dashboard/asesmen-keputusan','KeputusanController');
    Route::resource('/dashboard/ceklis-kompetensi','CeklisMAK01Controller');
    /** Surveilans **/
    Route::resource('/dashboard/surveilans','SurveilansController');
});
Route::group(['middleware' => ['auth', 'admin_sertifikat']], function() {
    /** Sertifikat */
    Route::resource('/dashboard/sertifikat','SertifikatController');
    Route::resource('/dashboard/pengiriman-sertifikat','PengirimanSertifikatController');
    Route::get('/dashboard/tracking-sertifikat','TrackingSertifikatController@index');
    Route::get('/dashboard/tracking-sertifikat/result','TrackingSertifikatController@track');
    Route::get('/dashboard/sertifikat-export','SertifikatController@export');
    Route::get('/dashboard/sertifikat-export/{id}','SertifikatController@exportByUjikom');
    Route::get('/dashboard/get-tanggal/{id}','PengirimanSertifikatController@getTanggalSertifikat');

});

Route::get('/sendbasicemail','MailController@basic_email');
Route::get('/sendhtmlemail','MailController@html_email');
Route::get('/sendattachmentemail','MailController@attachment_email');

Auth::routes();

