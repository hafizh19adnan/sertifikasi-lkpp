<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    protected $table = 'instansi';
    public $timestamps = false;
    protected $fillable = ['nama_instansi', 'alamat', 'email', 'nomor_telepon','kode_pos','id_kota'];
}
