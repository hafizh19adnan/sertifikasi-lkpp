<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asesi extends Model
{
    protected $table = 'asesi';
    public $timestamps = false;
    protected $fillable = ['nama', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir','nik','id_kota','email','nomor_telepon'];
}
