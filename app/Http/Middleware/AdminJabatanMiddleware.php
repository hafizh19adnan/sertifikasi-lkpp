<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminJabatanMiddleware
{
     /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

     /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->getUser()->role == '1' || $this->auth->getUser()->role == '2' || $this->auth->getUser()->role == '3') {
           
        }else{
            abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }
}
