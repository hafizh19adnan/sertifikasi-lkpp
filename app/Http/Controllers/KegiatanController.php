<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kegiatan = DB::select('SELECT * FROM kegiatan_mak01');
        $count = sizeof($kegiatan);
        return view('dashboard.kegiatan-default',['kegiatan'=>$kegiatan,'count'=>$count]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::table('kegiatan_mak01')->insert(
            ['kategori' => $request['kategori'],
            'sub_kategori' => $request['sub_kategori'],
            'nama_kegiatan' => $request['nama_kegiatan']
            ]
        );
        return redirect()->route('kegiatan.index')
        ->with('success','Data Kegiatan Berhasil Dimasukan');   
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kegiatan = DB::select('SELECT * FROM kegiatan_mak01 WHERE id=?',[$id]);
        $count = sizeof($kegiatan);
        return view('dashboard.kegiatan-edit',['kegiatan'=>$kegiatan,'count'=>$count]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('kegiatan_mak01')->where('id',$id)->update(
            ['kategori' => $request['kategori'],
            'sub_kategori' => $request['sub_kategori'],
            'nama_kegiatan' => $request['nama_kegiatan']
            ]
        );
        return redirect()->route('kegiatan.index')
        ->with('success','Data Kegiatan Berhasil Dimasukan'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from kegiatan_mak01 where id=?',[$id]);
        } catch(\Exception $e){
            return redirect()->route('kegiatan.index')
            ->with('error','Data provinsi tidak berhasil dihapus karena diacu oleh data lain');
        }
        return redirect()->route('kegiatan.index')
            ->with('success','Data provinsi berhasil dihapus');
    }
}
