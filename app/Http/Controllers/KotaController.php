<?php

namespace App\Http\Controllers;

use App\Kota;
use App\Provinsi;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kotas = DB::SELECT('SELECT kota.*,provinsi.nama_provinsi FROM kota, provinsi WHERE kota.id_provinsi=provinsi.id');
        $provinsis = Provinsi::all();
        $count = sizeof($kotas);
        return view('dashboard.kota-default',compact('provinsis','kotas','count'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
            'nama_kota' => 'required',
            'id_provinsi' => 'required',
        ]);
  
        DB::table('kota')->insert(
            ['id' => $request->id,
            'id_provinsi'=> $request->id_provinsi,
            'nama_kota' => $request->nama_kota
            ]
        );
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." memasukan data master kota",
            'timestamp' => Carbon::now()           
            ]
        ); 
   
        return redirect()->route('kota.index')
                        ->with('success','Instansi created successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kota = DB::select('SELECT * FROM KOTA where id=?',[$id]);
        $provinsi = DB::select('SELECT * FROM PROVINSI');
        
        return view('dashboard.kota-edit')->with(['kota'=>$kota,'provinsi'=>$provinsi]);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        DB::table('kota')
            ->where('id', $id)
            ->update(
            ['id' => $request->id,
            'nama_kota' => $request->nama_kota,
            'id_provinsi' => $request->id_provinsi
            ]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data master kota dengan ID".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );  
        return redirect()->route('kota.index')
        ->with('success','Kota berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from kota where id=?',[$id]);
        } catch(\Exception $e){
            return redirect()->route('kota.index')
                        ->with('error','Data kota tidak berhasil dihapus karena diacu oleh data lain');
        }
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data master kota dengan ID".$id.".",
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('kota.index')
                        ->with('success','Data kota berhasil dihapus');
    }
}
