<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriKomponenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = DB::table('kategori_komponen')->get();
        $count = sizeof($kategori);
        return view('dashboard.kategori-default',['count'=>$count,'kategori'=>$kategori]);
    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('kategori_komponen')->insert(
            ['nama_kategori' => $request->nama_kategori
            ]
        );
        return redirect()->route('kategori.index')
        ->with('success','Kategori created successfully.');
    }

  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori_komponen')->where('id',$id)->first();
        
        return view('dashboard.kategori-edit',['kategori'=>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('kategori_komponen')
        ->where('id', $id)
        ->update(
            ['nama_kategori' =>$request['nama_kategori']
            
            ]
    );
    return redirect()->route('kategori.index')
    ->with('success','Data provinsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from kategori_komponen where id=?',[$id]);
        } catch(\Exception $e){
            return redirect()->route('kategori.index')
            ->with('error','Data provinsi tidak berhasil dihapus karena diacu oleh data lain');
        }
        return redirect()->route('kategori.index')
            ->with('success','Data provinsi berhasil dihapus');
    }
}
