<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujikom= DB::select('SELECT U.*, K.nama_kota FROM UJIKOM U, Kota K WHERE U.id_kota=K.id');
        return view('dashboard.feedback-default',['ujikom'=>$ujikom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $ujikom = DB::select('SELECT id,judul FROM ujikom where id=?',[$id]);
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $komponen = DB::select('SELECT * FROM feedback_komponen');
        $count_komponen=DB::table('feedback_komponen')->count();
        
        return view('dashboard.feedback-insert',['komponen'=>$komponen,'count_komponen'=>$count_komponen,'ujikom'=>$ujikom,'asesi'=>$asesi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('feedback_general')->insert(
            ['id_ujikom' =>$request['id_ujikom'],
            'id_asesi' => $request['id_asesi'],
            'deskripsi_feedback' => $request['deskripsi_feedback']
            ]
        );
        $counter=$request['count_komponen'];
        for($i=0;$i<$counter;$i++){
            DB::table('feedback_ujikom')->insert(
                ['id_komponen' => $request['id_komponen'.$i],
                'id_ujikom' =>$request['id_ujikom'],
                'id_asesi' => $request['id_asesi'],
                'hasil' => $request['hasil'.$i]
                ]
            );
        }
        return redirect()->route('feedback.index')
        ->with('success','Data Feedback  Berhasil Dimasukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ujikom= DB::select('SELECT U.*, K.nama_kota FROM UJIKOM U, Kota K WHERE U.id_kota=K.id and U.id=?',[$id]);
        $skema = DB::select('SELECT * FROM skema s, skema_ujikom su WHERE su.id_skema=s.id and su.id_ujikom=?',[$id]);
        $general = DB::select('SELECT F.*,a.id as id_asesi, a.nama FROM feedback_general f, asesi a where f.id_asesi=a.id and f.id_ujikom=?',[$id]);
        $komponen = DB::SELECT("SELECT kategori, pernyataan, count(hasil) as frekuensi_ya FROM feedback_ujikom fu, feedback_komponen fk WHERE fu.id_komponen=fk.id and hasil='Ya' and fu.id_ujikom=? GROUP BY kategori, pernyataan",[$id]);
        return view('dashboard.feedback-detail',['komponen'=>$komponen,'ujikom'=>$ujikom,'skema'=>$skema,'general'=>$general]);
    }

    public function showAsesi($id){
        $feedback_general = DB::select('SELECT * FROM feedback_general f, ujikom u, asesi a WHERE f.id_asesi=a.id and f.id_ujikom=u.id and f.id=?',[$id]);
        $id_asesi = DB::table('feedback_general')->where('id',$id)->pluck('id_asesi');
        $id_ujikom = DB::table('feedback_general')->where('id',$id)->pluck('id_ujikom');
        
        $detail = DB::SELECT('SELECT * FROM feedback_ujikom f, feedback_komponen fk WHERE f.id_komponen=fk.id and f.id_asesi=? and f.id_ujikom=?',[$id_asesi[0],$id_ujikom[0]]);
        return view('dashboard.feedback-show-asesi',['detail'=>$detail,'feedback_general'=>$feedback_general]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
