<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PeninjauanAsesmenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peninjauan = DB::SELECT('SELECT * FROM asesmen_peninjauan');
        $count=sizeof($peninjauan);
        return view('dashboard.peninjauan-asesmen-mak07-default',['count'=>$count,'peninjauan'=>$peninjauan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $unit = DB::select('SELECT * from unit_kompetensi');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        return view('dashboard.peninjauan-asesmen-mak07-insert',['asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('asesmen_peninjauan')->insert(
            ['skema_sertifikasi' => $request->{'skema_sertifikasi'},
            'nomor_skema_sertifikasi' => $request->{'nomor_skema_sertifikasi'},
            'rekomendasi_prosedur' => $request->{'rekomendasi_prosedur'},
            'rekomendasi_konsistensi'=>$request->{'rekomendasi_konsistensi'},
            'perencanaan' => $request->{'perencanaan'},
            'pra' => $request->{'pra'},
            'pelaksanaan'=>$request->{'pelaksanaan'},
            'keputusan'=>$request->{'keputusan'},
            'umpan_balik'=>$request->{'umpan_balik'},
            'pencatatan'=>$request->{'pencatatan'},
            'konsistensi'=>$request->{'konsistensi'}
            
            ]
        ); 
        return redirect()->route('asesmen-peninjauan.index')
        ->with('success','Data MAK07  Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $unit = DB::select('SELECT * from unit_kompetensi');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $peninjauan = DB::SELECT('SELECT * FROM asesmen_peninjauan where id=?',[$id]);
        return view('dashboard.peninjauan-asesmen-mak07-detail',['peninjauan'=>$peninjauan,'asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $unit = DB::select('SELECT * from unit_kompetensi');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $peninjauan = DB::SELECT('SELECT * FROM asesmen_peninjauan where id=?',[$id]);
        return view('dashboard.peninjauan-asesmen-mak07-edit',['peninjauan'=>$peninjauan,'asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('asesmen_peninjauan')->where('id',$id)->update(
            ['skema_sertifikasi' => $request->{'skema_sertifikasi'},
            'nomor_skema_sertifikasi' => $request->{'nomor_skema_sertifikasi'},
            'rekomendasi_prosedur' => $request->{'rekomendasi_prosedur'},
            'rekomendasi_konsistensi'=>$request->{'rekomendasi_konsistensi'},
            'perencanaan' => $request->{'perencanaan'},
            'pra' => $request->{'pra'},
            'pelaksanaan'=>$request->{'pelaksanaan'},
            'keputusan'=>$request->{'keputusan'},
            'umpan_balik'=>$request->{'umpan_balik'},
            'pencatatan'=>$request->{'pencatatan'},
            'konsistensi'=>$request->{'konsistensi'}
            
            ]
        ); 
        return redirect()->route('asesmen-peninjauan.index')
        ->with('success','Data MAK07  Berhasil Diubah');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from asesmen_peninjauan where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('asesmen-peninjauan.index')
            ->with('error','Data MAK-07 tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('asesmen-peninjauan.index')
            ->with('success','Data MAK-07 berhasil dihapus');

    }
}
