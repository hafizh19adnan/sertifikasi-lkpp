<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kota;

class UnitKompetensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit_kompetensi = DB::table('unit_kompetensi')->get();
        return view('dashboard.unit-ujikom-default',['unit_kompetensi'=>$unit_kompetensi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('unit_kompetensi')->insert(
            ['nomor_unit_kompetensi' => $request['nomor_unit_kompetensi'],
            'nama_unit_kompetensi' => $request['nama_unit_kompetensi']
            ]
        );
        return redirect()->route('unit-ujikom.index')->with('success','Data Unit Kompetensi Berhasil Dimasukan');
    }

 
    public function edit($id){
        $unit_kompetensi = DB::table('unit_kompetensi')->where('id',$id)->first();
        return view('dashboard.unit-ujikom-edit',['unit_kompetensi'=>$unit_kompetensi]);
    }

    public function update(Request $request, $id){
        DB::table('unit_kompetensi')
            ->where('id', $id)
            ->update(
                ['nomor_unit_kompetensi' => $request['nomor_unit_kompetensi'],
                 'nama_unit_kompetensi' => $request['nama_unit_kompetensi']
            ]
        );
        return redirect()->route('unit-ujikom.index')
        ->with('success','Data unit ujikom berhasil diperbaharui.');
    }

    public function unitCheck(Request $request){
        $count = DB::table('unit_kompetensi')->where('nomor_unit_kompetensi',$request->nomor)->count();
        if($count==0){
            return "unique";
        }else{
            return "nor unique";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try{
            DB::delete('delete from unit_kompetensi where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('unit-ujikom.index')
                             ->with('error','Data Unit Kompetensi Tidak Berhasil Dihapus');
        }
     
        return redirect()->route('unit-ujikom.index')
                ->with('success','Data Unit Kompetensi Berhasil Dihapus');
    }
}
