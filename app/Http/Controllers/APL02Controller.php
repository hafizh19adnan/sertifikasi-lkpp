<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APL02Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ujikom = DB::select('SELECT id,judul FROM ujikom');
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $skema_unit = DB::select('SELECT * FROM skema_unit su, unit_kompetensi u WHERE su.id_unit=u.id and id_skema=1');
        $unit_pertanyaan = DB::select('SELECT * FROM unit_pertanyaan');
        return view('dashboard.apl02-insert',['unit_pertanyaan'=>$unit_pertanyaan,'unit'=>$skema_unit,'skema'=>$skema,'ujikom'=>$ujikom, 'asesi'=>$asesi,'asesor'=>$asesor]);
    }

    public function getAsesiSkemaAPL02($id){
        $result = DB::SELECT('SELECT a.nama, a.id FROM asesi a,skema s, asesi_skema ask WHERE ask.id_asesi=a.id and ask.id_skema=s.id and ask.id_skema=? and ask.id_asesi and id_asesi NOT IN (SELECT id_asesi FROM asesi_apl02)',[$id]);
        return $result;
    }

    public function getSkemaUnitPertanyaan($id){
        $result = DB::SELECT('SELECT up.id as id_pertanyaan, u.id, nomor_unit_kompetensi, nama_unit_kompetensi, pertanyaan FROM unit_Kompetensi u, skema_unit su, unit_pertanyaan up WHERE su.id_unit=u.id and u.id=up.id_unit and su.id_skema=? ORDER BY nomor_unit_kompetensi',[$id]);
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('asesi_apl02')->insert(
            ['id_asesi' => $request->{'id_asesi'},
            'id_asesor' => $request->{'id_asesor'},
            'id_skema' => $request->{'id_skema'},
            'rekomendasi_asesor'=>$request->{'rekomendasi_asesor'},
            'catatan'=>$request->{'catatan'},
            ]
        ); 
    
        $id_apl=DB::select('SELECT id FROM asesi_apl02 ORDER BY id DESC LIMIT 1');
        $counter=0;
       
        for($i=0;$i<$request->in;$i++){
            
            DB::table('asesi_apl02_detail')->insert(
                ['id_apl' => $id_apl[0]->id,
                'id_unit' => $request->{'id_unit'.$i},
                'id_pertanyaan' => $request->{'id_pertanyaan'.$i},
                'bukti_pendukung' => $request->{'bukti_pendukung'.$i},
                'isian_asesor' => $request->{'isian_asesor'.$i}
                
                ]
            ); 
        
        }
        return redirect()->route('asesi.index')
        ->with('success','Data APL02  Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
