<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengirimanSertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sertifikat = DB::select('SELECT * From sertifikat');
        $pengiriman = DB::select('SELECT s.nomor_sertifikat,sp.* FROM sertifikat_pengiriman sp, sertifikat s where sp.id_sertifikat=s.id');
        $count = sizeof($pengiriman);
        return view('dashboard.pengiriman-sertifikasi-default',['count'=>$count,'pengiriman'=>$pengiriman,'sertifikat'=>$sertifikat]);
    }
    public function getTanggalSertifikat($id){
        $result = DB::SELECT('SELECT u.tanggal_mulai as tanggal_ujikom FROM sertifikat s, ujikom u WHERE s.id_ujikom=u.id and s.id=?',[$id]);
        return $result;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('sertifikat_pengiriman')->insert(
            ['id_sertifikat' =>$request['id_sertifikat'],
            'tanggal_kirim' => $request['tanggal_kirim'],
            'alamat_pengiriman' => $request['alamat_pengiriman'],
            'nomor_telepon_pic' => $request['nomor_telepon_pic'],
            'nomor_resi' => $request['nomor_resi']
            ]
        );
        return redirect('/dashboard/pengiriman-sertifikat')->with('success','Data Pengiriman Sertifikat Berhasil Dimasukan');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sertifikat = DB::select('SELECT * From sertifikat');
        $pengiriman = DB::select('SELECT s.nomor_sertifikat,sp.* FROM sertifikat_pengiriman sp, sertifikat s where sp.id_sertifikat=s.id and sp.id=?',[$id]);
        return view('dashboard.pengiriman-sertifikasi-edit',['pengiriman'=>$pengiriman,'sertifikat'=>$sertifikat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('sertifikat_pengiriman')
            ->where('id', $id)
            ->update(
                ['id_sertifikat' =>$request['id_sertifikat'],
                'tanggal_kirim' => $request['tanggal_kirim'],
                'alamat_pengiriman' => $request['alamat_pengiriman'],
                'nomor_telepon_pic' => $request['nomor_telepon_pic'],
                'nomor_resi' => $request['nomor_resi']
                ]
        );
        // return $request;
        return redirect()->route('pengiriman-sertifikat.index')
        ->with('success','Data pengiriman sertifikat berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from sertifikat_pengiriman where id=?',[$id]);
        return redirect()->route('pengiriman-sertifikat.index')
        ->with('success','Data pengiriman sertifikat berhasil dihapus');
    }
}
