<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Kota;
use App\Provinsi;
use Response;
use App\Instansi;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_asesi= DB::table('asesi')->count();
        $count_asesor= DB::table('asesor')->count();
        $count_instansi= DB::table('instansi')->count();
        $count_pelaksanaan= DB::table('ujikom')->count();
        $rekap_instansi = DB::select("SELECT nama_instansi, nama_skema, count(*) as jumlah_kompeten FROM asesi_skema ask, asesi a, instansi i, skema s WHERE ask.id_asesi=a.id and a.id_instansi=i.id and ask.id_skema=s.id and hasil='K' GROUP BY nama_instansi, nama_skema");

        $skema_kompeten = DB::select("SELECT nama_skema, count(*) as jumlah_kompeten FROM skema s, asesi_skema ask WHERE ask.id_skema=s.id and hasil='K' GROUP BY nama_skema ORDER BY jumlah_kompeten");
        $skema_non_kompeten = DB::select("SELECT nama_skema, count(*) as jumlah_kompeten FROM skema s, asesi_skema ask WHERE ask.id_skema=s.id and hasil='BK' GROUP BY nama_skema ORDER BY jumlah_kompeten");
        $asesi_non_kompeten = DB::select("SELECT DISTINCT a.nama as nama_asesi, asr.nama as nama_asesor, s.nama_skema, u.judul, uah.hasil FROM ujikom_asesi_hasil uah, ujikom u, asesi a, asesor asr, skema s WHERE uah.id_asesi=a.id and uah.id_asesor=asr.id and uah.id_ujikom=u.id and uah.id_skema=s.id and hasil='BK'");
        
        $unit_kompeten= DB::select("SELECT nama_unit_kompetensi, count(*) as jumlah_kompeten  FROM unit_kompetensi u, asesi_unit ask WHERE ask.id_unit=u.id and hasil='K' GROUP BY nama_unit_kompetensi, hasil ORDER BY jumlah_kompeten");
        $unit_non_kompeten=DB::select("SELECT nama_unit_kompetensi, count(*) as jumlah_kompeten  FROM unit_kompetensi u, asesi_unit ask WHERE ask.id_unit=u.id and hasil='BK' GROUP BY nama_unit_kompetensi, hasil ");
        return view('dashboard.home',['count_asesi'=>$count_asesi,
        'count_asesor'=>$count_asesor,'count_instansi'=>$count_instansi,
        'count_pelaksanaan'=>$count_pelaksanaan,'asesi_non_kompeten'=>$asesi_non_kompeten, 'skema_kompeten'=>$skema_kompeten,'skema_non_kompeten'=>$skema_non_kompeten,
        'unit_kompeten'=>$unit_kompeten,'unit_non_kompeten'=>$unit_non_kompeten,'rekap_instansi'=>$rekap_instansi]);
    }
    public function rekapInstansi(){
        $headers = array(
           "Content-type" => "text/csv",
           "Content-Disposition" => "attachment; filename=Rekap per Instansi.csv",
           "Pragma" => "no-cache",
           "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
           "Expires" => "0"
       );

       $rekap_instansi = DB::select("SELECT nama_instansi, nama_skema, count(*) as jumlah_kompeten FROM asesi_skema ask, asesi a, instansi i, skema s WHERE ask.id_asesi=a.id and a.id_instansi=i.id and ask.id_skema=s.id and hasil='K' GROUP BY nama_instansi, nama_skema");
        $columns = array('NAMA INSTANSI','NAMA SKEMA','JUMLAH KOMPETEN');

       $callback = function() use ($rekap_instansi, $columns)
       {
           $file = fopen('php://output', 'w');
           fputcsv($file, $columns);

           foreach($rekap_instansi as $rekap_instansi) {
               
               fputcsv($file, array($rekap_instansi->nama_instansi,$rekap_instansi->nama_skema,$rekap_instansi->jumlah_kompeten));
           }
           fclose($file);
       };
       return Response::stream($callback, 200, $headers);
   }
    public function SkemaKompeten(){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=Rekap Skema Kompeten.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    );

    $skema_kompeten = DB::select("SELECT nama_skema, count(*) as jumlah_kompeten FROM skema s, asesi_skema ask WHERE ask.id_skema=s.id and hasil='K' GROUP BY nama_skema ORDER BY jumlah_kompeten");
    $columns = array('NAMA SKEMA KOMPETENSI','JUMLAH KOMPETEN');

    $callback = function() use ($skema_kompeten, $columns)
    {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($skema_kompeten as $skema_kompeten) {
            
            fputcsv($file, array($skema_kompeten->nama_skema,$skema_kompeten->jumlah_kompeten));
        }
        fclose($file);
    };
    return Response::stream($callback, 200, $headers);
    }
    public function SkemaNonKompeten(){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=Rekap Skema non Kompeten.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );

        $skema_non_kompeten = DB::select("SELECT nama_skema, count(*) as jumlah_kompeten FROM skema s, asesi_skema ask WHERE ask.id_skema=s.id and hasil='BK' GROUP BY nama_skema ORDER BY jumlah_kompeten");
        $columns = array('NAMA SKEMA KOMPETENSI','JUMLAH BELUM KOMPETEN');

        $callback = function() use ($skema_non_kompeten, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($skema_non_kompeten as $skema_non_kompeten) {
                
                fputcsv($file, array($skema_non_kompeten->nama_skema,$skema_non_kompeten->jumlah_kompeten));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
    public function AsesiNonKompeten(){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=Rekap Asesi non Kompeten.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );

        $asesi_non_kompeten = DB::select("SELECT DISTINCT a.nama as nama_asesi, asr.nama as nama_asesor, s.nama_skema, u.judul, uah.hasil FROM ujikom_asesi_hasil uah, ujikom u, asesi a, asesor asr, skema s WHERE uah.id_asesi=a.id and uah.id_asesor=asr.id and uah.id_ujikom=u.id and uah.id_skema=s.id and hasil='BK'");
        $columns = array('Nama Asesi', 'Nama Asesor','Nama Skema','Judul Ujikom','Hasil');

        $callback = function() use ($asesi_non_kompeten, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($asesi_non_kompeten as $asesi) {
                
                fputcsv($file, array($asesi->nama_asesi,$asesi->nama_asesor,$asesi->nama_skema,$asesi->judul,$asesi->hasil));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
    public function UnitKompeten(){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=Rekap Unit Kompeten.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );

        $unit_kompeten= DB::select("SELECT nama_unit_kompetensi, count(*) as jumlah_kompeten  FROM unit_kompetensi u, asesi_unit ask WHERE ask.id_unit=u.id and hasil='K' GROUP BY nama_unit_kompetensi, hasil ORDER BY jumlah_kompeten");
        $columns = array('Nama Unit', 'Jumlah Belum Kompeten');

        $callback = function() use ($unit_kompeten, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($unit_kompeten as $unit) {
                
                fputcsv($file, array($unit->nama_unit_kompetensi,$unit->jumlah_kompeten));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
    public function UnitNonKompeten(){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=Rekap Unit non Kompeten.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );

        $unit_non_kompeten=DB::select("SELECT nama_unit_kompetensi, count(*) as jumlah_kompeten  FROM unit_kompetensi u, asesi_unit ask WHERE ask.id_unit=u.id and hasil='BK' GROUP BY nama_unit_kompetensi, hasil ");
        $columns = array('Nama Unit', 'Jumlah Belum Kompeten');

        $callback = function() use ($unit_non_kompeten, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($unit_non_kompeten as $unit) {
                
                fputcsv($file, array($unit->nama_unit_kompetensi,$unit->jumlah_kompeten));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

}
