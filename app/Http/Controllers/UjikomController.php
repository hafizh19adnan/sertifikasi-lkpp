<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Provinsi;
use App\Kota;


class UjikomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujikom= DB::select('SELECT U.*, K.nama_kota FROM UJIKOM U, Kota K WHERE U.id_kota=K.id');
        
        return view('dashboard.ujikom-default',['ujikom'=>$ujikom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinsis = Provinsi::all();
        $skema = DB::table('skema')->get();
        return view('dashboard.ujikom-new',['provinsis'=>$provinsis,'skema'=>$skema]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('ujikom')->insert(
            ['judul' =>$request['judul'],
            'tuk' => $request['tuk'],
            'tanggal_mulai' => $request['tanggal_mulai'],
            'tanggal_selesai' => $request['tanggal_selesai'],
            'tempat_uji' => $request['tempat_uji'],
            'id_kota' => $request['id_kota']
            ]
        );
        $id_ujikom=DB::select('SELECT id FROM ujikom ORDER BY id DESC LIMIT 1');
        
        for($i=0;$i<sizeof($request['id_skema']);$i++){
            DB::table('skema_ujikom')->insert(
            ['id_ujikom' => $id_ujikom[0]->id,
            'id_skema' => $request['id_skema'][$i]
            ]);
        }

        return redirect()->route('ujikom.index')
        ->with('success','Data Ujikom Berhasil Dimasukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ujikom= DB::select('SELECT U.*, K.nama_kota FROM UJIKOM U, Kota K WHERE U.id_kota=K.id and U.id=?',[$id]);
        $asesi_hasil = DB::select('SELECT DISTINCT a.*,i.nama_instansi,ua.hasil FROM ujikom_asesi_hasil ua, asesi a, instansi i WHERE a.id_instansi=i.id and ua.id_asesi = a.id and ua.id_ujikom=?',[$id]);
        $asesis = DB::select('SELECT a.*,i.nama_instansi , k.nama_kota FROM kota k,instansi i, asesi_ujikom au, asesi a WHERE a.id_kota=k.id and a.id_instansi=i.id and au.id_asesi=a.id and au.id_ujikom=?',[$id]);
        $asesor = DB::select('SELECT a.*,i.nama_instansi FROM  asesor_penugasan ap, asesor a, instansi i WHERE ap.id_asesor=a.id and a.id_instansi=i.id and ap.id_ujikom=?',[$id]);
        $skema = DB::select('SELECT * FROM skema s, skema_ujikom su WHERE su.id_skema=s.id and su.id_ujikom=?',[$id]);
        if(sizeof($ujikom)==0){
            return view('dashboard.ujikom-detail-error');
        }else{
            return view('dashboard.ujikom-detail',['asesi_hasil'=>$asesi_hasil,'ujikom'=>$ujikom,'asesis'=>$asesis,'asesor'=>$asesor,'skema'=>$skema]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ujikom = DB::table('ujikom')->where('id',$id)->first();
        $selected_prov = DB::select('SELECT * FROM provinsi WHERE id IN (SELECT id_provinsi FROM kota WHERE id=?) LIMIT 1',[$ujikom->id_kota]);
        $provinsis = Provinsi::all();
        $skema = DB::table('skema')->get();
        $kota= Kota::all();
        return view('dashboard.ujikom-edit',['kotas'=>$kota,'selected_prov'=>$selected_prov,'ujikom'=>$ujikom,'provinsis'=>$provinsis,'skema'=>$skema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('ujikom')
        ->where('id', $id)
        ->update(
            ['judul' =>$request['judul'],
            'tuk' => $request['tuk'],
            'tanggal_mulai' => $request['tanggal_mulai'],
            'tanggal_selesai' => $request['tanggal_selesai'],
            'tempat_uji' => $request['tempat_uji'],
            'id_kota' => $request['id_kota']
            ]
        );
        return redirect()->route('ujikom.index')
        ->with('success','Data Ujikom Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from skema_ujikom where id_ujikom=?',[$id]);
            DB::delete('delete from ujikom where id=?',[$id]);
          
        }catch(\Exception $e){
            return redirect()->route('ujikom.index')
                             ->with('error','Data ujikom tidak berhasil dihapus');
        }
        
        return redirect()->route('ujikom.index')
                         ->with('success','Data ujikom berhasil dihapus');
    }

    public function AddAsesor($id){
        $ujikom = DB::table('ujikom')->where('id',$id)->first();
        $asesor = DB::select('SELECT * FROM asesor WHERE id NOT IN (SELECT id_asesor FROM asesor_penugasan WHERE id_ujikom=?)',[$id]);
        return view('dashboard.ujikom-add-asesor',['ujikom'=>$ujikom,'asesor'=>$asesor]);
    }

    public function storeAddAsesor(Request $request){
        DB::table('asesor_penugasan')->insert(
            ['id_ujikom' =>$request['id_ujikom'],
            'id_asesor' => $request['id_asesor']
            ]
        );
        return redirect()->route('ujikom.show',[$request['id_ujikom']])
        ->with('success','Data Asesor Tambahan Berhasil Dimasukan');
    }
    
    public function AddAsesi($id){
        $ujikom = DB::table('ujikom')->where('id',$id)->first();
        $asesi = DB::select('SELECT * FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_ujikom WHERE id_ujikom=?)',[$id]);
        return view('dashboard.ujikom-add-asesi',['ujikom'=>$ujikom,'asesi'=>$asesi]);
    }

    public function storeAddAsesi(Request $request){
        DB::table('asesi_ujikom')->insert(
            ['id_ujikom' =>$request['id_ujikom'],
            'id_asesi' => $request['id_asesi']
            ]
        );
        return redirect()->route('ujikom.show',[$request['id_ujikom']])
        ->with('success','Data Asesor Tambahan Berhasil Dimasukan');
    }

    public function setUjikomSkema($id){
        $ujikom = DB::table('ujikom')->where('id',$id)->first();
        $skema = DB::select('SELECT * FROM skema s where id NOT IN (SELECT id_skema FROM skema_ujikom WHERE id_ujikom=?)',[$id]);
        $skema_ujikom = DB::select('SELECT su.*, s.nama_skema FROM skema_ujikom su, skema s WHERE su.id_skema=s.id and su.id_ujikom=?',[$id]);
        $count_skema_ujikom = sizeof($skema_ujikom);
        return view('dashboard.ujikom-unit-set',['count_skema_ujikom'=>$count_skema_ujikom,'skema_ujikom'=>$skema_ujikom,'ujikom'=>$ujikom,'skema'=>$skema]);
    }

    public function storeUjikomSkema(Request $request){
        DB::table('skema_ujikom')->insert(
            ['id_ujikom' =>$request['id_ujikom'],
            'id_skema' => $request['id_skema']
            ]
        );
        return redirect('/dashboard/ujikom-skema-set/'.$request['id_ujikom'])
        ->with('success','Data Unit Ujikom Berhasil Dimasukan');
    }

    public function destroyUjikomSkema(Request $request, $id){
        DB::delete('delete from skema_ujikom where id=?',[$id]);
        
        return redirect('/dashboard/ujikom-skema-set/'.$request['id_ujikom_redirect'])
        ->with('success','Data Unit Ujikom Berhasil Dimasukan');
    }
    

}
