<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PenugasanAsesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asesors= DB::select('SELECT A.id, A.nama,A.jenis_asesor,I.nama_instansi FROM asesor A, instansi I WHERE A.id_instansi=I.id');
        return view('dashboard.asesor-penugasan-default',['asesors'=>$asesors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $asesors= DB::select('SELECT A.id, A.nama,A.jenis_asesor,I.nama_instansi FROM asesor A, instansi I WHERE A.id_instansi=I.id and A.id=?',[$id]);
        $ujikom=DB::select('SELECT * FROM ujikom WHERE id NOT IN (SELECT id_ujikom FROM asesor_penugasan WHERE id_asesor=?)',[$id]);
        return view('dashboard.asesor-penugasan-create',['asesors'=>$asesors,'ujikom'=>$ujikom]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('asesor_penugasan')->insert(
            ['id_asesor' =>$request['id_asesor'],
            'id_ujikom' => $request['id_ujikom']
            ]
        );
        return redirect('/dashboard/penugasan-asesor/show/'.$request['id_asesor'])->with('success','Data Penugasan Berhasil Dimasukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ujikom = DB::select('SELECT a.*,k.nama_kota, u.judul, u.tanggal_mulai, u.tanggal_selesai, u.tempat_uji FROM kota k, ujikom u, asesor_penugasan a WHERE a.id_ujikom=u.id and u.id_kota=k.id and a.id_asesor=?',[$id]);
        $asesor = DB::table('asesor')->where('id', $id)->first();
        return view('dashboard.asesor-penugasan-detail',['ujikom'=>$ujikom,'asesor'=>$asesor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from asesor_penugasan where id=?',[$id]);
        }catch(\Exception $e){
            return redirect('/dashboard/penugasan-asesor')->with('error','Data Penugasan tidak berhasil dihapus');
        }
        return redirect('/dashboard/penugasan-asesor')->with('success','Data Penugasan Berhasil dihapus');
    }
}
