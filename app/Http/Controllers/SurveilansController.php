<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class SurveilansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveilans= DB::SELECT("SELECT p.id, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, rekomendasi
        FROM surveilans p, asesi a, asesor asr, skema s 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id");
        $count=sizeof($surveilans);
        return view('dashboard.surveilans-default',['count'=>$count,'surveilans'=>$surveilans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s where id IN (SELECT id_skema FROM skema_ujikom)');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
     
        return view('dashboard.surveilans-insert',['skema'=>$skema,'asesi'=>$asesi,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('surveilans')->insert(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'tanggal' => $request['tanggal'],
            'rekomendasi' => $request['rekomendasi']
           
            ]
        );
        $id_surveilans=DB::select('SELECT id FROM surveilans ORDER BY id DESC LIMIT 1');
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('surveilans_detail')->insert(
                ['id_surveilans' =>$id_surveilans[0]->id,
                'id_unit_kompetensi' => $request['id_unit_kompetensi'.$i],
                'hasil' => $request['hasil'.$i]
                ]
            );
        }
        return redirect()->route('surveilans.index')
        ->with('success','Data Surveilans  Berhasil Dimasukan');   
    }

    public function getUnitSurveilans(Request $request){
        $unit = DB::SELECT("SELECT * from unit_kompetensi u, asesi_unit au WHERE au.id_unit=u.id and au.id_unit  and au.id_asesi=? and hasil='K'",[$request['id_asesi']]);
        return $unit;
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s, skema_ujikom su');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $surveilans= DB::SELECT("SELECT p.id, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, rekomendasi
        FROM surveilans p, asesi a, asesor asr, skema s 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id and p.id=?",[$id]);
        $detail = DB::SELECT('SELECT * FROM unit_kompetensi u, surveilans_detail s WHERE s.id_unit_kompetensi=u.id and s.id_surveilans=?',[$id]);
        return view('dashboard.surveilans-detail',['detail'=>$detail,'surveilans'=>$surveilans,'skema'=>$skema,'asesi'=>$asesi,'asesor'=>$asesor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $surveilans= DB::SELECT("SELECT p.id, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, rekomendasi
                    FROM surveilans p, asesi a, asesor asr, skema s 
                    WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id and p.id=?",[$id]);
        $detail = DB::SELECT('SELECT s.*,u.nomor_unit_kompetensi,u.nama_unit_kompetensi  FROM unit_kompetensi u, surveilans_detail s WHERE s.id_unit_kompetensi=u.id and s.id_surveilans=?',[$id]);
        return view('dashboard.surveilans-edit',['count'=>count($detail),'detail'=>$detail,'surveilans'=>$surveilans,'skema'=>$skema,'asesi'=>$asesi,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('surveilans')
        ->where('id', $id)
        ->update(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'tanggal' => $request['tanggal'],
            'rekomendasi' => $request['rekomendasi']
           
            ]
        );
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('surveilans_detail')->where('id', $request['id_detail'.$i])
            ->update(
                ['id_unit_kompetensi' => $request['id_unit_kompetensi'.$i],
                'hasil' => $request['hasil'.$i]
                ]
            );
        }
        return redirect()->route('surveilans.index')
        ->with('success','Data Surveilans  Berhasil Diperbarui');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from surveilans_detail where id_surveilans=?',[$id]);
            DB::delete('delete from surveilans where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('surveilans.index')
            ->with('error','Data surveilans tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('surveilans.index')
            ->with('success','Data surveilans  berhasil dihapus');
    }
}
