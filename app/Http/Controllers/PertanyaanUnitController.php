<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PertanyaanUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $pertanyaan= DB::select('SELECT * FROM unit_pertanyaan WHERE id_unit=?',[$id]);
        $unit_ujikom =DB::select('SELECT * FROM unit_kompetensi WHERE id=?',[$id]);
        $count=sizeof($pertanyaan);
        return view('dashboard.unit-ujikom-pertanyaan',['count'=>$count,'pertanyaan'=>$pertanyaan,'unit_ujikom'=>$unit_ujikom]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('unit_pertanyaan')->insert(
            ['id_unit' =>$request['id_unit'],
            'pertanyaan' => $request['pertanyaan']            
            ]
        );
        return redirect()->action(
            'PertanyaanUnitController@index', ['id'=>$request['id_unit']]
            )->with('success','Data Unit Kompetensi Berhasil Dimasukan');       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        DB::delete('delete from unit_pertanyaan where id=?',[$id]);
        return redirect()->action(
            'PertanyaanUnitController@index', ['id'=>$request['id_unit_ujikom']]
            )->with('success','Data Unit Kompetensi Berhasil Dihapus');  
    }
}
