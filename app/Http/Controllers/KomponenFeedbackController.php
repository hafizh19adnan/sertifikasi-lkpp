<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class KomponenFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = DB::table('kategori_komponen')->get();
        $komponen = DB::select('SELECT * FROM feedback_komponen');
        $count = sizeof($komponen);
        return view('dashboard.komponen-feedback-default',['kategori'=>$kategori,'count'=>$count,'komponen'=>$komponen]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('feedback_komponen')->insert(
            ['kategori' =>$request['kategori'],
            'pernyataan' => $request['pernyataan']
            
            ]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." memasukan data master komponen feedback",
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('komponen-feedback.index')
        ->with('success','Data Komponen Feedback  Berhasil Dimasukan');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komponen=DB::table('feedback_komponen')->where('id',$id)->first();
        return view('dashboard.komponen-feedback-edit',['komponen'=>$komponen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        DB::table('feedback_komponen')
            ->where('id', $id)
            ->update(
            ['kategori' => $request->kategori,
            'pernyataan' => $request->pernyataan
            ]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data master komponen feedback dengan ID ".$id,
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('komponen-feedback.index')
        ->with('success','Data komponen feedback berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from feedback_komponen where id=?',[$id]);
        }catch(Exception $e){
            return redirect()->route('komponen-feedback.index')
            ->with('success','Data Komponen Feedback tidak berhasil dihapus karena diacu data lain');
        }
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data master komponen feedback dengan ID ".$id,
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('komponen-feedback.index')
        ->with('success','Data Komponen Feedback  Berhasil Dihapus');
    }
}
