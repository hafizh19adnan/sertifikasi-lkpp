<?php

namespace App\Http\Controllers;

use App\Provinsi;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsis = Provinsi::all();
        $count=sizeof($provinsis);
        return view('dashboard.provinsi-default',compact('provinsis','count'));
    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_provinsi' => 'required',
            'id_provinsi' => 'required'
        ]);
  
        DB::table('provinsi')->insert(
            ['id' => $request->id_provinsi,
            'nama_provinsi' => $request->nama_provinsi]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." memasukan data master provinsi",
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('provinsi.index')
                        ->with('success','Data provinsi berhasil dimasukan.');
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function edit(Provinsi $provinsi)
    {
        return view('dashboard.provinsi-edit',compact('provinsi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provinsi $provinsi)
    {
        $request->validate([
            'nama_provinsi' => 'required',
        
        ]);
  
        $provinsi->update($request->all());
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data master provinsi dengan id ".$provinsi->id,
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('provinsi.index')
                        ->with('success','Data Provinsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provinsi $provinsi)
    {
        try{
            $provinsi->delete();
        } catch(\Exception $e){
            return redirect()->route('provinsi.index')
            ->with('error','Data provinsi tidak berhasil dihapus karena diacu oleh data lain');
        }
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data master provinsi dengan id ".$provinsi->id,
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('provinsi.index')
                        ->with('success','Data provinsi berhasil dihapus');
    }
}
