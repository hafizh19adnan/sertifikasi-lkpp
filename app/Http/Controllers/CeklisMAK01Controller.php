<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CeklisMAK01Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ceklis = DB::SELECT("SELECT cm.*,a.nama as asesi,asr.nama,u.judul,s.nama_skema FROM ceklis_mak01 cm, asesi a, asesor asr, ujikom u, skema s
        WHERE cm.id_asesi=a.id and cm.id_asesor=asr.id and cm.id_ujikom=u.id and cm.id_skema=s.id");
        $count = sizeof($ceklis);
        return view('dashboard.ceklis-default',['count'=>$count,'ceklis'=>$ceklis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $skema = DB::select('SELECT * from skema ORDER BY id');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $ujikom = DB::select('SELECT * FROM ujikom');
        $kegiatan = DB::select('SELECT * FROM kegiatan_mak01');
        return view('dashboard.ceklis-mak01-insert',['kegiatan'=>$kegiatan,'ujikom'=>$ujikom,'asesi'=>$asesi,'skema'=>$skema,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('ceklis_mak01')->insert(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'id_ujikom' => $request['id_ujikom'],
            'waktu_menit' => $request['waktu_menit'],
            'rekomendasi' => $request['rekomendasi']            ]
        );
        $id_ceklis=DB::select('SELECT id FROM ceklis_mak01 ORDER BY id DESC LIMIT 1');
        for($i=0;$i<$request['jumlah_kegiatan'];$i++){
            DB::table('ceklis_mak01_detail')->insert(
                ['id_ceklis' =>$id_ceklis[0]->id,
                'id_kegiatan' => $request['id_kegiatan'.$i],
                'hasil' => $request['hasil'.$i],
                'catatan' => $request['catatan'.$i]
                ]
            );
        }
        return redirect()->route('ceklis-kompetensi.index')
        ->with('success','Data MAK01 Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ceklis = DB::SELECT("SELECT cm.*,a.nama as asesi,asr.nama as asesor,u.judul,s.nama_skema FROM ceklis_mak01 cm, asesi a, asesor asr, ujikom u, skema s
        WHERE cm.id_asesi=a.id and cm.id_asesor=asr.id and cm.id_ujikom=u.id and cm.id_skema=s.id and cm.id=?",[$id]);
        $detail = DB::SELECT('SELECT * FROM ceklis_mak01_detail c, kegiatan_mak01 k WHERE c.id_kegiatan=k.id and id_ceklis=?',[$id]);
        return view('dashboard.ceklis-mak01-detail',['ceklis'=>$ceklis,'detail'=>$detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $skema = DB::select('SELECT * from skema ORDER BY id');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $ujikom = DB::select('SELECT * FROM ujikom');
        $kegiatan = DB::select('SELECT * FROM kegiatan_mak01');
        $ceklis = DB::SELECT("SELECT cm.*,a.nama as asesi,asr.nama as asesor,u.judul,s.nama_skema FROM ceklis_mak01 cm, asesi a, asesor asr, ujikom u, skema s
        WHERE cm.id_asesi=a.id and cm.id_asesor=asr.id and cm.id_ujikom=u.id and cm.id_skema=s.id and cm.id=?",[$id]);
        $detail = DB::SELECT('SELECT  c.*, k.kategori,k.sub_kategori,k.nama_kegiatan FROM ceklis_mak01_detail c, kegiatan_mak01 k WHERE c.id_kegiatan=k.id and id_ceklis=?',[$id]);
        return view('dashboard.ceklis-mak01-edit',['count'=>count($detail),'ceklis'=>$ceklis,'detail'=>$detail,'kegiatan'=>$kegiatan,'ujikom'=>$ujikom,'asesi'=>$asesi,'skema'=>$skema,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('ceklis_mak01') ->where('id', $id)
        ->update(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'id_ujikom' => $request['id_ujikom'],
            'waktu_menit' => $request['waktu_menit'],
            'rekomendasi' => $request['rekomendasi']            ]
        );
        for($i=0;$i<$request['jumlah_kegiatan'];$i++){
            DB::table('ceklis_mak01_detail')->where('id',  $request['id_detail'.$i])
            ->update(
                [
                'id_kegiatan' => $request['id_kegiatan'.$i],
                'hasil' => $request['hasil'.$i],
                'catatan' => $request['catatan'.$i]
                ]
            );
        }
        return redirect()->route('ceklis-kompetensi.index')
        ->with('success','Data MAK01 Berhasil Diperbarui');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from ceklis_mak01_detail where id_ceklis=?',[$id]);
            DB::delete('delete from ceklis_mak01 where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('ceklis-kompetensi.index')
            ->with('error','Data MAK-01 tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('ceklis-kompetensi.index')
            ->with('success','Data MAK-01 berhasil dihapus');
    }
}
