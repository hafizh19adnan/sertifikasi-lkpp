<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HasilUjikomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujikom= DB::select('SELECT U.*, K.nama_kota FROM UJIKOM U, Kota K WHERE U.id_kota=K.id');
        
        return view('dashboard.ujikom-hasil-default',['ujikom'=>$ujikom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $ujikom = DB::select('SELECT id,judul FROM ujikom where id=?',[$id]);
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id IN (SELECT id_asesi FROM asesi_ujikom WHERE id_ujikom=?)',[$id]);
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s, skema_ujikom su WHERE su.id_skema=s.id and su.id_ujikom=?',[$id]);
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a, asesor_penugasan ap WHERE ap.id_asesor=a.id and ap.id_ujikom=?',[$id]);
        return view('dashboard.ujikom-hasil-insert',['skema'=>$skema,'ujikom'=>$ujikom, 'asesi'=>$asesi,'asesor'=>$asesor]);
    }

    public function getAsesiBySkema($id_skema){
        $asesi = DB::select('SELECT a.id,a.nama FROM asesi a, asesi_skema ask WHERE ask.id_asesi=a.id and ask.id_skema=?',[$id_skema]);
        return json_encode($asesi);
    }

    public function getUnitBySkema($id_skema){
        $unit = DB::select('SELECT s.id_unit,u.nama_unit_kompetensi FROM unit_kompetensi u, skema_unit s WHERE s.id_unit=u.id and s.id_skema=?',[$id_skema]);
        return json_encode($unit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('ujikom_asesi_hasil')->insert(
            ['id_ujikom' =>$request['id_ujikom'],
            'id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'nomor_registrasi' => $request['nomor_registrasi'],
            'aspek_positif' => $request['aspek_positif'],
            'aspek_negatif' => $request['aspek_negatif'],
            'hasil' => $request['hasil_skema'],
            'keterangan' => $request['keterangan']
            ]
        );
        $id_hasil=DB::select('SELECT id FROM ujikom_asesi_hasil ORDER BY id DESC LIMIT 1');
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('ujikom_hasil_detail')->insert(
                ['id_hasil' =>$id_hasil[0]->id,
                'id_unit_kompetensi' => $request['id_unit_kompetensi'.$i],
                'hasil' => $request['hasil'.$i]
                ]
            );
        }
        DB::table('asesi_skema')
            ->where('id_asesi',$request['id_asesi'])
            ->where('id_skema',$request['id_skema'])
            ->update(
                ['hasil' => $request['hasil_skema_pleno']]
        );
        
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('asesi_unit')
            ->where('id_asesi',$request['id_asesi'])
            ->where('id_unit',$request['id_unit_kompetensi'.$i])
            ->update(
                ['hasil' => $request['hasil_pleno'.$i]]
            );
           
        }
    
        return redirect('/dashboard/ujikom-hasil/')
        ->with('success','Data Hasil Uji Kompetensi Berhasil Dimasukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hasil = DB::select('SELECT ua.*, a.nama as asesi,ar.nama as asesor,u.judul from ujikom_asesi_hasil ua, asesi a, asesor ar, ujikom u where ua.id_ujikom=u.id and ua.id_asesi=a.id and ua.id_asesor=ar.id and ua.id=?',[$id]);
        $hasil_detail = DB::select('SELECT * FROM ujikom_hasil_detail uh, unit_kompetensi uk WHERE uh.id_unit_kompetensi=uk.id and id_hasil=?',[$id]);
        return view("dashboard.ujikom-hasil-detail",['hasil'=>$hasil,'hasil_detail'=>$hasil_detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
