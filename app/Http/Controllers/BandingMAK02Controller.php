<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class BandingMAK02Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banding = DB::SELECT("SELECT b.id, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi, alasan_banding, tanggal_asesmen
        FROM banding_mak02 b, asesi a, asesor asr, unit_kompetensi u 
        WHERE b.id_asesi=a.id and b.id_asesor=asr.id and b.id_unit=u.id");
        $count = sizeof($banding);
        return view('dashboard.banding-mak02-default',['count'=>$count,'banding'=>$banding]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $unit = DB::select('SELECT * from unit_kompetensi');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        return view('dashboard.banding-mak02-insert',['asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('banding_mak02')->insert(
            ['id_asesi' => $request->{'id_asesi'},
            'id_asesor' => $request->{'id_asesor'},
            'id_unit' => $request->{'id_unit'},
            'alasan_banding'=>$request->{'alasan_banding'},
            'tanggal_asesmen'=>$request->{'tanggal_asesmen'},
            'pertanyaan1'=>$request->{'pertanyaan1'},
            'pertanyaan2'=>$request->{'pertanyaan2'},
            'pertanyaan3'=>$request->{'pertanyaan3'},
            ]
        ); 
        return redirect()->route('ujikom-banding.index')
        ->with('success','Data MAK02  Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $banding = DB::SELECT("SELECT b.*, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi
        FROM banding_mak02 b, asesi a, asesor asr, unit_kompetensi u 
        WHERE b.id_asesi=a.id and b.id_asesor=asr.id and b.id_unit=u.id and b.id=?",[$id]);
        
        return view('dashboard.banding-mak02-detail',['banding'=>$banding]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi');
        $unit = DB::select('SELECT * from unit_kompetensi');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $banding = DB::SELECT("SELECT b.*, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi
        FROM banding_mak02 b, asesi a, asesor asr, unit_kompetensi u 
        WHERE b.id_asesi=a.id and b.id_asesor=asr.id and b.id_unit=u.id and b.id=?",[$id]);
        
        return view('dashboard.banding-mak02-edit',['banding'=>$banding,'asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('banding_mak02')->where('id',$id)->update(
            ['id_asesi' => $request->{'id_asesi'},
            'id_asesor' => $request->{'id_asesor'},
            'id_unit' => $request->{'id_unit'},
            'alasan_banding'=>$request->{'alasan_banding'},
            'tanggal_asesmen'=>$request->{'tanggal_asesmen'},
            'pertanyaan1'=>$request->{'pertanyaan1'},
            'pertanyaan2'=>$request->{'pertanyaan2'},
            'pertanyaan3'=>$request->{'pertanyaan3'},
            ]
        ); 
        return redirect()->route('ujikom-banding.index')
        ->with('success','Data MAK02  Berhasil Diubah');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from banding_mak02 where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('ujikom-banding.index')
            ->with('error','Data MAK-02 tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('ujikom-banding.index')
            ->with('success','Data MAK-02 berhasil dihapus');
    }
}
