<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users= DB::table('users')->get();
        $count = sizeof($users);
        return view('dashboard.user-default',['users'=>$users,'count'=>$count]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('users')->insert(
            ['username' => $request->username,
            'password'=> Hash::make($request->password),
            'role' => $request->role
            ]
        );
   
        return redirect()->route('user.index')
                        ->with('success','Data User berhasil dibuat.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =DB::table('users')->where('id', $id)->first();
        return view('dashboard.user-edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request['password'] == null){
            DB::table('users')
            ->where('id', $id)
            ->update(
                ['username' =>$request['username'],
                'role' => $request['role']
                ]
        );
        }else{
            DB::table('users')
            ->where('id', $id)
            ->update(
                ['username' =>$request['username'],
                'password' => Hash::make($request['password']),
                'role' => $request['role']
                ]
            );
        }
        
        return redirect()->route('user.index')
        ->with('success','Data user berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from users where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('user.index')
                    ->with('error','Data user tidak berhasil dihapus karena diacu data lain');
        }

        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data master log user ",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('user.index')
                    ->with('success','Data  user berhasil dihapus');
    }

    public function checkOldPassword(Request $request){
        $password=  $request['old_password'];
        $username= $request['username'];
        $user = DB::table('users')->where('username',$username)->first();
        if(Hash::check($password, $user->password)) {
            return 'match';
        } else {
            return 'not match';
        }
    }
}
