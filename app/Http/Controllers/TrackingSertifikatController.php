<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrackingSertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.sertifikasi-tracking');
    }

    public function track(Request $request){
        $nomor_resi=$request['nomor_resi'];
        $data_sertifikat=DB::select('SELECT * From sertifikat s,sertifikat_pengiriman sp WHERE sp.id_sertifikat=s.id and nomor_resi=?',[$nomor_resi]);
        return view('dashboard.sertifikasi-tracking-result',['sertifikat'=>$data_sertifikat,'nomor_resi'=>$nomor_resi]);
    }
}
