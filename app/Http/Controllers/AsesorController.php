<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Kota;
use App\Instansi;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AsesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asesors= DB::select('SELECT A.*, K.nama_kota, I.nama_instansi FROM ASESOR A, instansi I, Kota K WHERE A.id_instansi=I.id and I.id_kota=K.id ORDER BY id');
        $count_ext_active= DB::table('asesor')
                            ->where('jenis_asesor', 'Eksternal')
                            ->where('is_active', 1)
                            ->count();
        $count_ext_inactive=DB::table('asesor')
                            ->where('jenis_asesor', 'Eksternal')
                            ->where('is_active', 0)
                            ->count();
        $count_int_active=DB::table('asesor')
                            ->where('jenis_asesor', 'Internal')
                            ->where('is_active', 1)
                            ->count();
        $count_int_inactive=DB::table('asesor')
                            ->where('jenis_asesor', 'Internal')
                            ->where('is_active', 0)
                            ->count();
                            
        return view('dashboard.asesor-default',['asesors'=>$asesors,'count_int_active'=>$count_int_active,'count_ext_active'=>$count_ext_active
        ,'count_int_inactive'=>$count_int_inactive,'count_ext_inactive'=>$count_ext_inactive]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kotas = Kota::all();
        $instansis=Instansi::all();
        return view('dashboard.asesor-new',['kotas'=>$kotas,'instansis'=>$instansis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        DB::table('asesor')
            ->insert(
                ['nomor_registrasi'=> $request->nomor_registrasi,
                'nama' => $request->nama,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nik' => $request->nik,
                'nip' => $request->nip,
                'npwp' => $request->npwp,
                'pangkat' => $request->pangkat,
                'golongan' => $request->golongan,
                'jabatan' => $request->jabatan,
                'id_instansi' => $request->id_instansi,
                'nomor_telepon' => $request->nomor_telepon,
                'email' => $request->email,
                'jenis_asesor' => $request->jenis_asesor,
                'masa_berlaku' => $request->masa_berlaku,
                'is_active' => $request->is_active,
                'keterangan' => 'RCC'
                ]
                
        );
        $id_asesor=DB::select('SELECT id FROM asesor ORDER BY id DESC LIMIT 1');
           
        for($i=1;$i<=$request->count_pekerjaan;$i++){
         
            DB::table('asesor_pekerjaan')->insert(
                ['id_asesor' => $id_asesor[0]->id,
                'id_instansi' => $request->{'id_instansi'.$i},
                'jabatan_prev' => $request->{'jabatan_prev'.$i},
                'periode' => $request->{'periode'.$i}
                
                ]
            );
            
        }
        // return $request->count_pekerjaan;
         //LOGGING ACTIVITY 
         $user_id= Auth::User()->id;
         $username= Auth::User()->username;
         DB::table('log_user')->insert(
             ['user_id' => $user_id,
             'activity' => $username." memasukan data asesor.",
             'timestamp' => Carbon::now()           
             ]
         );   

        return redirect()->route('asesor.index')
        ->with('success','Data asesor berhasil diperbaharui.');
     
          
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $asesor = DB::select('SELECT a.*,i.nama_instansi FROM asesor a, instansi i WHERE a.id_instansi=i.id and a.id=?',[$id]);
        $pekerjaan =DB::select('SELECT AP.*,I.*,K.nama_kota FROM asesor_pekerjaan AP, instansi I, kota K WHERE 
        ap.id_asesor=? and ap.id_instansi=I.id and I.id_kota=K.id',[$id]);
        $penugasan=DB::select('SELECT u.*,k.nama_kota FROM ujikom u, asesor_penugasan a,kota k where a.id_ujikom=u.id and u.id_kota=k.id and id_asesor=?',[$id]);
        $count = sizeof($asesor);
        return view('dashboard.asesor-detail',['count'=>$count,'asesor'=>$asesor,'pekerjaan'=>$pekerjaan,'penugasan'=>$penugasan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $instansis=Instansi::all();
        $asesor = DB::table('asesor')->where('id',$id)->first();
        $asesor_pekerjaan = DB::select('SELECT * FROM asesor_pekerjaan WHERE id_asesor=?',[$id]);
        $count_pekerjaan = sizeof($asesor_pekerjaan);
       
        return view('dashboard.asesor-edit',['count_pekerjaan'=>$count_pekerjaan,'asesor_pekerjaan'=>$asesor_pekerjaan,'asesor'=>$asesor,'instansis'=>$instansis,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        DB::table('asesor')
            ->where('id', $id)
            ->update(
                ['nama' => $request->nama,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'nik' => $request->nik,
                'nip' => $request->nip,
                'npwp' => $request->npwp,
                'pangkat' => $request->pangkat,
                'golongan' => $request->golongan,
                'jabatan' => $request->jabatan,
                'id_instansi' => $request->id_instansi,
                'nomor_telepon' => $request->nomor_telepon,
                'email' => $request->email,
                'jenis_asesor' => $request->jenis_asesor,
                'masa_berlaku' => $request->masa_berlaku,
                'is_active' => $request->is_active,
                'keterangan' => 'RCC'
                ]
                
        );
        for($i=1;$i<=$request->count_pekerjaan;$i++){
           $cek = sizeof(DB::SELECT('SELECT * FROM asesor_pekerjaan WHERE id=?',[$request->{'id_pekerjaan'.$i}])) ;
           if($cek>0){
                DB::table('asesor_pekerjaan')
                    ->where('id', $request->{'id_pekerjaan'.$i})
                    ->update(
                    ['id_instansi' => $request->{'id_instansi'.$i},
                    'jabatan_prev' => $request->{'jabatan_prev'.$i},
                    'periode' => $request->{'periode'.$i}
                    ]
                );
                // return $request;
            }else{
                DB::table('asesor_pekerjaan')->insert(
                    ['id_asesor' => $request->id,
                    'id_instansi' => $request->{'id_instansi'.$i},
                    'jabatan_prev' => $request->{'jabatan_prev'.$i},
                    'periode' => $request->{'periode'.$i}
                   
                    ]
                );
            }
        }
        // return $request->count_pekerjaan;
         //LOGGING ACTIVITY 
         $user_id= Auth::User()->id;
         $username= Auth::User()->username;
         DB::table('log_user')->insert(
             ['user_id' => $user_id,
             'activity' => $username." mengubah data asesor dengan id".$id.".",
             'timestamp' => Carbon::now()           
             ]
         );   
        return redirect()->route('asesor.index')
        ->with('success','Data asesor berhasil diperbaharui.');
        
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from asesor where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('asesor.index')
                             ->with('error','Data asesor ridak berhasil dihapus karena diacu data lain');
        }
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data asesor dengan id".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );   
        return redirect()->route('asesor.index')
                         ->with('success','Data asesi berhasil dihapus');
    }
    public function checkDuplicate(Request $request){
        $nomor_registrasi = $request['nomor_registrasi'];
        $data = DB::table("asesor")
        ->where('nomor_registrasi', $nomor_registrasi)
        ->count();
        if($data > 0)
        {
            return 'not_unique';
        }
        else
        {
            return 'unique';
        }
        return $nomor_registrasi;
       
    }
}
