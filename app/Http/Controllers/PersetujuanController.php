<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PersetujuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $persetujuan = DB::SELECT("SELECT p.id, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi, tanggal_asesmen, tempat_asesmen, bukti
        FROM persetujuan_mak3 p, asesi a, asesor asr, unit_kompetensi u 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_unit=u.id");
        $count=sizeof($persetujuan);
        return view('dashboard.persetujuan-mak03-default',['count'=>$count,'persetujuan'=>$persetujuan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $unit = DB::select('SELECT * from unit_kompetensi ORDER BY id');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        return view('dashboard.persetujuan-mak03-insert',['asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('persetujuan_mak3')->insert(
            ['id_asesi' => $request->{'id_asesi'},
            'id_asesor' => $request->{'id_asesor'},
            'id_unit' => $request->{'id_unit'},
            'bukti'=>$request->{'bukti'},
            'tanggal_asesmen'=>$request->{'tanggal_asesmen'},
            'tempat_asesmen'=>$request->{'tempat_asesmen'}
            
            ]
        ); 
        return redirect()->route('ujikom-persetujuan.index')
        ->with('success','Data MAK 03  Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $persetujuan = DB::SELECT("SELECT p.*, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi, tanggal_asesmen, tempat_asesmen, bukti
                        FROM persetujuan_mak3 p, asesi a, asesor asr, unit_kompetensi u 
                        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_unit=u.id and p.id=?",[$id]);
        return view('dashboard.persetujuan-mak03-detail',['persetujuan'=>$persetujuan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $unit = DB::select('SELECT * from unit_kompetensi ORDER BY id');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $persetujuan = DB::SELECT("SELECT p.*, a.nama as asesi, asr.nama as asesor, u.nama_unit_kompetensi, tanggal_asesmen, tempat_asesmen, bukti
        FROM persetujuan_mak3 p, asesi a, asesor asr, unit_kompetensi u 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_unit=u.id and p.id=?",[$id]);

        return view('dashboard.persetujuan-mak03-edit',['persetujuan'=>$persetujuan,'asesi'=>$asesi,'unit'=>$unit,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('persetujuan_mak3')->where('id',$id)->update(
            ['id_asesi' => $request->{'id_asesi'},
            'id_asesor' => $request->{'id_asesor'},
            'id_unit' => $request->{'id_unit'},
            'bukti'=>$request->{'bukti'},
            'tanggal_asesmen'=>$request->{'tanggal_asesmen'},
            'tempat_asesmen'=>$request->{'tempat_asesmen'}
            
            ]
        ); 
        return redirect()->route('ujikom-persetujuan.index')
        ->with('success','Data MAK 03  Berhasil Diperbarui');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from persetujuan_mak3 where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('ujikom-persetujuan.index')
            ->with('error','Data MAK-03 tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('ujikom-persetujuan.index')
            ->with('success','Data MAK-03 berhasil dihapus');
    }
}
