<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class SkemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skema = DB::table('skema')->get();
        $count = sizeof($skema);
        return view('dashboard.skema-default',['skema'=>$skema,'count'=>$count]);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('skema')->insert(
            ['nama_skema' =>$request['nama_skema']            
            ]
        );
        return redirect('/dashboard/skema-ujikom')->with('success','Data Skema Berhasil Dimasukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $skema = DB::table('skema')->where('id', $id)->first();
        $unit = DB::select('SELECT * FROM unit_kompetensi u where u.id NOT IN (SELECT id_unit FROM skema_unit WHERE id_skema=?) ORDER BY nomor_unit_kompetensi',[$id]);
        $unit_skema = DB::select('SELECT S.*,u.nomor_unit_kompetensi,u.nama_unit_kompetensi FROM skema_unit S, unit_kompetensi u where s.id_unit=u.id and s.id_skema=?',[$id]);
        $count_unit_skema = sizeof($unit_skema);
        return view('dashboard.skema-unit-show',['count_unit_skema'=>$count_unit_skema,'skema'=>$skema,'unit'=>$unit,'unit_skema'=>$unit_skema]);
    }

    public function insertUnitSkema(Request $request){
        $id_skema =$request['id_skema'];
        DB::table('skema_unit')->insert(
            ['id_skema' =>$request['id_skema'],
            'id_unit' =>$request['id_unit']
            ]
        );
        return redirect('/dashboard/skema-ujikom/'.$id_skema)->with('success','Data Unit Skema Berhasil Dimasukan');
    }

    public function destroyUnitSkema(Request $request, $id){
        DB::delete('delete from skema_unit where id=?',[$id]);
        return redirect('/dashboard/skema-ujikom/'.$request['id_skema_redirect'])->with('success','Data Unit Skema Berhasil dihapus');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skema = DB::table('skema')->where('id',$id)->first();
        return view('dashboard.skema-edit',['skema'=>$skema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('skema')
            ->where('id', $id)
            ->update(
                ['nama_skema' =>$request['nama_skema']            
                ]
        );
        return redirect()->route('skema-ujikom.index')
        ->with('success','Data skema berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from skema where id=?',[$id]);
            DB::delete('delete from skema_unit where id_skema=?',[$id]);
            DB::delete('delete from skema_ujikom where id_skema=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('skema-ujikom.index')
            ->with('error','Data Skema Kompetensi tidak Berhasil Dihapus');
        }

        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data skema dengan ID".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );
     
        return redirect()->route('skema-ujikom.index')
        ->with('success','Data Skema Kompetensi Berhasil Dihapus');
    
    }
}
