<?php

namespace App\Http\Controllers;

use App\Instansi;
use App\Kota;
use App\Provinsi;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class InstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instansis = DB::select('SELECT I.*, K.nama_kota FROM instansi I, kota K where i.id_kota=k.id');
        $kotas = Kota::all();
        $provinsis=Provinsi::all();
        $count = sizeof($instansis);
        
        return view('dashboard.instansi-default',['count'=>$count,'kotas'=>$kotas,'provinsis'=>$provinsis,'instansis'=>$instansis]);
    }

    function getKota($id){
        $kota = DB::table("kota")->where("id_provinsi",$id)->pluck("nama_kota","id");
        return json_encode($kota);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_instansi' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'nomor_telepon' => 'required',
            'kode_pos' => 'required',
            'id_kota' => 'required',
        ]);
  
        Instansi::create($request->all());
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." memasukan data master instansi",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('instansi.index')
                        ->with('success','Instansi created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instansi  $instansi
     * @return \Illuminate\Http\Response
     */
    public function show(Instansi $instansi)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instansi  $instansi
     * @return \Illuminate\Http\Response
     */
    public function edit(Instansi $instansi)
    {
        $kotas = Kota::all();
        $provinsi = Provinsi::all();
        $selected_prov = DB::select('SELECT * FROM provinsi WHERE id IN (SELECT id_provinsi FROM kota WHERE id=?) LIMIT 1',[$instansi->id_kota]);
        return view('dashboard.instansi-edit',compact('instansi','kotas','provinsi','selected_prov'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instansi  $instansi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instansi $instansi)
    {
        $request->validate([
            'nama_instansi' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'nomor_telepon' => 'required',
            'kode_pos' => 'required',
            'id_kota' => 'required',
        ]);
  
        $instansi->update($request->all());
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data master instansi dengan id ".$instansi->id,
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('instansi.index')
                        ->with('success','Data instansi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instansi  $instansi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instansi $instansi)
    {
        try{
            $instansi->delete();
        } catch(\Exception $e){
            return redirect()->route('instansi.index')
                        ->with('error','Data instansi tidak berhasil dihapus karena diacu oleh data lain');
        }
        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data master instansi dengan id ".$instansi->id,
            'timestamp' => Carbon::now()           
            ]
        );
  
        return redirect()->route('instansi.index')
                        ->with('success','Data provinsi berhasil dihapus');
    }

    public function checkDuplicate(Request $request){
        $nama_instansi = $request['nama_instansi'];
        $data = DB::table("instansi")
        ->where('nama_instansi', $nama_instansi)
        ->count();
        if($data > 0)
        {
            return 'not_unique';
        }
        else
        {
            return 'unique';
        }
       
    }
}
