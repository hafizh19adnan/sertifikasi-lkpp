<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Kota;
use App\Provinsi;
use App\Instansi;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AsesiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asesis= DB::select('SELECT A.id as id_asesi, A.*,K.nama_kota, I.nama_instansi FROM ASESI A, instansi I, Kota K WHERE 
                                  A.id_instansi=I.id and A.id_kota=K.id');
        $count_asesi= DB::table('asesi')->count();
        $count_instansi= DB::table('instansi')->count();
        
        return view('dashboard.asesi-default',['asesis'=>$asesis,'count_asesi'=>$count_asesi,'count_instansi'=>$count_instansi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kotas = Kota::all();
        $instansis=Instansi::all();
        $provinsis=Provinsi::all();
        $skema = DB::table('skema')->get();
       
        return view('dashboard.asesi-new',['skema'=>$skema,'kotas'=>$kotas,'instansis'=>$instansis,'provinsis'=>$provinsis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        DB::table('asesi')->insert(
            ['nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'npwp' => $request->npwp,
            'pangkat' => $request->pangkat,
            'golongan' => $request->golongan,
            'jabatan' => $request->jabatan,
            'id_instansi' => $request->id_instansi,
            'alamat' => $request->alamat,
            'id_kota' => $request->id_kota,
            'email' => $request->email,
            'nomor_telepon' => $request->nomor_telepon
            ]
        );
        $id_asesi=DB::select('SELECT id FROM asesi ORDER BY id DESC LIMIT 1');
 
        //insert data pendidikan
        for($i=1;$i<=$request->count_edu;$i++){
            DB::table('asesi_pendidikan')->insert(
                ['id_asesi' => $id_asesi[0]->id,
                'strata' => $request->{'strata'.$i},
                'nama_lembaga' => $request->{'nama_lembaga'.$i},
                'jurusan' => $request->{'jurusan'.$i},
                'tahun_lulus' => $request->{'tahun_lulus'.$i},
                'gelar' => $request->{'gelar'.$i},
            
                ]
            );
        }
        //insert data pekerjaan
        for($i=1;$i<=$request->count_work;$i++){
                DB::table('asesi_pekerjaan')->insert(
                    ['id_asesi' => $id_asesi[0]->id,
                    'id_instansi' => $request->{'id_instansi'.$i},
                    'jabatan_prev' => $request->{'jabatan_prev'.$i},
                    'periode' => $request->{'periode'.$i}
                
                    ]
                );
            
        }
        //insert data pengalaman
        for($i=1;$i<=$request->count_exp;$i++){
            
                DB::table('asesi_pengalaman')->insert(
                    ['id_asesi' => $id_asesi[0]->id,
                    'deskripsi_pengalaman' => $request->{'deskripsi_pengalaman'.$i},
                    'jabatan' => $request->{'jabatan'.$i},
                    'masa_jabatan' => $request->{'masa_jabatan'.$i},
                    'nomor_sk' => $request->{'nomor_sk'.$i}
                    ]
                );
            
        }

        //insert skema

        //Condition 1: Ada ceklist baru
        for($i=0;$i<sizeof($request->id_skema);$i++){
          
            DB::table('asesi_skema')->insert(
                ['id_asesi' => $id_asesi[0]->id ,
                'id_skema' => $request['id_skema'][$i]
            ]);
            $unit= DB::table('skema_unit')->where('id_skema', '=', $request['id_skema'][$i])->get();
            for($n=0;$n<sizeof($unit);$n++){
                DB::table('asesi_unit')->insert(
                ['id_asesi' => $id_asesi[0]->id,
                'id_unit' =>  $unit[$n]->id_unit
                ]);
            
            }
        
        }
        
        for($l=1;$l<=$request->count_pendukung;$l++){
            DB::table('asesi_kompetensi_pendukung')->insert(
                ['id_asesi' => $id_asesi[0]->id,
                'deskripsi_kompetensi' => $request->{'deskripsi_kompetensi'.$l},
                'bukti' => $request->{'bukti'.$l}
                
                ]
            );    
                
        }
        for($l=1;$l<=$request->count_kelengkapan;$l++){
            DB::table('asesi_kelengkapan')->insert(
                ['id_asesi' => $id_asesi[0]->id,
                'nama_berkas' => $request->{'nama_berkas'.$l},
                'bukti' => $request->{'bukti_lengkap'.$l}
                ]
            );    
                
        }
        
        
        //   LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menambahkan data asesi.",
            'timestamp' => Carbon::now()           
            ]
        ); 
        return redirect()->route('asesi.index')
        ->with('success','Data Asesi  Berhasil Dimasukan');
        
       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asesi= DB::select('SELECT A.id as id_asesi, A.*,K.nama_kota, I.nama_instansi FROM ASESI A, instansi I, Kota K WHERE A.id_instansi=I.id and A.id_kota=K.id and A.id=?',[$id]);
        $pekerjaan=DB::select('SELECT AP.*,I.*,K.nama_kota FROM asesi_pekerjaan AP, instansi I, kota K WHERE 
                                            ap.id_asesi=? and ap.id_instansi=I.id and I.id_kota=K.id',[$id]);
        $pendidikan=DB::select('SELECT * FROM ASESI A, asesi_pendidikan AP WHERE A.id=AP.id_asesi and A.id=?',[$id]);
        $asesi_pengalaman=DB::select('SELECT AP.* FROM ASESI A, asesi_pengalaman AP WHERE A.id=? and A.id=AP.id_asesi',[$id]);
        $pendukung=DB::select('SELECT * FROM asesi_kompetensi_pendukung WHERE id_asesi=?',[$id]);
        $ujikom=DB::select('SELECT ua.id,ua.nomor_registrasi,ua.hasil,u.judul, nama_skema from skema s, ujikom_asesi_hasil ua, ujikom u where ua.id_skema=s.id and ua.id_ujikom = u.id and id_asesi=?',[$id]);
        $unit_kompetensi=DB::select('SELECT u.nomor_unit_kompetensi, u.nama_unit_kompetensi, au.hasil FROM asesi_unit au, unit_kompetensi u WHERE au.id_unit=u.id and au.id_asesi=?',[$id]);
        $skema=DB::select('SELECT * FROM asesi_skema ask, skema s WHERE ask.id_skema=s.id and ask.id_asesi=?',[$id]);
        $kelengkapan = DB::SELECT('SELECT * FROM asesi a, asesi_kelengkapan ak WHERE ak.id_asesi=a.id and ak.id_asesi=?',[$id]);
        $apl02 =  DB::SELECT('SELECT * FROM asesi a, asesi_apl02 ak, asesor asr, skema s WHERE ak.id_asesor=asr.id and ak.id_skema=s.id and ak.id_asesi=a.id and ak.id_asesi=?',[$id]);


        return view('dashboard.asesi-detail')->with(['apl02'=>$apl02,'kelengkapan'=>$kelengkapan,'skema'=>$skema,'unit_kompetensi'=>$unit_kompetensi,'asesi'=>$asesi,'pendidikan'=>$pendidikan,'pekerjaan'=>$pekerjaan,
                                                        'pengalaman'=>$asesi_pengalaman,'pendukung'=>$pendukung,'ujikom'=>$ujikom]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provinsis=Provinsi::all();
        $kotas = Kota::all();
        $instansis=Instansi::all();
        $asesi= DB::select('SELECT A.*,K.nama_kota FROM asesi A, kota K WHERE A.id_kota=K.id and A.id=?',[$id]);
        $selected_prov = DB::select('SELECT * FROM provinsi WHERE id IN (SELECT id_provinsi FROM kota WHERE id=?) LIMIT 1',[$asesi[0]->id_kota]);
        $skema = DB::select('SELECT s.id, s.nama_skema FROM skema s');
        $skema2 = DB::select('SELECT s.id, s.nama_skema FROM skema s');
        
        $skema_asesi = DB::select('SELECT * FROM skema s, asesi_skema ask WHERE ask.id_skema=s.id and ask.id_asesi=?',[$id]);

        $asesi_pendidikan = DB::select('SELECT * FROM asesi_pendidikan WHERE id_asesi=?',[$id]);
        $count_pendidikan = sizeof($asesi_pendidikan);
        $asesi_pekerjaan = DB::select('SELECT * FROM asesi_pekerjaan WHERE id_asesi=?',[$id]);
        $count_pekerjaan = sizeof($asesi_pekerjaan);
        $asesi_pengalaman = DB::select('SELECT * FROM asesi_pengalaman WHERE id_asesi=?',[$id]);
        $count_pengalaman = sizeof($asesi_pengalaman);
        $asesi_pendukung = DB::select('SELECT * FROM asesi_kompetensi_pendukung WHERE id_asesi=?',[$id]);
        $count_pendukung = sizeof($asesi_pendukung);


        $ujikom = DB::select('SELECT id,judul FROM ujikom');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $skema_unit = DB::select('SELECT * FROM skema_unit su, unit_kompetensi u WHERE su.id_unit=u.id and id_skema=1');
        $unit_pertanyaan = DB::select('SELECT * FROM unit_pertanyaan');
        
        return view('dashboard.asesi-edit2')->with(['skema2'=>$skema2,'ujikom'=>$ujikom,'unit'=>$skema_unit,'asesor'=>$asesor,'unit_pertanyaan'=>$unit_pertanyaan,'skema'=>$skema,'selected_prov'=>$selected_prov,
                                                    'skema_asesi'=>$skema_asesi,'provinsis'=>$provinsis,'asesi'=>$asesi,'kotas'=>$kotas,
                                                    'asesi_pendukung'=>$asesi_pendukung,'asesi_pengalaman'=>$asesi_pengalaman,'asesi_pendidikan'=>$asesi_pendidikan,
                                                    'count_pendidikan'=>$count_pendidikan,'count_pekerjaan'=>$count_pekerjaan,'count_pengalaman'=>$count_pengalaman,
                                                    'count_pendukung'=>$count_pendukung,'asesi_pekerjaan'=>$asesi_pekerjaan,'instansis'=>$instansis,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        DB::table('asesi')
            ->where('id', $id)
            ->update(
            ['nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'npwp' => $request->npwp,
            'pangkat' => $request->pangkat,
            'golongan' => $request->golongan,
            'jabatan' => $request->jabatan,
            'id_instansi' => $request->id_instansi,
            'alamat' => $request->alamat,
            'id_kota' => $request->id_kota,
            'email' => $request->email,
            'nomor_telepon' => $request->nomor_telepon
            ]
        );
        
        //edit data pendidikan
        for($i=1;$i<=$request->count_pendidikan;$i++){
            $cek = sizeof(DB::SELECT('SELECT * FROM asesi_pendidikan WHERE id=?',[$request->{'id_pendidikan'.$i}])) ;
            if($cek>0){
                DB::table('asesi_pendidikan')
                    ->where('id', $request->{'id_pendidikan'.$i})
                    ->update(
                    ['strata' => $request->{'strata'.$i},
                    'nama_lembaga' => $request->{'nama_lembaga'.$i},
                    'jurusan' => $request->{'jurusan'.$i},
                    'tahun_lulus' => $request->{'tahun_lulus'.$i},
                    'gelar' => $request->{'gelar'.$i},
                   
                    ]
                );
            }else{
                DB::table('asesi_pendidikan')->insert(
                    ['id_asesi' => $request->id,
                    'strata' => $request->{'strata'.$i},
                    'nama_lembaga' => $request->{'nama_lembaga'.$i},
                    'jurusan' => $request->{'jurusan'.$i},
                    'tahun_lulus' => $request->{'tahun_lulus'.$i},
                    'gelar' => $request->{'gelar'.$i},
                   
                    ]
                );
            }
        }
        //edit data pekerjaan
        for($i=1;$i<=$request->count_pekerjaan;$i++){
            $cek = sizeof(DB::SELECT('SELECT * FROM asesi_pekerjaan WHERE id=?',[$request->{'id_pekerjaan'.$i}])) ;
           if($cek>0){
                DB::table('asesi_pekerjaan')
                    ->where('id', $request->{'id_pekerjaan'.$i})
                    ->update(
                    ['id_instansi' => $request->{'id_instansi'.$i},
                    'jabatan_prev' => $request->{'jabatan_prev'.$i},
                    'periode' => $request->{'periode'.$i}
                   
                    ]
                );
            }else{
                DB::table('asesi_pekerjaan')->insert(
                    ['id_asesi' => $request->id,
                    'id_instansi' => $request->{'id_instansi'.$i},
                    'jabatan_prev' => $request->{'jabatan_prev'.$i},
                    'periode' => $request->{'periode'.$i}
                   
                    ]
                );
            }
        }
        //edit data pengalaman
        for($i=1;$i<=$request->count_pengalaman;$i++){
            $cek = sizeof(DB::SELECT('SELECT * FROM asesi_pengalaman WHERE id=?',[$request->{'id_pengalaman'.$i}])) ;
           if($cek>0){
                DB::table('asesi_pengalaman')
                    ->where('id', $request->{'id_pengalaman'.$i})
                    ->update(
                    ['deskripsi_pengalaman' => $request->{'deskripsi_pengalaman'.$i},
                    'jabatan' => $request->{'jabatan'.$i},
                    'masa_jabatan' => $request->{'masa_jabatan'.$i},
                    'nomor_sk' => $request->{'nomor_sk'.$i}
                    ]
                );
            }else{
                DB::table('asesi_pengalaman')->insert(
                    ['id_asesi' => $request->id,
                    'deskripsi_pengalaman' => $request->{'deskripsi_pengalaman'.$i},
                    'jabatan' => $request->{'jabatan'.$i},
                    'masa_jabatan' => $request->{'masa_jabatan'.$i},
                    'nomor_sk' => $request->{'nomor_sk'.$i}
                    ]
                );
            }
        }

        //edit skema

        //Condition 1: Ada ceklist baru
        for($i=0;$i<sizeof($request->id_skema);$i++){
            $cek = sizeof(DB::select('SELECT * FROM asesi_skema WHERE id_asesi=? and id_skema=?',[$request->id,$request['id_skema'][$i]]));
            if($cek==0){
                DB::table('asesi_skema')->insert(
                    ['id_asesi' => $request->id ,
                    'id_skema' => $request['id_skema'][$i]
                ]);
                $unit= DB::table('skema_unit')->where('id_skema', '=', $request['id_skema'][$i])->get();
                for($n=0;$n<sizeof($unit);$n++){
                    $check_unit = DB::table('asesi_unit')->where('id_asesi', '=', $request->id)->where('id_unit', '=', $unit[$n]->id_unit)->count();
                    if($check_unit==0){
                        DB::table('asesi_unit')->insert(
                        ['id_asesi' => $request->id  ,
                        'id_unit' =>  $unit[$n]->id_unit
                        ]);
                    }
                }
            }
        }
        $asesi_skema = DB::select('SELECT * FROM asesi_skema WHERE id_asesi=?',[$request->id]);
        for($i=0;$i<sizeof($asesi_skema);$i++){
            $count=0;
            for($j=0;$j<sizeof($request->id_skema);$j++){
                if($request['id_skema'][$j]==$asesi_skema[$i]->id_skema){
                    $count++;
                }
            }
           
            if($count==0){
                DB::delete('delete from asesi_skema where id_asesi=? and id_skema=?',[$request->id,$asesi_skema[$i]->id_skema]);
            }
        }
        
        // //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data asesi dengan ID ".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('asesi.index')
        ->with('success','Data asesi berhasil diperbaharui.');
        // return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try{
            DB::delete('delete from asesi where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('asesi.index')
            ->with('error','Data asesi tidak berhasil dihapus karena diacu data lain');
        }

        
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data asesi dengan id".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );   
        return redirect()->route('asesi.index')
        ->with('success','Data asesi berhasil dihapus');
    }
}
