<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class KeputusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keputusan = DB::SELECT('SELECT p.id, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, tempat, rekomendasi_asesor
        FROM asesmen_keputusan_feedback p, asesi a, asesor asr, skema s 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id');
        $count = sizeof($keputusan);
        return view('dashboard.keputusan-mak04-default',['count'=>$count,'keputusan'=>$keputusan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $skema = DB::select('SELECT * from skema');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        return view('dashboard.keputusan-mak04-insert',['asesi'=>$asesi,'skema'=>$skema,'asesor'=>$asesor]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('asesmen_keputusan_feedback')->insert(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'tanggal' => $request['tanggal'],
            'tempat' => $request['tempat'],
            'feedback_pencapaian' => $request['feedback_pencapaian'],
            'identifikasi_kesenjangan' => $request['identifikasi_kesenjangan'],
            'saran_tindak_lanjut' => $request['saran_tindak_lanjut'],
            'rekomendasi_asesor' => $request['rekomendasi_asesor']
            ]
        );
        $id_keputusan=DB::select('SELECT id FROM asesmen_keputusan_feedback ORDER BY id DESC LIMIT 1');
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('asesmen_keputusan_feedback_detail')->insert(
                ['id_keputusan' =>$id_keputusan[0]->id,
                'id_unit' => $request['id_unit_kompetensi'.$i],
                'bukti_langsung' => $request['bukti_langsung'.$i],
                'bukti_tidak_langsung' => $request['bukti_tidak_langsung'.$i],
                'bukti_tambahan' => $request['bukti_tambahan'.$i],
                'keputusan' => $request['keputusan'.$i]
                ]
            );
        }
        return redirect()->route('asesmen-keputusan.index')
        ->with('success','Data MAK04 Berhasil Dimasukan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $skema = DB::select('SELECT * from skema');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $keputusan = DB::SELECT('SELECT p.*, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, tempat, rekomendasi_asesor
        FROM asesmen_keputusan_feedback p, asesi a, asesor asr, skema s 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id and p.id=?',[$id]);
        $detail = DB::SELECT('SELECT * FROM asesmen_keputusan_feedback_detail ak, unit_kompetensi u WHERE ak.id_unit=u.id and id_keputusan=?',[$id]);
        return view('dashboard.keputusan-mak04-detail',['detail'=>$detail,'keputusan'=>$keputusan,'asesi'=>$asesi,'skema'=>$skema,'asesor'=>$asesor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT id,nama FROM asesi WHERE id NOT IN (SELECT id_asesi FROM asesi_apl02)');
        $skema = DB::select('SELECT * from skema');
        $asesor = DB::select('SELECT a.id,a.nama FROM asesor a');
        $keputusan = DB::SELECT('SELECT p.*, a.nama as asesi, asr.nama as asesor, s.nama_skema, tanggal, tempat, rekomendasi_asesor
        FROM asesmen_keputusan_feedback p, asesi a, asesor asr, skema s 
        WHERE p.id_asesi=a.id and p.id_asesor=asr.id and p.id_skema=s.id and p.id=?',[$id]);
        $detail = DB::SELECT('SELECT ak.*,u.id as id_unit,u.nomor_unit_kompetensi,u.nama_unit_kompetensi FROM asesmen_keputusan_feedback_detail ak, unit_kompetensi u WHERE ak.id_unit=u.id and id_keputusan=?',[$id]);
        return view('dashboard.keputusan-mak04-edit',['count'=>count($detail),'detail'=>$detail,'keputusan'=>$keputusan,'asesi'=>$asesi,'skema'=>$skema,'asesor'=>$asesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('asesmen_keputusan_feedback')->where('id',$id)->update(
            ['id_asesi' => $request['id_asesi'],
            'id_asesor' => $request['id_asesor'],
            'id_skema' => $request['id_skema'],
            'tanggal' => $request['tanggal'],
            'tempat' => $request['tempat'],
            'feedback_pencapaian' => $request['feedback_pencapaian'],
            'identifikasi_kesenjangan' => $request['identifikasi_kesenjangan'],
            'saran_tindak_lanjut' => $request['saran_tindak_lanjut'],
            'rekomendasi_asesor' => $request['rekomendasi_asesor']
            ]
        );
        for($i=0;$i<$request['jumlah_unit'];$i++){
            DB::table('asesmen_keputusan_feedback_detail')->where('id', $request['id_detail'.$i])
                                                          ->update(
                ['id_unit' => $request['id_unit_kompetensi'.$i],
                'bukti_langsung' => $request['bukti_langsung'.$i],
                'bukti_tidak_langsung' => $request['bukti_tidak_langsung'.$i],
                'bukti_tambahan' => $request['bukti_tambahan'.$i],
                'keputusan' => $request['keputusan'.$i]
                ]
            );
        }
        return redirect()->route('asesmen-keputusan.index')
        ->with('success','Data MAK04 Berhasil Diubah'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from asesmen_keputusan_feedback_detail where id_keputusan=?',[$id]);
            DB::delete('delete from asesmen_keputusan_feedback where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('asesmen-keputusan.index')
            ->with('error','Data MAK-04 tidak berhasil dihapus karena diacu data lain');
        }
        return redirect()->route('asesmen-keputusan.index')
            ->with('success','Data MAK-04 berhasil dihapus');
    }
}
