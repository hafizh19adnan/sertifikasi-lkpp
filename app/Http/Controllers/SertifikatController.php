<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class SertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sertifikat = DB::select('SELECT S.*,A.nama,U.judul, U.tanggal_mulai, U.tanggal_selesai FROM sertifikat S, asesi A, ujikom U 
                        WHERE S.id_ujikom=U.id and S.id_asesi=A.id');
        return view('dashboard.sertifikasi-default',['sertifikat'=>$sertifikat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asesi = DB::select('SELECT asesi.id,asesi.nama FROM asesi');
        $ujikom = DB::select('SELECT ujikom.id,ujikom.judul FROM ujikom');
        return view('dashboard.sertifikasi-create',['asesi'=>$asesi,'ujikom'=>$ujikom]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('sertifikat')->insert(
            ['id_asesi' =>$request['id_asesi'],
            'id_ujikom' => $request['id_ujikom'],
            'nomor_sertifikat' => $request['nomor_sertifikat'],
            'catatan' => $request['catatan'],
            'tanggal_keluar' => $request['tanggal_keluar'],
            'masa_berlaku' => $request['masa_berlaku']
            
            ]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." memasukan data sertifikat",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect('/dashboard/sertifikat')->with('success','Data Sertifikat Berhasil Dimasukan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sertifikat = DB::select('SELECT s.*,a.nama,u.judul,u.tanggal_mulai, u.tanggal_selesai FROM sertifikat s, asesi a, ujikom u where s.id_asesi=a.id and s.id_ujikom=u.id and s.id=?',[$id]);
        $pengiriman = DB::select('SELECT * FROM sertifikat_pengiriman WHERE id_sertifikat=?',[$id]);
        return view('dashboard.sertifikasi-detail',['sertifikat'=>$sertifikat,'pengiriman'=>$pengiriman]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asesi = DB::select('SELECT asesi.id,asesi.nama FROM asesi');
        $ujikom = DB::select('SELECT ujikom.id,ujikom.judul FROM ujikom');
        $sertifikat = DB::select('SELECT * FROM sertifikat where id=?',[$id]);
        return view('dashboard.sertifikasi-edit',['asesi'=>$asesi,'ujikom'=>$ujikom,'sertifikat'=>$sertifikat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('sertifikat')
            ->where('id', $id)
            ->update(
                ['id_asesi' =>$request['id_asesi'],
                'id_ujikom' => $request['id_ujikom'],
                'nomor_sertifikat' => $request['nomor_sertifikat'],
                'catatan' => $request['catatan'],
                'tanggal_keluar' => $request['tanggal_keluar'],
                'masa_berlaku' => $request['masa_berlaku']
                ]
        );
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." mengubah data sertifikat dengan ID".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('sertifikat.index')
        ->with('success','Data sertifikat berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::delete('delete from sertifikat where id=?',[$id]);
        }catch(\Exception $e){
            return redirect()->route('sertifikat.index')
                    ->with('success','Data  sertifikat tidak berhasil dihapus karena diacu data lain');
        }   
        //LOGGING ACTIVITY 
        $user_id= Auth::User()->id;
        $username= Auth::User()->username;
        DB::table('log_user')->insert(
            ['user_id' => $user_id,
            'activity' => $username." menghapus data sertifikat dengan ID".$id.".",
            'timestamp' => Carbon::now()           
            ]
        );
        return redirect()->route('sertifikat.index')
                    ->with('success','Data  sertifikat berhasil dihapus');
    }
  

    public function export(){
         $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=Rekap Data Sertifikat BNSP.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $sertifikat=DB::select('SELECT a.nama,a.nik, a.tempat_lahir,a.tanggal_lahir,a.jenis_kelamin,a.alamat,a.id_kota, k.id_provinsi,a.nomor_telepon,a.email,s.nama_skema, u.tanggal_mulai,u.tuk,asr.nomor_registrasi,a.id_instansi, uh.hasil
                                FROM asesi a, asesor asr, ujikom_asesi_hasil uh, ujikom u, kota k, provinsi p, skema s
                                WHERE uh.id_asesi=a.id and uh.id_asesor = asr.id and uh.id_ujikom=u.id and uh.id_skema=s.id and a.id_kota=k.id and k.id_provinsi=p.id');
        $columns = array('NAMA ASESI','NIK','TEMPAT LAHIR','TANGGAL LAHIR','JENIS KELAMIN','TEMPAT TINGGAL','KODE KAB/KOTA','KODE PROVINSI','TELP','EMAIL','KODE PENDIDIKAN','KODE PEKERJAAN','KODE SKEMA','TANGGAL UJI','KODE TUK','NOMOR REGISTRASI ASESOR','KODE KEMENTRIAN','K/BK');

        $callback = function() use ($sertifikat, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($sertifikat as $sertifikat) {
                
                fputcsv($file, array($sertifikat->nama,$sertifikat->nik,$sertifikat->tempat_lahir,$sertifikat->tanggal_lahir,$sertifikat->jenis_kelamin,$sertifikat->alamat,
                $sertifikat->id_kota,$sertifikat->id_provinsi,$sertifikat->nomor_telepon,$sertifikat->email,'X','X',$sertifikat->nama_skema,$sertifikat->tanggal_mulai,$sertifikat->tuk,$sertifikat->nomor_registrasi,$sertifikat->id_instansi,$sertifikat->hasil));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
    public function exportByUjikom($id){
        $headers = array(
           "Content-type" => "text/csv",
           "Content-Disposition" => "attachment; filename=Rekap Data Sertifikat BNSP.csv",
           "Pragma" => "no-cache",
           "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
           "Expires" => "0"
       );

       $sertifikat=DB::select('SELECT a.nama,a.nik, a.tempat_lahir,a.tanggal_lahir,a.jenis_kelamin,a.alamat,a.id_kota, k.id_provinsi,a.nomor_telepon,a.email,s.nama_skema, u.tanggal_mulai,u.tuk,asr.nomor_registrasi,a.id_instansi, uh.hasil
                               FROM asesi a, asesor asr, ujikom_asesi_hasil uh, ujikom u, kota k, provinsi p, skema s
                               WHERE uh.id_asesi=a.id and uh.id_asesor = asr.id and uh.id_ujikom=u.id and uh.id_skema=s.id and a.id_kota=k.id and k.id_provinsi=p.id and uh.id_ujikom=?',[$id]);
       $columns = array('NAMA ASESI','NIK','TEMPAT LAHIR','TANGGAL LAHIR','JENIS KELAMIN','TEMPAT TINGGAL','KODE KAB/KOTA','KODE PROVINSI','TELP','EMAIL','KODE PENDIDIKAN','KODE PEKERJAAN','KODE SKEMA','TANGGAL UJI','KODE TUK','NOMOR REGISTRASI ASESOR','KODE KEMENTRIAN','K/BK');

       $callback = function() use ($sertifikat, $columns)
       {
           $file = fopen('php://output', 'w');
           fputcsv($file, $columns);

           foreach($sertifikat as $sertifikat) {
               
               fputcsv($file, array($sertifikat->nama,$sertifikat->nik,$sertifikat->tempat_lahir,$sertifikat->tanggal_lahir,$sertifikat->jenis_kelamin,$sertifikat->alamat,
               $sertifikat->id_kota,$sertifikat->id_provinsi,$sertifikat->nomor_telepon,$sertifikat->email,'X','X',$sertifikat->nama_skema,$sertifikat->tanggal_mulai,$sertifikat->tuk,$sertifikat->nomor_registrasi,$sertifikat->id_instansi,$sertifikat->hasil));
           }
           fclose($file);
       };
       return Response::stream($callback, 200, $headers);
   }
}
