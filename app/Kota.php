<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'kota';
    public $timestamps = false;
    protected $fillable = ['nama_kota', 'id_provinsi'];
}
