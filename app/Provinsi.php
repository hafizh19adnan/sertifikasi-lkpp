<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    public $timestamps = false;
    public $table='provinsi';
    protected $fillable = [
        'nama_provinsi'
    ];
}
