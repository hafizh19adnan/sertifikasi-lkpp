<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUjikomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skema', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_skema');
        });

        Schema::create('unit_kompetensi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nomor_unit_kompetensi');
            $table->string('nama_unit_kompetensi');
        });
        Schema::create('skema_unit', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_skema')->unsigned();
            $table->integer('id_unit')->unsigned();
            
            $table->foreign('id_skema')->references('id')->on('skema');
            $table->foreign('id_unit')->references('id')->on('unit_kompetensi');
        });
        Schema::create('skema_ujikom', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->integer('id_skema')->unsigned();
            
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
            $table->foreign('id_skema')->references('id')->on('skema_ujikom');
        });
        Schema::create('asesi_skema', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_skema')->unsigned();
            $table->string('hasil')->nullable();

            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_skema')->references('id')->on('skema');
        });
        Schema::create('asesi_unit', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_unit')->unsigned();
            $table->string('hasil')->nullable();

            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_unit')->references('id')->on('unit_kompetensi');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ujikom');
        Schema::dropIfExists('skema_ujikom');
        Schema::dropIfExists('asesi_skema');
        Schema::dropIfExists('skema_unit_kompetensi');
    }
}
