<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsesiUjikom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asesi_ujikom', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_ujikom')->unsigned();

            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asesi_ujikom');
    }
}
