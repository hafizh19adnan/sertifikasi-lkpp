<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesUserProvinsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',100)->unique();
            $table->string('password');
            $table->string('role');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('log_user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('activity');
            $table->datetime('timestamp');

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('provinsi', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->string('nama_provinsi');  

            $table->primary('id');
            
        });
        Schema::create('kota', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('id_provinsi')->unsigned();
            $table->string('nama_kota');
            
            $table->foreign('id_provinsi')->references('id')->on('provinsi');
            $table->primary('id');
            
        });

        Schema::create('instansi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_instansi');
            $table->string('alamat');
            $table->string('email');
            $table->string('nomor_telepon');
            $table->integer('kode_pos');
            $table->integer('id_kota')->unsigned();
            //Table Constraints
 
            $table->foreign('id_kota')->references('id')->on('kota');

        });
        Schema::create('asesi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama');
            $table->char('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('nik')->nullable();
            $table->string('nip')->nullable();
            $table->string('npwp')->nullable();
            $table->string('pangkat');
            $table->string('golongan');
            $table->string('jabatan');
            $table->integer('id_instansi')->unsigned();
            $table->string('alamat');
            $table->integer('id_kota')->unsigned();
            $table->string('email');
            $table->string('nomor_telepon');
            
            //Table Constraints
    
            $table->foreign('id_kota')->references('id')->on('kota');
            $table->foreign('id_instansi')->references('id')->on('instansi');

        });
        Schema::create('asesi_pendidikan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->string('strata');
            $table->string('nama_lembaga');
            $table->string('jurusan');
            $table->integer('tahun_lulus');
            $table->string('gelar')->nullable();
            //Table Constraints
 
            $table->foreign('id_asesi')->references('id')->on('asesi');

        });
        Schema::create('asesi_pekerjaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_instansi')->unsigned();
            $table->string('jabatan_prev');
            $table->string('periode');
            
            //Table Constraints
            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_instansi')->references('id')->on('instansi');
        });

        Schema::create('asesi_pengalaman', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->string('deskripsi_pengalaman');
            $table->string('jabatan');
            $table->string('masa_jabatan');
            $table->string('nomor_sk');
          
            //Table Constraints
            $table->foreign('id_asesi')->references('id')->on('asesi');
           
        });
        Schema::create('asesi_kompetensi_pendukung', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->string('deskripsi_kompetensi');
            $table->string('bukti');
                    
            //Table Constraints
            $table->foreign('id_asesi')->references('id')->on('asesi');
           
        });
        Schema::create('asesor', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nomor_registrasi',50)->unique();
            $table->string('nama');
            $table->char('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('nik')->nullable();
            $table->string('nip')->nullable();
            $table->string('npwp')->nullable();
            $table->string('pangkat');
            $table->string('golongan');
            $table->string('jabatan');
            $table->integer('id_instansi')->unsigned();
            $table->string('nomor_telepon');
            $table->string('email');
            $table->string('jenis_asesor');
            $table->string('masa_berlaku');
            $table->boolean('is_active');
            $table->string('keterangan')->nullable();
        });
        
        Schema::create('asesor_pekerjaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesor')->unsigned();
            $table->integer('id_instansi')->unsigned();
            $table->string('jabatan_prev')->nullable();
            $table->string('periode',30)->nullable();

            $table->foreign('id_asesor')->references('id')->on('asesor');
            $table->foreign('id_instansi')->references('id')->on('instansi');
        });
        Schema::create('ujikom', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('judul');
            $table->string('tuk');
            $table->datetime('tanggal_pelaksanaan');
            $table->string('tempat_uji');
            $table->integer('id_kota')->unsigned();
            $table->string('skema_kompetensi');

            $table->foreign('id_kota')->references('id')->on('kota');
        });
        Schema::create('ujikom_unit_kompetensi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->string('nomor_unit_kompetensi');
            $table->string('nama_unit_kompetensi');

            $table->foreign('id_ujikom')->references('id')->on('ujikom');
        });
        
        Schema::create('ujikom_asesi_hasil', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_asesor')->unsigned();
            $table->string('nomor_registrasi');
            $table->string('aspek_positif')->nullable();
            $table->string('aspek_negatif')->nullable();
            $table->char('hasil');
            $table->string('keterangan')->nullable();

            $table->foreign('id_ujikom')->references('id')->on('ujikom');
            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_asesor')->references('id')->on('asesor');
        });
        Schema::create('ujikom_hasil_detail', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_hasil')->unsigned();
            $table->integer('id_unit_kompetensi')->unsigned();
            $table->char('hasil');

            $table->foreign('id_hasil')->references('id')->on('ujikom_asesi_hasil');
            $table->foreign('id_unit_kompetensi')->references('id')->on('ujikom_unit_kompetensi');
        });
        Schema::create('asesor_penugasan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_asesor')->unsigned();
            $table->integer('id_ujikom')->unsigned();

            $table->foreign('id_asesor')->references('id')->on('asesor');
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
        });
        Schema::create('sertifikat', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->string('nomor_sertifikat');
            $table->date('tanggal_keluar');
            $table->date('masa_berlaku');
            
            $table->string('catatan')->nullable();

            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
            
        });
        Schema::create('sertifikat_pengiriman', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_sertifikat')->unsigned();
            $table->datetime('tanggal_kirim');
            $table->string('alamat_pengiriman');
            $table->string('nomor_telepon_pic');
            $table->string('nomor_resi');

            $table->foreign('id_sertifikat')->references('id')->on('sertifikat');
        });
        
        
        Schema::create('ujikom_feedback', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->string('deskripsi_feedback')->nullable();
           
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
            $table->foreign('id_asesi')->references('id')->on('asesi');
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('users');
        Schema::dropIfExists('asesi_pengalaman');
        Schema::dropIfExists('asesi_pendidikan');
        Schema::dropIfExists('asesi_pekerjaan');
        Schema::dropIfExists('asesi_kompetensi_pendukung');
        Schema::dropIfExists('sertifikat_pengiriman');
        Schema::dropIfExists('log_user');
        Schema::dropIfExists('asesor_penugasan');
        Schema::dropIfExists('asesor_pekerjaan');
        Schema::dropIfExists('ujikom_unit_kompetensi');
        Schema::dropIfExists('ujikom_hasil_detail');
        Schema::dropIfExists('ujikom_asesi_hasil');
        Schema::dropIfExists('ujikom_feedback');
        Schema::dropIfExists('ujikom_evaluasi');
        Schema::dropIfExists('sertifikat');
        Schema::dropIfExists('ujikom');
        Schema::dropIfExists('instansi');
        Schema::dropIfExists('kota');
        Schema::dropIfExists('provinsi');
        Schema::dropIfExists('asesor');
        Schema::dropIfExists('asesi');

        Schema::enableForeignKeyConstraints();
    }
}
