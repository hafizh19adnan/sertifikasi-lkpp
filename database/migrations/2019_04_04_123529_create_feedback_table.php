<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_komponen', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('kategori');
            $table->string('pernyataan');
        });

        Schema::create('feedback_ujikom', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_komponen')->unsigned();
            $table->integer('id_asesi')->unsigned();
            $table->integer('id_ujikom')->unsigned();
            $table->string('hasil');

            $table->foreign('id_komponen')->references('id')->on('feedback_komponen');
            $table->foreign('id_asesi')->references('id')->on('asesi');
            $table->foreign('id_ujikom')->references('id')->on('ujikom');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_komponen');
        Schema::dropIfExists('feedback_ujikom');
    }
}
