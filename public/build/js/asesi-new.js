$(document).ready(function() {
  var counter_edu= 1;
  var counter_work=1;
  var counter_exp=1;
  var counter_comp=1;
  var counter_kelengkapan=1;
  var instansi =jQuery.parseJSON($('meta[name=instansi]').attr('content'));
  var dropdown_instansi ='';
  
  jQuery.each(instansi, function(i, val) {
      dropdown_instansi += ' <option  value='+val['id'] +'>'+val['nama_instansi']+'</option>';
  });
	
  $('#nik').keypress(function(e){
    if(e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
  
  $('#tahun_lulus').keypress(function(e){
    if(e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
  
  $('#nip').keypress(function(e){
    if(e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
  
  $('#npwp').keypress(function(e){
    if(e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
  
  $('#nomor_telepon').keypress(function(e){
    if(e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
  
  $('#add_pendidikan').click(function(){
    counter_edu+=1;
    $('#count_edu').val(2);
    $('#pendidikan-container').append("<div id=row_pendidikan"+counter_edu+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Riwayat Pendidikan "+counter_edu+"</h4></strong><br> </div><div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Strata Pendidikan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='strata' class='select2_single form-control' name='strata"+counter_edu+"' tabindex='-1'> <option>D3</option> <option>D4</option> <option>S1</option> <option>S2</option> <option>S3</option> </select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Sekolah/Lembaga <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='nama_lembaga' name='nama_lembaga"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jurusan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='jurusan' name='jurusan"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tahun Lulus <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='tahun_lulus' name='tahun_lulus"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Gelar </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='gelar' name='gelar"+counter_edu+"'  class='form-control col-md-7 col-xs-12'> </div> </div> </div>")
  });
  
  $('#add_pekerjaan').click(function(){
      counter_work++;
      $('#count_work').val(counter_work);
      $('#pekerjaan-container').append("<br><div id='row-pekerjaan"+counter_work+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Pekerjaan "+counter_work+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='id_instansi' class='select2_single form-control' name='id_instansi"+counter_work+"' tabindex='-1'>" + dropdown_instansi+ "</select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='jabatan_hist' name='jabatan_prev"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='periode_hist' name='periode"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> </div>")
  });
  
  $('#add_pengalaman').click(function(){
      counter_exp++;
      $('#count_exp').val(counter_exp);
      $('#pengalaman-container').append("<br><div id='row-pengalaman"+counter_exp+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Pengalaman "+counter_exp+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Pengalaman <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='deskripsi_pengalaman' name='deskripsi_pengalaman"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'>  <select id='jabatan1' class='select2_single form-control' name='jabatan"+counter_exp+"' tabindex='-1'> <option>PPK</option> <option>Pokja PP</option> <option>Pokja P</option> <option>PJPHP</option> </select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Masa Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='masa_jabatan' name='masa_jabatan"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor SK <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='nomor_sk' name='nomor_sk"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> </div>")    
  });
  
  $('#add_pendukung').click(function(){
      counter_comp++;
      $('#count_pendukung').val(counter_comp);
      $('#pendukung-container').append("<br><div id='row-pendukung"+counter_comp+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Bukti Kompetensi yang Relevan "+counter_comp+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Kompetensi Pendukung <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='deskripsi_kompetensi' name='deskripsi_kompetensi"+counter_comp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Pendukung <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='bukti' class='select2_single form-control' name='bukti"+counter_comp+"' tabindex='-1'> <option  value='Ada'>Ada</option> <option  value='Tidak Ada'>Tidak Ada</option> </select> </div> </div> </div>")
  });
  
  $('#add_kelengkapan').click(function(){
    counter_kelengkapan++;
    $('#count_kelengkapan').val(counter_kelengkapan);
    $('#kelengkapan-container').append("<div id='row-kelengkapan"+counter_kelengkapan+"> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Bukti Kelengkapan "+counter_kelengkapan+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Berkas Persyaratan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='kelengkapan1' name='nama_berkas"+counter_kelengkapan+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Kelengkapan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='bukti' class='select2_single form-control' name='bukti_lengkap"+counter_kelengkapan+"' tabindex='-1'> <option>Ada(Memenuhi Syarat)</option> <option>Ada(Tidak Memenuhi Syarat)</option> <option>Tidak Ada</option> </select> </div> </div> </div>");
});

  //Validate Pangkat
  var pangkat= $('#pangkat').val();
  if(pangkat==null){
    $('#submit').attr('disabled','disabled');
  }

  jQuery('select[name="golongan"]').on('change',function(){
    $('#submit').attr('disabled',false);
      switch ($('#golongan').val()) {
        case 'IA':
          $('#pangkat').val('Juru Muda');
          break;
        case 'IB':
          $('#pangkat').val('Juru Muda Tingkat I');
          break;
        case 'IC':
          $('#pangkat').val('Juru');
          break;
        case 'ID':
          $('#pangkat').val('Juru Tingkat I');
          break;
        case 'IIA':
          $('#pangkat').val('Pengatur Muda');
          break;
        case 'IIB':
          $('#pangkat').val('Pengatur Muda Tingkat I');
          break;
        case 'IIC':
          $('#pangkat').val('Pengatur');
          break;
        case 'IID':
          $('#pangkat').val('Pengatur Tingkat I');
          break;
        case 'IIIA':
          $('#pangkat').val('Penata Muda');
          break;
        case 'IIIB':
          $('#pangkat').val('Penata Muda Tingkat I');
          break;
        case 'IIIC':
          $('#pangkat').val('Penata');
          break;
        case 'IIID':
          $('#pangkat').val('Penata Tingkat I');
          break;
        case 'IVA':
          $('#pangkat').val('Pembina');
          break;
        case 'IVB':
          $('#pangkat').val('Pembina Tingkat I');
          break;
        case 'IVC':
          $('#pangkat').val('Pembina Utama Muda');
          break;
        case 'IVD':
          $('#pangkat').val('Pembina Utama Madya');
          break;
        case 'IVE':
          $('#pangkat').val('Pembina Utama');
          break;  
      }
    });
    
    jQuery('select[name="id_provinsi"]').on('change',function(){
      $('#submit').attr('disabled', false);
       var countryID = jQuery(this).val();
       if(countryID)
       {
          jQuery.ajax({
             url : '/dashboard/getkota/' +countryID,
             type : "GET",
             dataType : "json",
             success:function(data)
             {
                console.log(data);
                jQuery('select[name="id_kota"]').empty();
                jQuery.each(data, function(key,value){
                   $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
             }
          });
       }
       else
       {
          $('select[name="id_kota"]').empty();
       }
    });
    $(document).on('blur', '[data-validator]', function () {
        new Validator($(this));
  });
  
  $('.js-example-basic-multiple').select2({
    maximumSelectionLength: 1,
    allowClear: true
  });
  
  jQuery('select[name="golongan"]').on('change',function(){
    switch ($('#golongan').val()) {
      case 'IA':
        $('#pangkat').val('Juru Muda');
        break;
      case 'IB':
        $('#pangkat').val('Juru Muda Tingkat I');
        break;
      case 'IC':
        $('#pangkat').val('Juru');
        break;
      case 'ID':
        $('#pangkat').val('Juru Tingkat I');
        break;
      case 'IIA':
        $('#pangkat').val('Pengatur Muda');
        break;
      case 'IIB':
        $('#pangkat').val('Pengatur Muda Tingkat I');
        break;
      case 'IIC':
        $('#pangkat').val('Pengatur');
        break;
      case 'IID':
        $('#pangkat').val('Pengatur Tingkat I');
        break;
      case 'IIIA':
        $('#pangkat').val('Penata Muda');
        break;
      case 'IIIB':
        $('#pangkat').val('Penata Muda Tingkat I');
        break;
      case 'IIIC':
        $('#pangkat').val('Penata');
        break;
      case 'IIID':
        $('#pangkat').val('Penata Tingkat I');
        break;
      case 'IVA':
        $('#pangkat').val('Pembina');
        break;
      case 'IVB':
        $('#pangkat').val('Pembina Tingkat I');
        break;
      case 'IVC':
        $('#pangkat').val('Pembina Utama Muda');
        break;
      case 'IVD':
        $('#pangkat').val('Pembina Utama Madya');
        break;
      case 'IVE':
        $('#pangkat').val('Pembina Utama');
        break;  
    }
  });
  
  jQuery('select[name="id_provinsi"]').on('change',function(){
     var countryID = jQuery(this).val();
     if(countryID)
     {
        jQuery.ajax({
           url : '/dashboard/getkota/' +countryID,
           type : "GET",
           dataType : "json",
           success:function(data)
           {
              console.log(data);
              jQuery('select[name="id_kota"]').empty();
              jQuery.each(data, function(key,value){
                 $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
              });
           }
        });
     }
     else
     {
        $('select[name="id_kota"]').empty();
     }
  });
});	