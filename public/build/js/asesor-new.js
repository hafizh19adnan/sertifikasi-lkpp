$(document).ready(function() {
    init_SmartWizard();
    var counter_work=1;
 

    var instansi =jQuery.parseJSON($('meta[name=instansi]').attr('content'));
    var dropdown_instansi ='';

    $('#nik').keypress(function(e){
        if(e.charCode < 48 || e.charCode > 57) {
          return false;
        }
      });
      $('#nip').keypress(function(e){
        if(e.charCode < 48 || e.charCode > 57) {
          return false;
        }
      });
      $('#npwp').keypress(function(e){
        if(e.charCode < 48 || e.charCode > 57) {
          return false;
        }
      });
    
    jQuery.each(instansi, function(i, val) {
        dropdown_instansi += ' <option  value='+val['id'] +'>'+val['nama_instansi']+'</option>';
       
      });
      $('#add_pekerjaan').click(function(){
        counter_work++;
        $('#count_pekerjaan').val(counter_work);
        $('#pekerjaan-container').append("<br><div id='row-pekerjaan"+counter_work+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h3>Pekerjaan "+counter_work+"</h3></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='id_instansi' class='select2_single form-control' name='id_instansi"+counter_work+"' tabindex='-1'>" + dropdown_instansi+ "</select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='jabatan_hist' name='jabatan_prev"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='periode_hist' name='periode"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> </div>")
    });
  
   
});	


function init_SmartWizard() {

    if( typeof ($.fn.smartWizard) === 'undefined'){ return; }
        console.log('init_SmartWizard');

        $('#wizard').smartWizard({
        // Properties
        selected: 0,  // Selected Step, 0 = first step   
        keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
        enableAllSteps: false,  // Enable/Disable all steps on first load
        transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
        contentURL:null, // specifying content url enables ajax content loading
        contentURLData:null, // override ajax query parameters
        contentCache:true, // cache step contents, if false content is fetched always from ajax url
        cycleSteps: false, // cycle step navigation
        enableFinishButton: false, // makes finish button enabled always
        hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead
        errorSteps:[],    // array of step numbers to highlighting as error steps
        labelNext:'Next', // label for Next button
        labelPrevious:'Previous', // label for Previous button
        labelFinish:'Finish',  // label for Finish button        
        noForwardJumping:false,
        ajaxType: 'POST',
        // Events
        onLeaveStep: null, // triggers when leaving a step
        onShowStep: null,  // triggers when showing a step
        onFinish: tes,  // triggers when Finish button is clicked  
        buttonOrder: ['prev', 'next', 'finish']  // button order, to hide a button remove it from the list
    }); 

    $('.buttonNext').addClass('btn btn-success');
    $('.buttonPrevious').addClass('btn btn-primary');
    $('.buttonFinish').addClass('btn btn-default');

};
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
function tes(e){
    event.preventDefault();
    var umum=getFormData(($('#asesor_new')));
    var pekerjaan=getFormData($('#riwayat_pekerjaan'));
    console.log(umum);
    console.log(pekerjaan);
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    jQuery.ajax({
        url: "/dashboard/asesor/post",
        method: 'POST',
        data: {
            umum:umum,
            pekerjaan:pekerjaan                              
        },
        success: function(result){
            if(result=="success"){
                console.log(result);
                window.location = "/dashboard/asesor";
            }else{
                console.log(result);
                window.location = "/dashboard/asesor/create";
            }
            console.log(result);
        },
        error: function(response){
            alert('Error'+response);
        }
    });
};