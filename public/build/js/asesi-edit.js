$(document).ready(function() {
    init_SmartWizard();
    var counter_edu= $('#count_pendidikan').val();
    var counter_work=$('#count_pekerjaan').val();;
    var counter_exp=$('#count_pengalaman').val();;
    var counter_comp=$('#count_pendukung').val();;

    var instansi =jQuery.parseJSON($('meta[name=instansi]').attr('content'));
    var dropdown_instansi ='';
    jQuery.each(instansi, function(i, val) {
        dropdown_instansi += ' <option  value='+val['id'] +'>'+val['nama_instansi']+'</option>';
       
      });
    $('#nik').keypress(function(e){
      if(e.charCode < 48 || e.charCode > 57) {
        return false;
      }
    });
    $('#nip').keypress(function(e){
      if(e.charCode < 48 || e.charCode > 57) {
        return false;
      }
    });
    $('#npwp').keypress(function(e){
      if(e.charCode < 48 || e.charCode > 57) {
        return false;
      }
    });
    $('#add_pendidikan').click(function(){
      counter_edu++;
      $('#count_pendidikan').val(counter_edu);
      $('#pendidikan-container').append("<div id=row_pendidikan"+counter_edu+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Riwayat Pendidikan "+counter_edu+"</h4></strong><br> </div><div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Strata Pendidikan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='strata' class='select2_single form-control' name='strata"+counter_edu+"' tabindex='-1'> <option>D3</option> <option>D4</option> <option>S1</option> <option>S2</option> <option>S3</option> </select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Sekolah/Lembaga <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='nama_lembaga' name='nama_lembaga"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jurusan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='jurusan' name='jurusan"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tahun Lulus <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='number' id='tahun_lulus' name='tahun_lulus"+counter_edu+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Gelar </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='gelar' name='gelar"+counter_edu+"'  class='form-control col-md-7 col-xs-12'> </div> </div> </div>")
    });
    $('#add_pekerjaan').click(function(){
        counter_work++;
        $('#count_pekerjaan').val(counter_work);
        $('#pekerjaan-container').append("<br><div id='row-pekerjaan"+counter_work+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Pekerjaan "+counter_work+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='id_instansi' class='select2_single form-control' name='id_instansi"+counter_work+"' tabindex='-1'>" + dropdown_instansi+ "</select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='jabatan_hist' name='jabatan_prev"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='periode_hist' name='periode"+counter_work+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> </div>")
    });
    $('#add_pengalaman').click(function(){
        counter_exp++;
        $('#count_pengalaman').val(counter_exp);
        $('#pengalaman-container').append("<br><div id='row-pengalaman"+counter_exp+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Pengalaman "+counter_exp+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Pengalaman <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='deskripsi_pengalaman' name='deskripsi_pengalaman"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'>  <select id='jabatan1' class='select2_single form-control' name='jabatan"+counter_exp+"' tabindex='-1'> <option>PPK</option> <option>Pokja PP</option> <option>Pokja P</option> <option>PJPHP</option> </select> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Masa Jabatan <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='masa_jabatan' name='masa_jabatan"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor SK <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='nomor_sk' name='nomor_sk"+counter_exp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> </div>")    
    });
    $('#add_pendukung').click(function(){
        counter_comp++;
        $('#count_pendukung').val(counter_comp);
        $('#pendukung-container').append("<br><div id='row-pendukung"+counter_comp+"'> <div class='col-md-2'></div> <div class='col-md-10'> <strong><h4>Kompetensi Pendukung "+counter_comp+"</h4></strong><br> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Kompetensi Pendukung <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='text' id='deskripsi_kompetensi' name='deskripsi_kompetensi"+counter_comp+"' required='required' class='form-control col-md-7 col-xs-12'> </div> </div> <div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Pendukung <span class='required'>*</span> </label> <div class='col-md-6 col-sm-6 col-xs-12'> <select id='bukti' class='select2_single form-control' name='bukti"+counter_comp+"' tabindex='-1'> <option  value='Ada'>Ada</option> <option  value='Tidak Ada'>Tidak Ada</option> </select> </div> </div> </div>")
    });
    
    jQuery('select[name="golongan"]').on('change',function(){
        switch ($('#golongan').val()) {
          case 'IA':
            $('#pangkat').val('Juru Muda');
            break;
          case 'IB':
            $('#pangkat').val('Juru Muda Tingkat I');
            break;
          case 'IC':
            $('#pangkat').val('Juru');
            break;
          case 'ID':
            $('#pangkat').val('Juru Tingkat I');
            break;
          case 'IIA':
            $('#pangkat').val('Pengatur Muda');
            break;
          case 'IIB':
            $('#pangkat').val('Pengatur Muda Tingkat I');
            break;
          case 'IIC':
            $('#pangkat').val('Pengatur');
            break;
          case 'IID':
            $('#pangkat').val('Pengatur Tingkat I');
            break;
          case 'IIIA':
            $('#pangkat').val('Penata Muda');
            break;
          case 'IIIB':
            $('#pangkat').val('Penata Muda Tingkat I');
            break;
          case 'IIIC':
            $('#pangkat').val('Penata');
            break;
          case 'IIID':
            $('#pangkat').val('Penata Tingkat I');
            break;
          case 'IVA':
            $('#pangkat').val('Pembina');
            break;
          case 'IVB':
            $('#pangkat').val('Pembina Tingkat I');
            break;
          case 'IVC':
            $('#pangkat').val('Pembina Utama Muda');
            break;
          case 'IVD':
            $('#pangkat').val('Pembina Utama Madya');
            break;
          case 'IVE':
            $('#pangkat').val('Pembina Utama');
            break;  
        }
      });
      
      jQuery('select[name="id_provinsi"]').on('change',function(){
         var countryID = jQuery(this).val();
         if(countryID)
         {
            jQuery.ajax({
               url : '/dashboard/getkota/' +countryID,
               type : "GET",
               dataType : "json",
               success:function(data)
               {
                  console.log(data);
                  jQuery('select[name="id_kota"]').empty();
                  jQuery.each(data, function(key,value){
                     $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                  });
               }
            });
         }
         else
         {
            $('select[name="id_kota"]').empty();
         }
      });
      $(document).on('blur', '[data-validator]', function () {
          new Validator($(this));
    });
        
   
});	


function init_SmartWizard() {

    if( typeof ($.fn.smartWizard) === 'undefined'){ return; }
        console.log('init_SmartWizard');

        $('#wizard').smartWizard({
        // Properties
        selected: 0,  // Selected Step, 0 = first step   
        keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
        enableAllSteps: false,  // Enable/Disable all steps on first load
        transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
        contentURL:null, // specifying content url enables ajax content loading
        contentURLData:null, // override ajax query parameters
        contentCache:true, // cache step contents, if false content is fetched always from ajax url
        cycleSteps: false, // cycle step navigation
        enableFinishButton: false, // makes finish button enabled always
        hideButtonsOnDisabled: true, // when the previous/next/finish buttons are disabled, hide them instead
        errorSteps:[],    // array of step numbers to highlighting as error steps
        labelNext:'Next', // label for Next button
        labelPrevious:'Previous', // label for Previous button
        labelFinish:'Finish',  // label for Finish button        
        noForwardJumping:false,
        ajaxType: 'POST',
        // Events
        onLeaveStep: null, // triggers when leaving a step
        onShowStep: null,  // triggers when showing a step
        onFinish: finishFunction,  // triggers when Finish button is clicked  
        buttonOrder: ['finish', 'next', 'prev']
    }); 

    $('.buttonPrevious').addClass('btn btn-primary');
    $('.buttonNext').addClass('btn btn-success');
    $('.buttonFinish').addClass('btn btn-default');
    

};
function validate(e){

}
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
function finishFunction(e){
    event.preventDefault();
    var umum=getFormData(($('#asesi_new')));
    var pendidikan=getFormData($('#riwayat_pendidikan'));
    var pekerjaan=getFormData($('#riwayat_pekerjaan'));
    var pengalaman=getFormData($('#riwayat_pengalaman'));
    var pendukung=getFormData($('#pendukung'));
    var skema=$('#skema').serializeArray();
    console.log(umum);
    console.log(pendidikan);
    console.log(pekerjaan);
    console.log(pengalaman);
    console.log(pendukung);
    console.log(skema);
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    jQuery.ajax({
        url: "/dashboard/asesi/post",
        method: 'POST',
        data: {
            umum:umum,
            pendidikan:pendidikan,
            pekerjaan:pekerjaan,
            pengalaman:pengalaman,
            pendukung:pendukung,
            skema:skema                               
        },
        success: function(result){
            if(result=="success"){
                console.log(result);
                window.location = "/dashboard/asesi";
            }else{
                console.log(result);
                window.location = "/dashboard/asesi/create";
            }
            // console.log(result);
        
            
        },
        error: function(response){
            console.log('Error'+response);
        }
    });
};