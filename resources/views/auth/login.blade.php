@extends('layouts.app')
@section('content')
    
	<div class="limiter">
	
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			
			<div class="wrap-login100">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="text-center">
					<img src="/images/lkpp.png" width='180px' class='mx-auto' />
				</div>
                <span class="login100-form-title p-b-34 p-t-27">
						Log in - LKPP
				</span>
                <div class="wrap-input100 validate-input" data-validate = "Enter username">
                    <input id="login" type="text"
                            class="input100{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                            name="login" placeholder="Username"  required >
                
                        @if ($errors->has('username') || $errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                            </span>
                        @endif
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
				</div>
            
                <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input id="password" type="password" placeholder="Password" class="input100{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
				</div>
                <div class="container-login100-form-btn">
						<button class="login100-form-btn">Login</button>
					</div>
               
              
           
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/login/main.js"></script>
@endsection
