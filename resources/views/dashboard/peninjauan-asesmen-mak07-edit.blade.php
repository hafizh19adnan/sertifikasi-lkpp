@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form MAK07 </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/asesmen-peninjauan'><i class='fa fa-arrow-left'></i> Kembali ke Rekap MAK 07</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Masukan Data MAK07</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('asesmen-peninjauan.update',$peninjauan[0]->id) }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      @method('PUT')
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema Sertifikasi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='skema_sertifikasi'>
                              <option {{ $peninjauan[0]->skema_sertifikasi == 'Unit' ? 'selected' : '' }}>Unit</option>
                              <option {{ $peninjauan[0]->skema_sertifikasi == 'Klaster' ? 'selected' : '' }}>Klaster</option>
                              <option {{ $peninjauan[0]->skema_sertifikasi == 'Kualifikasi' ? 'selected' : '' }}>Kualifikasi</option>
                            
                          </select>
                        </div>
                      </div>
                     
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Nomor Skema Sertifikasi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='alasan_banding' value=' {{$peninjauan[0]->nomor_skema_sertifikasi}}' name='nomor_skema_sertifikasi' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <h4>Prosedur Asesmen</h4>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      
                          <th>Aspek yang dikaji ulang</th>
                          <th>Pemenuhan terhadap prinsip-prinsip asesmen</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>Perencanaan Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='perencanaan'>
                                <option {{ $peninjauan[0]->perencanaan == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->perencanaan == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->perencanaan == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->perencanaan == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Pra Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='pra'>
                                <option {{ $peninjauan[0]->pra == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->pra == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->pra == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->pra == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Pelaksanaan Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='pelaksanaan'>
                                <option {{ $peninjauan[0]->pelaksanaan == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->pelaksanaan == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->pelaksanaan == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->pelaksanaan == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Keputusan Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='keputusan'>
                                <option {{ $peninjauan[0]->keputusan == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->keputusan == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->keputusan == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->keputusan == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Umpan Balik Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='umpan_balik'>
                              <option {{ $peninjauan[0]->umpan_balik == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->umpan_balik == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->umpan_balik == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->umpan_balik == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Pencatatan Asesmen</td>
                            <td>
                              <select class='select2_single form-control' name='pencatatan'>
                              <option {{ $peninjauan[0]->pencatatan == 'Valid' ? 'selected' : '' }}>Valid</option>
                                <option {{ $peninjauan[0]->pencatatan == 'Reliable' ? 'selected' : '' }}>Reliable</option>
                                <option {{ $peninjauan[0]->pencatatan == 'Flexible' ? 'selected' : '' }}>Flexible</option>
                                <option {{ $peninjauan[0]->pencatatan == 'Fair' ? 'selected' : '' }}>Fair</option>
                              </select>
                            </td>
                        </tr>
                       
                      </tbody>
                      </table>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi Perbaikan <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <textarea rows="4" cols="50" id="last-name" name="rekomendasi_prosedur" required="required" class="form-control col-md-7 col-xs-12">
                        {{$peninjauan[0]->rekomendasi_prosedur}}
                        </textarea>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <h4>Konsistensi Keputusan Asesmen</h4>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      
                          <th>Aspek yang dikaji ulang</th>
                          <th>Pemenuhan terhadap dimensi kompetensi</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>Bukti dari rentang asesmen diperiksa terhadap konsistensi dimensi kompetensi</td>
                            <td>
                              <select class='select2_single form-control' name='konsistensi'>
                                <option {{ $peninjauan[0]->konsistensi == 'Task Skill' ? 'selected' : '' }}>Task Skill</option>
                                <option {{ $peninjauan[0]->konsistensi == 'Task Management Skill' ? 'selected' : '' }}>Task Management Skill</option>
                                <option {{ $peninjauan[0]->konsistensi == 'Contingency Management Skill' ? 'selected' : '' }}>Contingency Management Skill</option>
                                <option {{ $peninjauan[0]->konsistensi == 'Job Role/Environment Skill' ? 'selected' : '' }}>Job Role/Environment Skill</option>
                              </select>
                            </td>
                        </tr>
                     
                       
                      </tbody>
                      </table>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi Perbaikan <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <textarea rows="4" cols="50" id="last-name" name="rekomendasi_konsistensi"  required="required" class="form-control col-md-7 col-xs-12">
                        {{$peninjauan[0]->rekomendasi_konsistensi}}
                        </textarea>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_skemas = $('#id_skema').val();
            jQuery.ajax({
              url : '/dashboard/getUnitBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  $('#jumlah-unit').empty();
                  $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                  $('#unit-container').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                    
                  });
                  
              }
            });
            $('#tes').click(function(){
              alert($('#out').val());
              alert($('#in').val());
            });
            
            jQuery('select[name="id_skema"]').on('change',function(){
                  var id_skema = jQuery(this).val();

                  if(id_skema)
                  {
                      jQuery.ajax({
                        url : '/dashboard/getAsesiBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="id_asesi"]').empty();
                            jQuery.each(data, function(key,value){
                              $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });
                           
                        }
                      });
                      jQuery.ajax({
                        url : '/dashboard/getUnitBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            // alert(data.length);
                            $('#jumlah-unit').empty();
                            $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                            $('#unit-container').empty();
                            jQuery.each(data, function(key,value){
                               $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                              
                            });
                           
                        }
                      });
                  }
                  else
                  {
                      $('select[name="id_kota"]').empty();
                  }
                });
          });
        </script>

   

@endsection