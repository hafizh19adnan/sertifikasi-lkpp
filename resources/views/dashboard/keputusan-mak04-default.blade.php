@extends('dashboard.layout')
@section('content')
       <!-- page content -->
       <div class="right_col" role="main">
          <div class="">
          <a class="btn btn-success" href="/dashboard/asesmen-keputusan/create">Masukan Data MAK04</a>
          
            <div class="clearfix"></div>
            <div class="row">
                  @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($message = Session::get('error'))
                      <div class="alert alert-danger">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
       
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Rekap Asesi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nama Asesor</th>
                          <th>Nama Asesi </th>
                          <th>Nama Skema</th>
                          <th>Tempat</th>
                          <th>Tanggal Asesmen</th>
                          <th>Rekomendasi</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($keputusan as $keputusan)
                        <tr>
                            <td>{{ $keputusan->id }}</td>
                            <td>{{ $keputusan->asesor}}</td>
                            <td>{{ $keputusan->asesi}}</td>
                            <td>{{ $keputusan->nama_skema}}</td>
                            <td>{{ $keputusan->tanggal}}</td>
                            <td>{{ $keputusan->tempat}}</td>
                            <td>{{ $keputusan->rekomendasi_asesor}}</td>
                            <td>
                                    <a class="btn btn-success" href="{{ route('asesmen-keputusan.show',$keputusan->id) }}">Detail</a>
                                                
                                    <a class="btn btn-primary" href="{{ route('asesmen-keputusan.edit',$keputusan->id) }}">Edit</a>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                      Hapus 
                                    </button>
                            </td>
                        </tr>
                        @endforeach
                        
                        
                      
                      </tbody>
                    </table>
                  <!-- The Modal -->
                  @if($count>0)
                  <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Konfirmasi</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <h5>Apakah anda yakin ingin menghapus?</h5>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <form action="{{ route('asesmen-keputusan.destroy',$keputusan->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        </div>

                      </div>
                    </div>
                  </div>
                  @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>
@endsection