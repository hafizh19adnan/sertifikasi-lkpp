@extends('dashboard.layout')
@section('content')
       <!-- page content -->
       <div class="right_col" role="main">
          <div class="">
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Masukan Data Pengiriman Sertifikat</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  <div class="x_content">
                    <br />
                    <form method="POST" action="{{ route('pengiriman-sertifikat.store') }}" data-parsley-validate class="form-horizontal form-label-left">
                      @csrf
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Sertifikat<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="id_sertifikat" id='id_sertifikat'>
                            @foreach ($sertifikat as $sertifikat)
                              <option  value="{{ $sertifikat->id }}">{{ $sertifikat->nomor_sertifikat }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_pengirman">Tanggal Ujikom<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" id="tanggal_ujikom" name="tanggal_ujikom" readonly required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>                   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_pengirman">Tanggal Pengiriman<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" id="tanggal_pengiriman" name="tanggal_kirim" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat_pengirman">Alamat Pengiriman<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="alamat_pengiriman" name="alamat_pengiriman" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_telepon_pic">Nomor Telepon PIC<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nomor_telepon_pic" name="nomor_telepon_pic" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_resi">Nomor Resi<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nomor_resi" name="nomor_resi" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Simpan Data</button>
                        </div>
                      </div>

                    </form>
                  
                  </div>
                </div>
            <div class="clearfix"></div>

            <div class="row">
              
       
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Pengajuan Sertifikat</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nomor Sertifikat</th>
                          <th>Tanggal Kirim</th>
                          <th>Alamat Pengiriman</th>
                          <th>Nomor Telepon PIC</th>
                          <th>Nomor Resi</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($pengiriman as $pengiriman)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $pengiriman->nomor_sertifikat }}</td>
                            <td>{{ $pengiriman->tanggal_kirim }}</td>
                            <td>{{ $pengiriman->alamat_pengiriman }}</td>
                            <td>{{ $pengiriman->nomor_telepon_pic }}</td>
                            <td>{{ $pengiriman->nomor_resi }}</td>
                            <td>
                                              
                                <a class="btn btn-primary" href="{{ route('pengiriman-sertifikat.edit',$pengiriman->id) }}">Edit</a>

                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                      Hapus 
                                    </button>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                        
                      
                      </tbody>
                    </table>
					
            <!-- The Modal -->
            <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Konfirmasi</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <h5>Apakah anda yakin ingin menghapus?</h5>
                        </div>

                        <!-- Modal footer -->
                        @if($count>0)
                        <div class="modal-footer">
                        <form action="{{ route('pengiriman-sertifikat.destroy',$pengiriman->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
              
                            <button type="submit" class="btn btn-danger">Hapus</button>
                        </form>
                        </div>
                        @endif
                      </div>
                    </div>
                  </div>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>
    <script>
    $(document).ready(function(){
      var id_sertifikat=$('#id_sertifikat').val();
      jQuery.ajax({
          url : '/dashboard/get-tanggal/' +id_sertifikat,
          type : "GET",
          dataType : "json",
          success:function(data)
          {
              $('#tanggal_ujikom').val(data[0]['tanggal_ujikom']);
              console.log();
          }
        });
        $('#id_sertifikat').change(function(){
          var id_sertifikats= $('#id_sertifikat').val();
          
          jQuery.ajax({
          url : '/dashboard/get-tanggal/' +id_sertifikats,
          type : "GET",
          dataType : "json",
          success:function(data)
          {
              $('#tanggal_ujikom').val(data[0]['tanggal_ujikom']);
              console.log(data);
          }
        });
        });
    });
    
    </script>
@endsection