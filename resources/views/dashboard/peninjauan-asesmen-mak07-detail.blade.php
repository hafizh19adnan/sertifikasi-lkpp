@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form MAK07 </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/asesmen-peninjauan'><i class='fa fa-arrow-left'></i> Kembali ke Rekap MAK 07</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Detail Data MAK07</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form data-parsley-validate class='form-horizontal form-label-left'>
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema Sertifikasi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <P style='margin-top:10px'>{{$peninjauan[0]->skema_sertifikasi}}</P>
                        </div>
                      </div>
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Nomor Skema Sertifikasi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$peninjauan[0]->nomor_skema_sertifikasi}}</P>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <h4>Prosedur Asesmen</h4>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      
                          <th>Aspek yang dikaji ulang</th>
                          <th>Pemenuhan terhadap prinsip-prinsip asesmen</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>Perencanaan Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->perencanaan}}</P>
                            </td>
                        </tr>
                        <tr>
                            <td>Pra Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->pra}}</P>
                            </td>
                        </tr>
                        <tr>
                            <td>Pelaksanaan Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->pelaksanaan}}</P>
                            </td>
                        </tr>
                        <tr>
                            <td>Keputusan Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->keputusan}}</P>
                            </td>
                        </tr>
                        <tr>
                            <td>Umpan Balik Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->umpan_balik}}</P>
                            </td>
                        </tr>
                        <tr>
                            <td>Pencatatan Asesmen</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->pencatatan}}</P>
                            </td>
                        </tr>
                       
                      </tbody>
                      </table>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi Perbaikan <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <P style='margin-top:10px'>{{$peninjauan[0]->rekomendasi_prosedur}}</P>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <h4>Konsistensi Keputusan Asesmen</h4>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      
                          <th>Aspek yang dikaji ulang</th>
                          <th>Pemenuhan terhadap dimensi kompetensi</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>Bukti dari rentang asesmen diperiksa terhadap konsistensi dimensi kompetensi</td>
                            <td>
                            <P style='margin-top:10px'>{{$peninjauan[0]->konsistensi}}</P>
                            </td>
                        </tr>
                     
                       
                      </tbody>
                      </table>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi Perbaikan <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <P style='margin-top:10px'>{{$peninjauan[0]->rekomendasi_konsistensi}}</P>
                        </div>
                      </div> 
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_skemas = $('#id_skema').val();
            jQuery.ajax({
              url : '/dashboard/getUnitBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  $('#jumlah-unit').empty();
                  $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                  $('#unit-container').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                    
                  });
                  
              }
            });
            $('#tes').click(function(){
              alert($('#out').val());
              alert($('#in').val());
            });
            
            jQuery('select[name="id_skema"]').on('change',function(){
                  var id_skema = jQuery(this).val();

                  if(id_skema)
                  {
                      jQuery.ajax({
                        url : '/dashboard/getAsesiBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="id_asesi"]').empty();
                            jQuery.each(data, function(key,value){
                              $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });
                           
                        }
                      });
                      jQuery.ajax({
                        url : '/dashboard/getUnitBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            // alert(data.length);
                            $('#jumlah-unit').empty();
                            $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                            $('#unit-container').empty();
                            jQuery.each(data, function(key,value){
                               $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                              
                            });
                           
                        }
                      });
                  }
                  else
                  {
                      $('select[name="id_kota"]').empty();
                  }
                });
          });
        </script>

   

@endsection