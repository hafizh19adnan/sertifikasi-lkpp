@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form Keputusan dan Umpan Balik (MAK-04) </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/asesmen-keputusan'><i class='fa fa-arrow-left'></i> Kembali ke Rekap MAK04</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Masukan Data Formulir MAK-04</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('asesmen-keputusan.store') }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_skema' name='id_skema'>
                            @foreach ($skema as $skema)
                              <option  value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option  value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tanggal Asesmen <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='date' id='tempat_uji' name='tanggal' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tempat <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='tempat' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Umpan balik terhadap pencapaian unjuk kerja: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='feedback_pencapaian' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Identifikasi kesenjangan pencapaian unjuk kerja: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='identifikasi_kesenjangan' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Saran tindak lanjut hasil asesmen: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='saran_tindak_lanjut' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Rekomendasi Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='rekomendasi_asesor'>
                          
                              <option  value='K'>Kompeten</option>
                              <option  value='BK'>Belum Kompeten</option>
                        
                          </select>
                        </div>
                      </div>
                   
                      <br>
                      <div class='ln_solid'></div>
                      <h4>Keputusan Unit Kompetensi</h4><hr>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Unit Kompetensi</th>
                          <th>Bukti Langsung</th>
                          <th>Bukti Tidak Langsung</th>
                          <th>Bukti Tambahan</th>
                          <th>Keputusan</th>
                          
                        </tr>
                      </thead>
                      <tbody id='unit-container'>
                       
                        
                      </tbody>
                    </table>
                      
                      <div id='jumlah-unit'>
                     
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_skemas = $('#id_skema').val();
            jQuery.ajax({
              url : '/dashboard/getUnitBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  $('#jumlah-unit').empty();
                  $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                  $('#unit-container').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container').append("<tr> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <td> "+value.nama_unit_kompetensi+" </td> <td> <select class='select2_single form-control' name='bukti_langsung"+key+"'> <option  value='CLS'>CLS</option> <option  value='SK'>SK</option> </select> </td> <td> <select class='select2_single form-control' name='bukti_tidak_langsung"+key+"'> <option  value='CLP'>CLP</option> </select> </td> <td> <select class='select2_single form-control' name='bukti_tambahan"+key+"'> <option  value='DPL'>DPL</option> <option  value='DPT'>DPT</option> <option  value='DPW'>DPW</option> </select> </td> <td> <select class='select2_single form-control' name='keputusan"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </td> </tr>");
                    
                  });
                  
              }
            });
            jQuery('select[name="id_skema"]').on('change',function(){
                  var id_skema = jQuery(this).val();

                  if(id_skema)
                  {
                      jQuery.ajax({
                        url : '/dashboard/getAsesiBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="id_asesi"]').empty();
                            jQuery.each(data, function(key,value){
                              $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });
                           
                        }
                      });
                      jQuery.ajax({
                        url : '/dashboard/getUnitBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            // alert(data.length);
                            $('#jumlah-unit').empty();
                            $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                            $('#unit-container').empty();
                            jQuery.each(data, function(key,value){
                              $('#unit-container').append("<tr> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <td> "+value.nama_unit_kompetensi+" </td> <td> <select class='select2_single form-control' name='bukti_langsung"+key+"'> <option  value='CLS'>CLS</option> <option  value='SK'>SK</option> </select> </td> <td> <select class='select2_single form-control' name='bukti_tidak_langsung"+key+"'> <option  value='CLP'>CLP</option> </select> </td> <td> <select class='select2_single form-control' name='bukti_tambahan"+key+"'> <option  value='DPL'>DPL</option> <option  value='DPT'>DPT</option> <option  value='DPW'>DPW</option> </select> </td> <td> <select class='select2_single form-control' name='keputusan"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </td> </tr>");
                    
                            });
                           
                        }
                      });
                  }
                  else
                  {
                      $('select[name="id_kota"]').empty();
                  }
                });
          });
        </script>

   

@endsection