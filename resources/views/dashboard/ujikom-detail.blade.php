@extends('dashboard.layout')

@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
    
          <div class="row top_tiles">
              
            
            <div class="clearfix"></div>
            @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a class="btn btn-primary" href="/dashboard/ujikom"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Pelaksanaan Ujikom</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Ujikom</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Daftar Asesor </a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Daftar Peserta   </a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Hasil Peserta   </a>
                          </li>
                         
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          
                            
                            <table class="data table no-margin">
                                <tbody>
                                    <tr>
                                        <th>Judul Kompetensi</th>
                                        <th>{{$ujikom[0]->judul}}</th>
                                        
                                    <th>
                                    <tr>
                                        <th>TUK</th>
                                        <th>{{$ujikom[0]->tuk}}</th>
                                    <th>
                                    <tr>
                                        <th>Tanggal Mulai</th>
                                        <th>{{$ujikom[0]->tanggal_mulai}}</th>
                                    <th>
                                    <tr>
                                        <th>Tanggal Selesai</th>
                                        <th>{{$ujikom[0]->tanggal_selesai}}</th>
                                    <th>
                                    <tr>
                                        <th>Lokasi Pelaksanaan</th>
                                        <th>{{$ujikom[0]->tempat_uji}}</th>
                                    <th>
                                    <tr>
                                        <th>Kota</th>
                                        <th>{{$ujikom[0]->nama_kota}}</th>
                                    <th>
                                  
                                    <tr>
                                        <th>Skema Kompetensi</th>
                                        <th>@foreach ($skema as $skema) {{$skema->nama_skema}}, @endforeach</th>
                                    <th>
                                   
                                    
                                </tbody>
                            </table>
                            
                         
                            <!-- end recent activity -->

                          </div>

                         
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <a class='btn btn-primary' href='/dashboard/ujikom-add-asesor/{{$ujikom[0]->id}}'>+ Tambah Asesor</a>
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Instansi</th>
                                  <th>Jenis Asesor</th>
                                  <th>Masa Berlaku</th>
                                  <th>Status</th>
                                
                                </tr>
                              </thead>
                              <tbody>
                              @foreach ($asesor as $asesor)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>{{ $asesor->nama}}</td>
                                    <td>@if ($asesor->jenis_kelamin === 'L')
                                          Laki-Laki
                                        @else
                                          Perempuan
                                        @endif</td>
                                    <td>{{ $asesor->nama_instansi}}</td>
                                    <td>{{ $asesor->jenis_asesor}}</td>
                                    <td>{{ $asesor->masa_berlaku}}</td>
                                    <td>@if ($asesor->is_active === 1)
                                            Aktif
                                        @else
                                          Non-Aktif
                                        @endif</td>
                                </tr>
                                @endforeach
                                
                                
                              
                              </tbody>
                            </table>
                           
                            <!-- end user projects -->
                            
                          </div>
                         
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <a class='btn btn-primary' href='/dashboard/ujikom-add-asesi/{{$ujikom[0]->id}}'>+ Tambah Asesi</a>
                          
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>Instansi</th>
                          <th>Pangkat</th>
                          <th>Golongan</th>
                          <th>Jabatan</th>
                          <th>Kota</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($asesis as $asesi)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $asesi->nama}}</td>
                            <td>{{ $asesi->jenis_kelamin}}</td>
                            <td>{{ $asesi->nama_instansi}}</td>
                            <td>{{ $asesi->pangkat}}</td>
                            <td>{{ $asesi->golongan}}</td>
                            <td>{{ $asesi->jabatan}}</td>
                            <td>{{ $asesi->nama_kota}}</td>
                            
                            <td>
                              <a class="btn btn-success" href="{{ route('asesi.show',$asesi->id) }}">Detail</a>
                                          
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                Hapus 
                              </button>
                            </td>
                        </tr>
                        @endforeach
                        
                        
                      
                      </tbody>
                    </table>
                            </div>

                              <!--End Section-->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                          
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Instansi</th>
                                  <th>Pangkat</th>
                                  <th>Golongan</th>
                                  <th>Hasil</th>
                            
                              
                                </tr>
                              </thead>
                              <tbody>
                              @foreach ($asesi_hasil as $asesi_hasil)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>{{ $asesi_hasil->nama}}</td>
                                    <td>{{ $asesi_hasil->jenis_kelamin}}</td>
                                    <td>{{ $asesi_hasil->nama_instansi}}</td>
                                    <td>{{ $asesi_hasil->pangkat}}</td>
                                    <td>{{ $asesi_hasil->golongan}}</td>
                                    <td>{{ $asesi_hasil->hasil}}</td>
                                    
                                </tr>
                                @endforeach
                                
                                
                              
                              </tbody>
                            </table>
                            </div>
                             <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    
                                </ul>

                                <a class="btn btn-success" href="{{ route('ujikom.edit', $ujikom[0]->id) }}"><i class="fa fa-edit m-right-xs"></i>Ubah Data Ujikom</a>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                  Hapus 
                                </button>
                                
                                <br />

                                
                              </div>

                              <!-- The Modal -->
                              <div class="modal fade" id="myModal">
                                <div class="modal-dialog">
                                  <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h4 class="modal-title">Konfirmasi</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                      <h5>Apakah anda yakin ingin menghapus?</h5>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                    <form action="{{ route('ujikom.destroy',$ujikom[0]->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                          
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    </div>

                                  </div>
                                </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="/vendors/raphael/raphael.min.js"></script>
    <script src="/vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/vendors/moment/min/moment.min.js"></script>
    <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

@endsection