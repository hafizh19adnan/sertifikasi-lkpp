@extends('dashboard.layout')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Masukan Data User</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					@if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
					@endif
					@if ($message = Session::get('error'))
						<div class="alert alert-danger">
							<p>{{ $message }}</p>
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="x_content"><br />
						<form method="POST" action="{{ route('user.store') }}" data-parsley-validate class="form-horizontal form-label-left">
							@csrf
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="password_conf">Konfirmasi Password <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="password" id="password_conf" name="password_conf" required="required" class="form-control col-md-7 col-xs-12">
									<span id="error_missmatch"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Peran<span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="select2_single form-control" name="role">
										<option value="1">Super Admin</option>
										<option value="2">Admin Jabfung</option>
										<option value="3">Admin Okupasi</option>
										<option value="4">Admin Sertifikat</option>
									</select>
								</div>
							</div>                                        
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<button class="btn btn-primary" type="reset">Reset</button>
									<button type="submit" id="submit" class="btn btn-success">Simpan Data</button>
								</div>
							</div>
						</form>                  
					</div>
                </div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">       
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Daftar User</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                                           
						</ul>
						<div class="clearfix"></div>
					</div>
                  <div class="x_content">                  
						<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>Peran</th>                      
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($users as $user)
								<tr>
									<td>{{ ++$loop->index }}</td>
									<td>{{ $user->username }}</td>
									<td>@if ($user->role == '1')
											Super Admin
										@elseif($user->role === '2')
											Admin Jabfung
										@elseif($user->role === '3')
											Admin Okupasi
										@elseif($user->role === '4')
											Admin Sertifikat
										@endif
									</td>
									<td>
										<a class="btn btn-primary" href="{{ route('user.edit',$user->id) }}">Edit</a>
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Hapus </button>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<!-- The Modal -->
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<div class="modal-content">
								<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title">Konfirmasi</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<!-- Modal body -->
									<div class="modal-body">
										<h5>Apakah anda yakin ingin menghapus?</h5>
									</div>
									<!-- Modal footer -->
									<div class="modal-footer">
										@if($count>0)
										<form action="{{ route('user.destroy',$user->id) }}" method="POST">
											@csrf
											@method('DELETE')                 
											<button type="submit" class="btn btn-danger">Delete</button>
										</form>
										@endif
									</div>
								</div>
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>

<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>

<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>

<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="/build/js/custom.min.js"></script>
<script>
$(document).ready(function(){
	$('#password_conf').blur(function(){
		var pass= $('#password').val();
		var pass_conf=$('#password_conf').val();
		if(pass === "" && pass_conf === ""){
			$('#error_missmatch').html('<label class="text-danger">Silahkan Isi Password Terlebih Dahulu</label>');
			$('#submit').attr('disabled', 'disabled');
		}
		else{
			if(pass!=pass_conf){
				$('#error_missmatch').html('<label class="text-danger">Password tidak cocok</label>');
				$('#submit').attr('disabled', 'disabled');
            }else{
				$('#error_missmatch').html('<label class="text-success">Password  cocok</label>');
				$('#submit').attr('disabled', false);
            }
		}
	});
	
	$('#password').blur(function(){
		var pass= $('#password').val();
        var pass_conf=$('#password_conf').val();
		if(pass === "" && pass_conf === ""){
			$('#error_missmatch').html('<label class="text-danger">Silahkan Isi Password Terlebih Dahulu</label>');
			$('#submit').attr('disabled', 'disabled');
		}
		else{
			if(pass!=pass_conf){
				$('#error_missmatch').html('<label class="text-danger">Password tidak cocok</label>');
				$('#submit').attr('disabled', 'disabled');
            }else{
				$('#error_missmatch').html('<label class="text-success">Password  cocok</label>');
				$('#submit').attr('disabled', false);
            }
		}
	});
});
</script>
@endsection