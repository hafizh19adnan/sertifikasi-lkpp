@extends('dashboard.layout')
@section('content')
<meta name="instansi" content="{{ $instansis }}" >
	<!-- page content -->
	<div class='right_col' role='main'>
		<div class=''>
            <div class='page-title'>
				<div class='title_left'>
					<h3>Data Asesi Baru</h3>
				</div>
				<div class='title_right'>
					<div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
				</div>
			</div>
            <div class='clearfix'></div>
            <div class='row'>
				<div class='col-md-12 col-sm-12 col-xs-12'>
					<div class='x_panel'>
						<div class='x_title'>
							<h2>Ubah Data Asesi </h2>
							<ul class='nav navbar-right panel_toolbox'>
								<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
							</ul>
							<div class='clearfix'></div>
						</div>
						<div class='x_content'>
							<!-- Smart Wizard -->                                      
							<form  method="POST" action="{{ route('asesi.update', $asesi[0]->id) }}" class='form-horizontal form-label-left' id='asesi_new' >
								@csrf
								@method('PUT')
								<input type="hidden" name="id" value="{{$asesi[0]->id}}">
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' name='nama' id='nama' required='required' value="{{$asesi[0]->nama}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>                         
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12'>Jenis Kelamin</label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<select class='select2_single form-control' id='jenis_kelamin' name='jenis_kelamin' tabindex='-1'>
												<option {{ $asesi[0]->jenis_kelamin == 'L' ? 'selected' : '' }}  value='L' >Laki-laki</option>
												<option {{ $asesi[0]->jenis_kelamin == 'P' ? 'selected' : '' }}  value='P' >Perempuan</option>
											</select>
										</div>                            
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tempat Lahir <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='tempat_lahir' name='tempat_lahir' value="{{$asesi[0]->tempat_lahir}}" required='required' class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12'>Tanggal Lahir <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input id='tanggal_lahir' name='tanggal_lahir' value="{{$asesi[0]->tanggal_lahir}}" class='date-picker form-control col-md-7 col-xs-12' required='required' type='date'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIK</label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='nik' name='nik' maxlength="16"  value="{{$asesi[0]->nik}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIP</label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='nip' name='nip' maxlength="18"  value="{{$asesi[0]->nip}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NPWP</label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='npwp' name='npwp' maxlength="18"  value="{{$asesi[0]->npwp}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Golongan <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<select id='golongan' class='select2_single form-control' name='golongan' tabindex='-1'>
												<option>{{$asesi[0]->golongan}}</option>
												<option {{ $asesi[0]->golongan == 'IA' ? 'selected' : '' }}>IA</option>
												<option {{ $asesi[0]->golongan == 'IB' ? 'selected' : '' }}>IB</option>
												<option {{ $asesi[0]->golongan == 'IC' ? 'selected' : '' }}>IC</option>
												<option {{ $asesi[0]->golongan == 'ID' ? 'selected' : '' }}>ID</option>
												<option {{ $asesi[0]->golongan == 'IIA' ? 'selected' : '' }}>IIA</option>
												<option {{ $asesi[0]->golongan == 'IIB' ? 'selected' : '' }}>IIB</option>
												<option {{ $asesi[0]->golongan == 'IIC' ? 'selected' : '' }}>IIC</option>
												<option {{ $asesi[0]->golongan == 'IID' ? 'selected' : '' }}>IID</option>
												<option {{ $asesi[0]->golongan == 'IIIA' ? 'selected' : '' }}>IIIA</option>
												<option {{ $asesi[0]->golongan == 'IIIB' ? 'selected' : '' }}>IIIB</option>
												<option {{ $asesi[0]->golongan == 'IIIC' ? 'selected' : '' }}>IIIC</option>
												<option {{ $asesi[0]->golongan == 'IIID' ? 'selected' : '' }}>IIID</option>
												<option {{ $asesi[0]->golongan == 'IVA' ? 'selected' : '' }}>IVA</option>
												<option {{ $asesi[0]->golongan == 'IVB' ? 'selected' : '' }}>IVB</option>
												<option {{ $asesi[0]->golongan == 'IVC' ? 'selected' : '' }}>IVC</option>
												<option {{ $asesi[0]->golongan == 'IVD' ? 'selected' : '' }}>IVD</option>
												<option {{ $asesi[0]->golongan == 'IVE' ? 'selected' : '' }}>IVE</option>
											</select>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Pangkat <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='pangkat' name='pangkat' value="{{$asesi[0]->pangkat}}" required='required' readonly class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='jabatan' name='jabatan' required='required' value="{{$asesi[0]->jabatan}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<select id='id_instansi' multiple="multiple" class='js-example-basic-multiple form-control' name='id_instansi' tabindex='-1'>
											@foreach ($instansis as $instansi)
												<option  value='{{ $instansi->id }}' {{ $instansi->id == $asesi[0]->id_instansi ? 'selected' : '' }}>{{ $instansi->nama_instansi }}</option>
											@endforeach
											</select>
										</div>
									</div>          
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Alamat <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='alamat' name='alamat' required='required' value="{{$asesi[0]->alamat}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>                        
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Provinsi<span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<select class="js-example-basic-single form-control" name="id_provinsi" tabindex="-1">
											@foreach ($provinsis as $provinsi)
												<option  value="{{ $provinsi->id }}"  {{ $provinsi->id == $selected_prov[0]->id ? 'selected' : '' }}>{{ $provinsi->nama_provinsi }}</option>                              
											@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Kota<span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<select class="select2_single form-control" name="id_kota" tabindex="-1">
											@foreach ($kotas as $kota)
												<option  value="{{ $kota->id }}"  {{ $kota->id == $asesi[0]->id_kota ? 'selected' : '' }}>{{ $kota->nama_kota }}</option>                              
											@endforeach
											</select>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor Telepon <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='number' id='nomor_telepon' name='nomor_telepon' value="{{$asesi[0]->nomor_telepon}}" required='required' class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class='form-group'>
										<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Email <span class='required'>*</span></label>
										<div class='col-md-6 col-sm-6 col-xs-12'>
											<input type='text' id='email' name='email' required='required' value="{{$asesi[0]->email}}" class='form-control col-md-7 col-xs-12'>
										</div>
									</div>
									<div class="ln_solid"></div>
										<h2> Data Pendidikan</h2>
										<div class="ln_solid"></div>                      
										<div id='pendidikan-container'><br>
											<input type='hidden' name='count_pendidikan' id='count_pendidikan' value='{{$count_pendidikan}}' />
											<div id=row_pendidikan1>
											@foreach($asesi_pendidikan as $pend)
												<div class='col-md-2'></div>
												<div class='col-md-10'>
													<input type='hidden' name='id_pendidikan{{++$loop->index}}' id='id_pendidikan{{$loop->index}}' value='{{$pend->id}}' />                            
													<hr>
													<strong><h4>Riwayat Pendidikan {{$loop->index }}</h4></strong><br>
												</div>                             
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Strata Pendidikan <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<select id='strata' class='select2_single form-control' name='strata{{$loop->index}}' tabindex='-1'>
															<option  {{ $pend->strata == 'D3' ? 'selected' : '' }}>D3</option>
															<option {{ $pend->strata == 'D4' ? 'selected' : '' }}>D4</option>
															<option {{ $pend->strata == 'S1' ? 'selected' : '' }}>S1</option>
															<option {{ $pend->strata == 'S2' ? 'selected' : '' }}>S2</option>
															<option {{ $pend->strata == 'S3' ? 'selected' : '' }}>S3</option>
														</select>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Perguruan Tinggi <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' data-validator='required' value="{{$pend->nama_lembaga}}" id='nama_lembaga' name='nama_lembaga{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jurusan <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' data-validator='required' id='jurusan' value="{{$pend->jurusan}}" name='jurusan{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>                            
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tahun Lulus <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='number' data-validator='required' id='tahun_lulus' value="{{$pend->tahun_lulus}}" name='tahun_lulus{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Gelar</label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='gelar' name='gelar{{$loop->index}}' value="{{$pend->gelar}}" class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
											@endforeach
											</div>                                                        
										</div>
										<div class='col-md-3'></div>
										<div class='col-md-9'>
											<button class='btn btn-primary' id='add_pendidikan'>Tambah Data Pendidikan</button>
										</div><br>
										<div class="ln_solid"></div>
										<h2>Data Pekerjaan</h2>
										<div class="ln_solid"></div>
										<div id='pekerjaan-container'>
											<input type='hidden' name='count_pekerjaan' id='count_pekerjaan' value='{{$count_pekerjaan}}' />
											<div id='row-pekerjaan1'>
											@foreach($asesi_pekerjaan as $work)                            
												<div class='col-md-2'></div>
												<input type='hidden' name='id_pekerjaan{{++$loop->index}}' id='id_pekerjaan{{$loop->index}}' value='{{$work->id}}' />                          
												<div class='col-md-10'>
													<strong><h4>Pekerjaan {{$loop->index}}</h4></strong><br>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<select id='id_instansi1' class='select2_single form-control' name='id_instansi{{$loop->index}}' tabindex='-1'>
														@foreach ($instansis as $instansi)
															<option  value='{{ $instansi->id }}' {{ $work->id_instansi == $instansi->id  ? 'selected' : '' }}>{{ $instansi->nama_instansi }}</option>
														@endforeach
														</select>
													</div>
												</div>           
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='jabatan_prev{{$loop->index++}}' value='{{$work->jabatan_prev}}' name='jabatan_prev{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='periode1' name='periode{{$loop->index}}'  value='{{$work->periode}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
											@endforeach
											</div>
										</div><br>
										<div class='col-md-3'></div>
										<div class='col-md-9'>
											<button class='btn btn-primary' id='add_pekerjaan'>Tambah Data Pekerjaan</button>
										</div><br><br>                      
										<div class="ln_solid"></div>
										<h2>Data Pengalaman</h2>
										<div class="ln_solid"></div>
										<div id='pengalaman-container'>
											<input type='hidden' name='count_pengalaman' id='count_pengalaman' value='{{$count_pengalaman}}' />
											@foreach($asesi_pengalaman as $exp)
											<div id='row-pengalaman1'>
												<div class='col-md-2'></div>
												<div class='col-md-10'>
													<input type='hidden' name='id_pengalaman{{++$loop->index}}' id='id_pengalaman{{$loop->index}}' value='{{$exp->id}}' />                          
													<strong><h4>Pengalaman {{$loop->index}}</h4></strong><br>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Pengalaman <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='deskripsi_pengalaman'  value='{{$exp->deskripsi_pengalaman}}' name='deskripsi_pengalaman{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												<div class='form-group'>                      
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Jabatan <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<select id='jabatan1' class='select2_single form-control' name='jabatan{{$loop->index}}'  tabindex='-1'>
															<option {{ $exp->jabatan == 'PPK'  ? 'selected' : '' }}>PPK</option>
															<option {{ $exp->jabatan == 'Pokja PP'  ? 'selected' : '' }}>Pokja PP</option>
															<option {{ $exp->jabatan == 'Pokja P'  ? 'selected' : '' }}>Pokja P</option>
															<option {{ $exp->jabatan == 'PJPHP'  ? 'selected' : '' }}>PJPHP</option>
														</select>                                   
													</div>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Masa Jabatan <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='masa_jabatan' name='masa_jabatan{{$loop->index}}' value='{{$exp->masa_jabatan}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>                          
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor SK <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='nomor_sk' name='nomor_sk{{$loop->index}}' value='{{$exp->nomor_sk}}' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												@endforeach
											</div>
										</div>
										<div class='col-md-3'></div>
										<div class='col-md-9'>
											<button class='btn btn-primary' id='add_pengalaman'>Tambah Data Pengalaman</button>
										</div><br>                     
										<div class="ln_solid"></div>
										<h2>Data Pendukung</h2>
										<div class="ln_solid"></div>
										<div id='skema-container'>
											<div class='col-md-2'></div>
											<div class='col-md-10'>
												<strong><h4>Skema Kompetensi</h4></strong><br>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Skema Kompetensi <span class="required">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												@foreach($skema as $skema)
													<div class="checkbox">
														<label><input class="flat" type="checkbox" id="id_skema" name="id_skema[]" value="{{$skema->id}}"
														@foreach($skema_asesi as $skm)
															@if($skema->id==$skm->id_skema)
																checked=1
																@break
															@endif
														@endforeach
														> {{$skema->nama_skema}}
														</label>
													</div>                                
												@endforeach
												</div>
											</div>              
										</div><br><br>
										<div class="ln_solid"></div>
										<div id='pendukung-container'>
											<div id='row-pendukung1'>
											@foreach($asesi_pendukung as $pendukung)
												<div class='col-md-2'></div>
												<input type='hidden' name='count_pendukung' id='count_pendukung' value='{{$count_pendukung}}' />                           
												<div class='col-md-10'>
													<strong><h4>Kompetensi Pendukung {{++$loop->index}}</h4></strong><br>
												</div>
												<div class='form-group'>
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Kompetensi Pendukung <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<input type='text' id='deskripsi_kompetensi' value='{{$pendukung->deskripsi_kompetensi}}' name='deskripsi_kompetensi1' required='required' class='form-control col-md-7 col-xs-12'>
													</div>
												</div>
												<div class='form-group'>                      
													<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Pendukung <span class='required'>*</span></label>
													<div class='col-md-6 col-sm-6 col-xs-12'>
														<select id='bukti' class='select2_single form-control' name='bukti1' tabindex='-1'>
															<option {{ $pendukung->bukti == 'Ada'  ? 'selected' : '' }}  value='Ada'>Ada</option>
															<option {{ $pendukung->bukti == 'Tidak Ada'  ? 'selected' : '' }}  value='Tidak Ada'>Tidak Ada</option>
														</select>
													</div>
												</div>
											@endforeach
											</div>
										</div>
										<div class='col-md-3'></div>
										<div class='col-md-9'>
											<button class='btn btn-primary' id='add_pendukung'>Tambah Data Kompetensi Pendukung</button>
										</div><br>
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button class="btn btn-primary" type="reset">Reset</button>
												<button type="submit" class="btn btn-success">Simpan Data</button>
											</div>
										</div>
									</form>
								</div>                    
							</div>
						</div>
					</div>
				</div>
			</div>

<!-- /page content -->
<!-- jQuery -->
<script src='/vendors/jquery/dist/jquery.min.js'></script>

<!-- Bootstrap -->
<script src='/vendors/bootstrap/dist/js/bootstrap.js'></script>
<script src='/build/js/custom.js'></script>

<!-- jQuery Smart Wizard -->
<script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
<script src='/build/js/jquery.form-validation.js'></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>

<!-- Custom Theme Scripts -->
<script src='/build/js/asesi-edit.js'></script>
<script>
$(document).ready(function(){
	$('.js-example-basic-multiple').select2({
		maximumSelectionLength: 1,
        allowClear: true
	});
    
	jQuery('select[name="golongan"]').on('change',function(){
		switch ($('#golongan').val()) {
			case 'IA':
				$('#pangkat').val('Juru Muda');
				break;
            case 'IB':
				$('#pangkat').val('Juru Muda Tingkat I');
				break;
            case 'IC':
				$('#pangkat').val('Juru');
				break;
            case 'ID':
				$('#pangkat').val('Juru Tingkat I');
				break;
            case 'IIA':
				$('#pangkat').val('Pengatur Muda');
				break;
			case 'IIB':
				$('#pangkat').val('Pengatur Muda Tingkat I');
				break;
            case 'IIC':
				$('#pangkat').val('Pengatur');
				break;
            case 'IID':
				$('#pangkat').val('Pengatur Tingkat I');
				break;
            case 'IIIA':
				$('#pangkat').val('Penata Muda');
				break;
            case 'IIIB':
                  $('#pangkat').val('Penata Muda Tingkat I');
                  break;
            case 'IIIC':
                  $('#pangkat').val('Penata');
                  break;
            case 'IIID':
                  $('#pangkat').val('Penata Tingkat I');
                  break;
			case 'IVA':
                  $('#pangkat').val('Pembina');
                  break;
            case 'IVB':
                  $('#pangkat').val('Pembina Tingkat I');
                  break;
            case 'IVC':
                  $('#pangkat').val('Pembina Utama Muda');
                  break;
            case 'IVD':
                  $('#pangkat').val('Pembina Utama Madya');
                  break;
            case 'IVE':
                  $('#pangkat').val('Pembina Utama');
                  break;  
		}
	});
    
	jQuery('select[name="id_provinsi"]').on('change',function(){
		var countryID = jQuery(this).val();
        if(countryID){
			jQuery.ajax({
				url : '/dashboard/getkota/' +countryID,
                type : "GET",
                dataType : "json",
                success:function(data){
					console.log(data);
                    jQuery('select[name="id_kota"]').empty();
                    jQuery.each(data, function(key,value){
						$('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
					});
				}
			});
		}
        else{
			$('select[name="id_kota"]').empty();
		}
	});
});
</script>
@endsection