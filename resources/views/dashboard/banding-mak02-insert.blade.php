@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form MAK02 </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/ujikom-banding'><i class='fa fa-arrow-left'></i> Kembali ke Rekap Banding</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Masukan Data MAK02</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('ujikom-banding.store') }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_asesi' name='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option  value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Unit Kompetensi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_unit' name='id_unit'>
                            @foreach ($unit as $unit)
                              <option  value='{{ $unit->id }}'>{{ $unit->nama_unit_kompetensi }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Alasan Banding <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='alasan_banding' name='alasan_banding' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div> 
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tanggal Asesmen <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='date' id='tanggal_asesmen' name='tanggal_asesmen' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div> 
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>1. Apakah proses banding telah dijelaskan kepada anda?<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='pertanyaan1'>
                          
                              <option>Ya</option>
                              <option>Tidak</option>
                        
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>2. Apakah anda telah mendiskusikan proses banding dengan asesor?<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='pertanyaan2'>
                          
                              <option>Ya</option>
                              <option>Tidak</option>
                        
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>3. Apakah anda mau melibatkan orang lain membantu anda dalam proses banding?<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='pertanyaan3'>
                          
                              <option>Ya</option>
                              <option>Tidak</option>
                        
                          </select>
                        </div>
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_skemas = $('#id_skema').val();
           
            jQuery.ajax({
              url : '/dashboard/getUnitBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  $('#jumlah-unit').empty();
                  $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                  $('#unit-container').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                    
                  });
                  
              }
            });
            $('#tes').click(function(){
              alert($('#out').val());
              alert($('#in').val());
            });
            
            jQuery('select[name="id_skema"]').on('change',function(){
                  var id_skema = jQuery(this).val();

                  if(id_skema)
                  {
                      jQuery.ajax({
                        url : '/dashboard/getAsesiBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="id_asesi"]').empty();
                            jQuery.each(data, function(key,value){
                              $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });
                           
                        }
                      });
                      jQuery.ajax({
                        url : '/dashboard/getUnitBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            // alert(data.length);
                            $('#jumlah-unit').empty();
                            $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                            $('#unit-container').empty();
                            jQuery.each(data, function(key,value){
                               $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                              
                            });
                           
                        }
                      });
                  }
                  else
                  {
                      $('select[name="id_kota"]').empty();
                  }
                });
          });
        </script>

   

@endsection