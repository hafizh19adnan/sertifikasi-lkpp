@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Data Pelaksanaan Uji Kompetensi</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group"></div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ubah Data Uji Kompetensi </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    
                    <form  method="POST" action="{{ route('ujikom.update', $ujikom->id) }}" data-parsley-validate class="form-horizontal form-label-left">
                      @csrf
                      @method('PUT')
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Judul Uji Kompetensi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="judul" name="judul" value="{{$ujikom->judul}}" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">TUK <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="select2_single form-control" name="tuk">
                              <option {{ $ujikom->tuk == 'Sewaktu' ? 'selected' : '' }}  value="Sewaktu" >Sewaktu</option>
                              <option {{ $ujikom->tuk == 'Mandiri' ? 'selected' : '' }} value="Mandiri">Mandiri</option>
                              <option {{ $ujikom->tuk == 'Tempat Ujian' ? 'selected' : '' }} value="Tempat Ujian">Tempat Ujian</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Tanggal Mulai <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" id="tanggal_mulai" value="{{$ujikom->tanggal_mulai}}" name="tanggal_mulai" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Tanggal Selesai <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" id="tanggal_selesai"value="{{$ujikom->tanggal_selesai}}" name="tanggal_selesai" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Lokasi Pelaksanaan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="tempat_uji" name="tempat_uji" value="{{$ujikom->tempat_uji}}" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Provinsi<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="id_provinsi" tabindex="-1">
                            @foreach ($provinsis as $provinsi)
                              <option  value="{{ $provinsi->id }}"  {{ $provinsi->id == $selected_prov[0]->id ? 'selected' : '' }}>{{ $provinsi->nama_provinsi }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kota<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="id_kota" tabindex="-1">
                          @foreach ($kotas as $kota)
                              <option  value="{{ $kota->id }}"  {{ $kota->id == $ujikom->id_kota ? 'selected' : '' }}>{{ $kota->nama_kota }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <a class="btn btn-primary" href="/dashboard/ujikom-skema-set/{{$ujikom->id}}">Atur Skema Kompetensi Ujikom</a>
                            
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                  
                  

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src="/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/build/js/custom.min.js"></script>
        <script>
      $(document).ready(function(){
       
        jQuery('select[name="id_provinsi"]').on('change',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/dashboard/getkota/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="id_kota"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="id_kota"]').empty();
               }
            });
      });

       
    </script>

   

@endsection