@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form Keputusan dan Umpan Balik (MAK-04) </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/asesmen-keputusan'><i class='fa fa-arrow-left'></i> Kembali ke Rekap MAK04</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Ubah Data Formulir MAK-04</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('asesmen-keputusan.update',$keputusan[0]->id) }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      @method('PUT')
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_skema' name='id_skema'>
                            @foreach ($skema as $skema)
                              <option {{ $skema->nama_skema == $keputusan[0]->nama_skema ? 'selected' : '' }}  value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option {{ $asesi->nama == $keputusan[0]->asesi ? 'selected' : '' }}   value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option {{ $asesor->nama == $keputusan[0]->asesor ? 'selected' : '' }}  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tanggal Asesmen <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='date' id='tempat_uji' name='tanggal' value='{{$keputusan[0]->tanggal}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tempat <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='tempat' value='{{$keputusan[0]->tempat}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Umpan balik terhadap pencapaian unjuk kerja: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='feedback_pencapaian' value='{{$keputusan[0]->feedback_pencapaian}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Identifikasi kesenjangan pencapaian unjuk kerja: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='identifikasi_kesenjangan' value='{{$keputusan[0]->identifikasi_kesenjangan}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Saran tindak lanjut hasil asesmen: <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='saran_tindak_lanjut' value='{{$keputusan[0]->saran_tindak_lanjut}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Rekomendasi Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='rekomendasi_asesor'>
                          
                              <option  {{ 'K' == $keputusan[0]->rekomendasi_asesor ? 'selected' : '' }} value='K'>Kompeten</option>
                              <option   {{ 'BK' == $keputusan[0]->rekomendasi_asesor ? 'selected' : '' }}value='BK'>Belum Kompeten</option>
                        
                          </select>
                        </div>
                      </div>
                   
                      <br>
                      <div class='ln_solid'></div>
                      <h4>Keputusan Unit Kompetensi</h4><hr>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Unit Kompetensi</th>
                          <th>Bukti Langsung</th>
                          <th>Bukti Tidak Langsung</th>
                          <th>Bukti Tambahan</th>
                          <th>Keputusan</th>
                          
                        </tr>
                      </thead>
                      <tbody id='unit-container'>
                        @foreach($detail as $detail)
                          <tr>
                         
                             
                             <input type='hidden' name='id_detail{{$loop->index}}' value='{{$detail->id}}'/>
                             <input type='hidden' name='id_unit_kompetensi{{$loop->index}}' value='{{$detail->id_unit}}'/>
                            
                            <td>{{$detail->nama_unit_kompetensi}}</td>
                            <td> 
                              <select class='select2_single form-control' name='bukti_langsung{{$loop->index}}'> 
                                <option {{ 'CLS' == $detail->bukti_langsung ? 'selected' : '' }} value='CLS'>CLS</option> 
                                <option  {{ 'SK' == $detail->bukti_langsung ? 'selected' : '' }}value='SK'>SK</option> 
                              </select> 
                            </td>
                            <td> 
                            <select class='select2_single form-control' name='bukti_tidak_langsung{{$loop->index}}'> 
                              <option  {{ 'CLP' == $detail->bukti_tidak_langsung ? 'selected' : '' }}  value='CLP'>CLP</option> 
                            </select>
                            </td>
                            
                            <td> 
                                <select class='select2_single form-control' name='bukti_tambahan{{$loop->index}}'> 
                                  <option  {{ 'DPL' == $detail->bukti_tambahan ? 'selected' : '' }} value='DPL'>DPL</option> 
                                  <option   {{ 'DPT' == $detail->bukti_tambahan ? 'selected' : '' }}value='DPT'>DPT</option> 
                                  <option  {{ 'DPW' == $detail->bukti_tambahan ? 'selected' : '' }} value='DPW'>DPW</option> 
                                </select> 
                            </td>
                            <td>
                                <select class='select2_single form-control' name='keputusan{{$loop->index}}'> 
                                <option  {{ 'K' == $detail->keputusan ? 'selected' : '' }}   value='K'>Kompeten</option> 
                                <option {{ 'BK' == $detail->keputusan ? 'selected' : '' }}  value='BK'>Belum Kompeten</option>
                            </select> 
                              </td>
                              
                          </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                      
                      <div id='jumlah-unit'>
                        <input type='hidden' name='jumlah_unit' value='{{$count}}'>
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        
   

@endsection