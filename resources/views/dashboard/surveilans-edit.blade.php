@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Ubah Hasil Surveilans </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href="{{ route('surveilans.index') }}"><i class='fa fa-arrow-left'></i> Kembali ke Rekap Surveilans</a>
            <br>       
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Ubah Data Hasil Surveilans</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('surveilans.update',$surveilans[0]->id) }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      @method('PUT')
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_skema' name='id_skema'>
                            @foreach ($skema as $skema)
                              <option {{ $skema->nama_skema == $surveilans[0]->nama_skema ? 'selected' : '' }} value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesi' id='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option  {{ $asesi->nama == $surveilans[0]->asesi ? 'selected' : '' }}  value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option {{ $asesor->nama == $surveilans[0]->asesor ? 'selected' : '' }}   value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tanggal <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='date' id='tempat_uji' name='tanggal' value='{{$surveilans[0]->tanggal}}' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi 
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='rekomendasi' value='{{$surveilans[0]->rekomendasi}}'  class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      
                      <br>
                      <h4>Penilaian Unit Kompetensi</h4><hr>
                      <div id='unit-container'>
                        @foreach($detail as $detail)
                        <div class='form-group'> 
                          <label class='control-label col-md-3 col-sm-3 col-xs-12'>{{++$loop->index}}. {{$detail->nama_unit_kompetensi}}<span class='required'>*</span></label> 
                          <div class='col-md-6 col-sm-6 col-xs-12'> 
                          <input type='hidden' name='id_detail{{$loop->index}}' value='{{$detail->id}}'> 
                          
                          <input type='hidden' name='id_unit_kompetensi{{$loop->index}}' value='{{$detail->id_unit_kompetensi}}'> 
                          <select class='select2_single form-control' name='hasil{{$loop->index}}'> 
                            <option  {{ 'Memadai' == $detail->hasil ? 'selected' : '' }}  value='Memadai'>Memadai</option> 
                            <option   {{ 'Terdapat Kesenjangan' == $detail->hasil ? 'selected' : '' }} value='Terdapat Kesenjangan'>Terdapat Kesenjangan</option> 
                          </select> 
                          </div> 
                        </div>
                        @endforeach 
                      </div>
                      <input type='hidden' value='{{$count}}' name='jumlah_unit'/>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' id='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
      
@endsection