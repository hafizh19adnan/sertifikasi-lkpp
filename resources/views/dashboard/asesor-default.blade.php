@extends('dashboard.layout')
@section('content')
       <!-- page content -->
       <div class="right_col" role="main">
          <div class="">
          <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">{{$count_int_active}}</div>
                  <h3 style="font-size:20px"> Asesor Internal Aktif</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">{{$count_int_inactive}}</div>
                  <h3 style="font-size:20px"> Asesor Internal Non-Aktif</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">{{$count_ext_active}}</div>
                  <h3 style="font-size:20px"> Asesor Eksternal Aktif</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">{{$count_ext_inactive}}</div>
                  <h3 style="font-size:20px"> Asesor Eksternal Non-Aktif</h3>
                  
                </div>
              </div>

            <div class="clearfix"></div>
            <a class="btn btn-success" href="/dashboard/asesor/create">Masukan Data Asesor</a>
           
            <div class="row">
                  @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($message = Session::get('error'))
                      <div class="alert alert-danger">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Asesor</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>Instansi</th>
                          <th>Kota</th>
                          <th>Jenis Asesor</th>
                          <th>Masa Berlaku</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($asesors as $asesor)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $asesor->nama}}</td>
                            <td>@if ($asesor->jenis_kelamin === 'L')
                                    Laki-Laki
                                @else
                                  Perempuan
                                @endif</td>
                            <td>{{ $asesor->nama_instansi}}</td>
                            <td>{{ $asesor->nama_kota}}</td>
                            <td>{{ $asesor->jenis_asesor}}</td>
                            <td>{{ $asesor->masa_berlaku}}</td>
                            <td>@if ($asesor->is_active === 1)
                                    Aktif
                                @else
                                  Non-Aktif
                                @endif</td>
                            
                            <td>
                              <a class="btn btn-success" href="{{ route('asesor.show',$asesor->id) }}">Detail</a>
                              <a class="btn btn-primary" href="{{ route('asesor.edit',$asesor->id) }}">Edit</a>
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                Hapus 
                              </button>
                            </td>
                        </tr>
                        @endforeach
                        
                        
                      
                      </tbody>
                    </table>
					  <!-- The Modal -->
            <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Konfirmasi</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <h5>Apakah anda yakin ingin menghapus?</h5>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <form action="{{ route('asesor.destroy',$asesor->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        </div>

                      </div>
                    </div>
                  </div>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>
    
@endsection