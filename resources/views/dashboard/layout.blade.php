<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/images/favicon.png" type="image/png" />

    <title>Dashboard Sertifikasi LKPP</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="/build/css/bootstrap-combobox.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/build/css/custom.min.css" rel="stylesheet">
</head>
<body  class="nav-md">
<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/dashboard" class="site_title"><span>UJIKOM LKPP</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/images/favicon.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>{{Auth::user()->username}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Beranda <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard">Statistik Umum</a></li>
                     
                    </ul>
                  </li>
                  @if(Auth::user()->role==='1'||Auth::user()->role==='2'||Auth::user()->role==='3')
                  <li><a><i class="fa fa-user"></i> Asesi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/asesi/create">Masukan Data Asesi (APL 01)</a></li>
                      <li><a href="/dashboard/apl02/create">Masukan Data APL 02</a></li>
                      <li><a href="/dashboard/asesi">Rekap Asesi</a></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> Asesor <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/asesor/create">Masukan Data Asesor</a></li>
                      <li><a href="/dashboard/asesor">Rekap Asesor</a></li>
                      <li><a href="/dashboard/penugasan-asesor">Penugasan Asesor</a></li>                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Uji Kompetensi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/unit-ujikom">Atur Unit Kompetensi</a></li> 
                      <li><a href="/dashboard/skema-ujikom">Atur Skema</a></li>
                      <li><a href="/dashboard/ujikom/create">Buat Jadwal</a></li>
                      <li><a href="/dashboard/ujikom">Data Pelaksanaan</a></li>
                      <li><a href="/dashboard/ujikom-persetujuan/create">Formulir Persetujuan & Kerahasiaan (MAK-01)</a></li>
                      <li><a href="/dashboard/asesmen-keputusan/create">Masukan Keputusan (MAK-02)</a></li>  

                      <li><a href="/dashboard/ujikom-banding/create">Formulir Banding (MAK-03)</a></li>
                     
                      <li><a href="/dashboard/feedback">Masukan Feedback (MAK-04)</a></li> 
                      <li><a href="/dashboard/ujikom-hasil">Masukan Hasil Ujikom(MAK-05)</a></li>   
                      <li><a href="/dashboard/asesmen-peninjauan/create">Formulir Peninjauan Asesmen (MAK-06)</a></li>
                                          
                    </ul>
                  </li>
                  <li><a><i class="fa fa-suitcase"></i> Surveilans <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/surveilans/create">Masukan Hasil Surveilans</a></li>
                      <li><a href="/dashboard/surveilans">Rekap Surveilans</a></li>
                                           
                    </ul>
                  </li>
                  @endif
                  @if(Auth::user()->role==='1'||Auth::user()->role==='4')
                  <li><a><i class="fa fa-file"></i> Sertifikat <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/sertifikat/create">Buat Pengajuan Sertifikat</a></li>
                      <li><a href="/dashboard/sertifikat">Rekap Pengajuan Sertifikat</a></li>
                      <li><a href="/dashboard/pengiriman-sertifikat">Atur Pengiriman Sertifikat</a></li>                   
                      <li><a href="/dashboard/tracking-sertifikat">Tracking Sertifikat</a></li>                   
                    </ul>
                  </li>
                  @endif
                  @if(Auth::user()->role==='1')
                  <li><a><i class="fa fa-database"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/dashboard/instansi">Instansi</a></li>
                      <li><a href="/dashboard/kota">Kota</a></li>
                      <li><a href="/dashboard/provinsi">Provinsi</a></li> 
                      <li><a href="/dashboard/user">User Data</a></li>
                      <li><a href="/dashboard/komponen-feedback">Komponen Feedback</a></li>   
                      <li><a href="/dashboard/kategori">Kategori Komponen</a></li>  
                      <li><a href="/dashboard/kegiatan">Kegiatan(Komponen MAK01)</a></li>                                   
                    </ul>
                  </li>
                  @endif
                 
                </ul>
              </div>
             
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small"></div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="/images/favicon.png" alt="">{{Auth::user()->username}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                    <a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i>{{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                  </ul>
                </li>

               
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
         @yield('content')
       
        
</div>
   
</body>
</html>