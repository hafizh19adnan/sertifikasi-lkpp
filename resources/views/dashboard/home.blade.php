@extends('dashboard.layout')

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<!-- top tiles -->
	<div class="row top_tiles">
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-user"></i></div>
				<div class="count">{{$count_asesi}}</div>
				<h3>Total Asesi</h3>
			</div>
		</div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-user"></i></div>
				<div class="count">{{$count_asesor}}</div>
				<h3>Total Asesor</h3>
			</div>
		</div>
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-building"></i></div>
				<div class="count">{{$count_instansi}}</div>
                <h3>Total Instansi</h3>
			</div>
		</div>
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-external-link"></i></div>
				<div class="count">{{$count_pelaksanaan}}</div>
				<h3>Total Pelaksanaan Ujikom</h3>
			</div>
		</div>
	</div>
    <!-- /top tiles -->

    <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tile fixed_height_240">
                <div class="x_title">
					<h2>Rekap Per Instansi </h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a class='btn btn-success' href='/dashboard/export-rekap-instansi'>Export Excel</a>
					<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Instansi</th>
								<th>Nama Skema</th>
								<th>Jumlah Kompeten</th>                          
							</tr>
						</thead>
						<tbody>
							@foreach ($rekap_instansi as $rekap_instansi)
							<tr>
								<td>{{ ++$loop->index }}</td>
								<td>{{ $rekap_instansi->nama_instansi }}</td>
								<td>{{ $rekap_instansi->nama_skema }}</td>
								<td>{{ $rekap_instansi->jumlah_kompeten }}</td>                          
							</tr>
							@endforeach
						</tbody>
					</table>
                </div>
			</div>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel tile fixed_height_240">
				<div class="x_title">
					<h2>Rekap Kompeten per Skema</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a class='btn btn-success' href='/dashboard/export-skema-kompeten'>Export Excel</a>
					<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th>#</th>
							  <th>Nama Skema Kompetensi</th>
							  <th>Jumlah Kompeten</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($skema_kompeten as $skema_kompeten)
							<tr>
								<td>{{ ++$loop->index }}</td>
								<td>{{ $skema_kompeten->nama_skema}}</td>
								<td>{{ $skema_kompeten->jumlah_kompeten}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
                </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel tile fixed_height_240">
                <div class="x_title">
					<h2>Rekap Belum Kompeten per Skema</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a class='btn btn-success' href='/dashboard/export-skema-non-kompeten'>Export Excel</a>
				
					<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Skema Kompetensi</th>
								<th>Jumlah Belum Kompeten</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($skema_non_kompeten as $skema_non_kompeten)
							<tr>
								<td>{{ ++$loop->index }}</td>
								<td>{{ $skema_non_kompeten->nama_skema}}</td>
								<td>{{ $skema_non_kompeten->jumlah_kompeten}}</td>                            
							</tr>
							@endforeach
						</tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel tile fixed_height_240">
                <div class="x_title">
					<h2>Rekap Asesi Belum Kompeten </h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a class='btn btn-success' href='/dashboard/export-asesi-non-kompeten'>Export Excel</a>
	
					<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Asesi</th>
								<th>Nama Asesor</th>
								<th>Skema Kompetensi</th>
								<th>Judul Ujikom</th>
								<th>Hasil</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($asesi_non_kompeten as $asesi_non_kompeten)
							<tr>
								<td>{{ ++$loop->index }}</td>
								<td>{{ $asesi_non_kompeten->nama_asesi }}</td>
								<td>{{ $asesi_non_kompeten->nama_asesor }}</td>
								<td>{{ $asesi_non_kompeten->nama_skema }}</td>
								<td>{{ $asesi_non_kompeten->judul }}</td>
								<td>{{ $asesi_non_kompeten->hasil }}</td>
							</tr>
							@endforeach
						</tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
	<br>
    <div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel tile fixed_height_240">
                <div class="x_title">
					<h2>Rekap Unit Kompeten </h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a href='/dashboard/export-unit-kompeten' class='btn btn-success'>Export Excel</a>
					<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Unit Kompetensi</th>
								<th>Jumlah Kompeten</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($unit_kompeten as $unit_kompeten)
							<tr>
								<td>{{ ++$loop->index }}</td>
								<td>{{ $unit_kompeten->nama_unit_kompetensi}}</td>
								<td>{{ $unit_kompeten->jumlah_kompeten}}</td>                            
							</tr>
							@endforeach
						</tbody>
                    </table>
                </div>
			</div>
		</div>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel tile fixed_height_240">
				<div class="x_title">
					<h2>Rekap Unit Belum Kompeten </h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
								<a href='/dashboard/export-unit-non-kompeten' class='btn btn-success'>Export Excel</a>
                <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

                      </thead>
                      <tbody>
                     
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Unit Kompetensi</th>
                          <th>Jumlah Belum Kompeten</th>
                       </tr>
                      </thead>
                      <tbody>
                      @foreach ($unit_non_kompeten as $unit_non_kompeten)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $unit_non_kompeten->nama_unit_kompetensi}}</td>
                            <td>{{ $unit_non_kompeten->jumlah_kompeten}}</td>
                            
                        </tr>
                        @endforeach
                        
                        
                      
                      </tbody>
                        
                      
                      </tbody>
                    </table>

                </div>
              </div>
            </div>
          </div>
          <br>
          

              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
       <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="/vendors/Flot/jquery.flot.js"></script>
    <script src="/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/vendors/moment/min/moment.min.js"></script>
    <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
     <!-- Datatables -->
     <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   \

    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

	
@endsection