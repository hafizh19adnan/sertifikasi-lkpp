@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>{{$ujikom[0]->judul}} </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/ujikom-hasil'><i class='fa fa-arrow-left'></i> Kembali ke Daftar Asesi</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Masukan Data Hasil Uji Kompetensi Baru</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action='/dashboard/ujikom-hasil/post' data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      <h4>Formulir MAK 06 (Hasil Penilaian Asesor)</h3>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>ID Uji Kompetensi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='judul' name='id_ujikom' value='{{$ujikom[0]->id}}' readonly required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Judul Uji Kompetensi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text'  value='{{$ujikom[0]->judul}}' readonly required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_skema' name='id_skema'>
                            @foreach ($skema as $skema)
                              <option  value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control'id='id_asesi' name='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option  value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_asesor' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Nomor Registrasi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='nomor_registrasi' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Aspek Positif <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='aspek_positif' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Aspek Negatif <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='aspek_negatif' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div> 
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'> Rekomendasi Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='hasil_skema'>
                          
                              <option  value='K'>Kompeten</option>
                              <option  value='BK'>Belum Kompeten</option>
                        
                          </select>
                        </div>
                      </div>
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='keterangan'>Keterangan <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' name='keterangan' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <br>
                      <h4>Penilaian Unit Kompetensi</h4><hr>
                      <div id='unit-container'>
                      </div>
                      <div id='jumlah-unit'>
                     
                      </div>
                      <div class='ln_solid'></div>
                      <h2>Hasil Keputusan Pleno</h2>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'> Rekomendasi Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='hasil_skema_pleno'>
                          
                              <option  value='K'>Kompeten</option>
                              <option  value='BK'>Belum Kompeten</option>
                        
                          </select>
                         
                        </div>
                      </div><br>
                        <h2 style='text-align:left'>Hasil Pleno Unit</h2><br>
                        <div id='unit-container-pleno'>
                        </div>
                      <div id='jumlah-unit-pleno'>
                     
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'id='submit'> Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_skemas = $('#id_skema').val();
          
            $('#id_asesor').change(function(){
              $('#submit').attr('disabled',false);
            }); 
            $('#id_asesi').change(function(){
              $('#submit').attr('disabled',false);
            });            
            jQuery.ajax({
              url : '/dashboard/getAsesiBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  jQuery.each(data, function(key,value){
                    console.log(value.id);
                    $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                  });
                  
              }
            });
            jQuery.ajax({
              url : '/dashboard/getUnitBySkema/' +id_skemas,
              type : "GET",
              dataType : "json",
              success:function(data)
              {
                  $('#jumlah-unit').empty();
                  $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                  $('#unit-container').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                    
                  });
                  $('#unit-container-pleno').empty();
                  jQuery.each(data, function(key,value){
                      $('#unit-container-pleno').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi_pleno"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil_pleno"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                    
                  });
                  
              }
            });
            jQuery('select[name="id_skema"]').on('change',function(){
                  var id_skema = jQuery(this).val();

                  if(id_skema)
                  {
                      jQuery.ajax({
                        url : '/dashboard/getAsesiBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="id_asesi"]').empty();
                            jQuery.each(data, function(key,value){
                              $('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });
                           
                        }
                      });
                      jQuery.ajax({
                        url : '/dashboard/getUnitBySkema/' +id_skema,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            // alert(data.length);
                            $('#jumlah-unit').empty();
                            $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                            $('#unit-container').empty();
                            jQuery.each(data, function(key,value){
                               $('#unit-container').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                              
                            });
                            $('#unit-container-pleno').empty();
                            jQuery.each(data, function(key,value){
                                $('#unit-container-pleno').append("<div class='form-group'> <label class='control-label col-md-3 col-sm-3 col-xs-12'>"+(key+1)+". "+value.nama_unit_kompetensi+"<span class='required'>*</span></label> <div class='col-md-6 col-sm-6 col-xs-12'> <input type='hidden' name='id_unit_kompetensi_pleno"+key+"' value='"+value.id_unit+"'> <select class='select2_single form-control' name='hasil_pleno"+key+"'> <option  value='K'>Kompeten</option> <option  value='BK'>Belum Kompeten</option> </select> </div> </div> ");
                              
                            });
                           
                        }
                      });
                  }
                  else
                  {
                      $('select[name="id_kota"]').empty();
                  }
                });
                var id_asesor = $('#id_asesor').val();
        

            if(id_asesor==nullq){
              alert('Belum Ada Asesor Yang ditugaskan untuk ujikom ini. Silahkan lakukan penugasan asesor dahulu')
              $('#submit').attr('disabled','disabled');
            }
          });
        </script>

   

@endsection