@extends('dashboard.layout')

@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Profil Asesi</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a class="btn btn-primary" href="/dashboard/asesi"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Asesi</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md- col-sm-3 col-xs-12 profile_left">
                      <div clpass="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="/images/favicon.png" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3> {{$asesi[0]->nama}}</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{$asesi[0]->nama_instansi}}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i>  {{$asesi[0]->pangkat}}
                        </li>

                        
                      </ul>

                      <a class="btn btn-success" href="{{ route('asesi.edit',$asesi[0]->id) }}"><i class="fa fa-edit m-right-xs" ></i>Edit Profile</a>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                      Hapus 
                                    </button>
                      
                      <br />

                      
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Pribadi</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Riwayat Pendidikan</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Riwayat Pekerjaan </a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Riwayat Pengalaman</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Data Lainnya</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Data Uji Kompetensi </a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <table class="data table no-margin">
                                <tbody>
                                    <tr>
                                        <th>Nama</th>
                                        <th>{{$asesi[0]->nama}}</th>
                                    <th>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <th>@if ($asesi[0]->jenis_kelamin === 'L')
                                              Laki-Laki
                                            @else
                                              Perempuan
                                            @endif
                                        </th>
                                    <th>
                                    <tr>
                                        <th>Tempat Lahir</th>
                                        <th>{{$asesi[0]->tempat_lahir}}</th>
                                    <th>
                                    <tr>
                                        <th>Tanggal Lahir</th>
                                        <th>{{$asesi[0]->tanggal_lahir}}</th>
                                    <th>
                                    <tr>
                                        <th>NIK</th>
                                        <th>{{$asesi[0]->nik}}</th>
                                    <th>
                                    <tr>
                                        <th>NIP</th>
                                        <th>{{$asesi[0]->nip}}</th>
                                    <th>
                                    <tr>
                                        <th>Pangkat</th>
                                        <th>{{$asesi[0]->pangkat}}</th>
                                    <th>
                                    <tr>
                                        <th>Golongan</th>
                                        <th>{{$asesi[0]->golongan}}</th>
                                    <th>
                                    <tr>
                                        <th>Jabatan</th>
                                        <th>{{$asesi[0]->jabatan}}</th>
                                    <th>
                                    <tr>
                                        <th>Alamat</th>
                                        <th>{{$asesi[0]->alamat}}</th>
                                    <th>
                                    <tr>
                                        <th>Kota</th>
                                        <th>{{$asesi[0]->nama_kota}}</th>
                                    <th>
                                    <tr>
                                        <th>Email</th>
                                        <th>{{$asesi[0]->email}}</th>
                                    <th>
                                    <tr>
                                        <th>No.Telepon</th>
                                        <th>{{$asesi[0]->nomor_telepon}}</th>
                                    <th>
                                </tbody>
                            </table>
                            <!-- end recent activity -->

                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <table class="data table  table-striped no-margin">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nama Lembaga</th>
                                <th>Jurusan</th>
                                <th>Tahun Lulus</th>
                                <th>Gelar</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($pendidikan as $pendidikan)
                              <tr>
                                  <td>{{ ++$loop->index }}</td>
                                  <td>{{ $pendidikan->nama_lembaga }}</td>
                                  <td>{{ $pendidikan->jurusan }}</td>
                                  <td>{{ $pendidikan->tahun_lulus }}</td>
                                  <td>{{ $pendidikan->gelar }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <table class="data table  table-striped no-margin">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nama Instansi</th>
                                <th>Nama Kota</th>
                                <th>Jabatan</th>
                                <th>Periode</th>
                          
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($pekerjaan as $pekerjaan)
                              <tr>
                                  <td>{{ ++$loop->index }}</td>
                                  <td>{{ $pekerjaan->nama_instansi }}</td>
                                  <td>{{ $pekerjaan->nama_kota }}</td>
                                  <td>{{ $pekerjaan->jabatan_prev }}</td>
                                  <td>{{ $pekerjaan->periode }}</td>    
                              </tr>
                              @endforeach
                              
                              
                            </tbody>
                          </table>
                            
                           
                            <!-- end user projects -->
                            
                          </div>
                         
                            <!--End Section-->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Deskripsi Pengalaman</th>
                                    <th>Jabatan</th>
                                    <th>Masa Jabatan</th>
                                    <th>Nomor SK</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($pengalaman as $pengalaman)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $pengalaman->deskripsi_pengalaman }}</td>
                                      <td>{{ $pengalaman->jabatan }}</td>
                                      <td>{{ $pengalaman->masa_jabatan }}</td>
                                      <td>{{ $pengalaman->nomor_sk }}</td>
                                     
                                  </tr>
                                  @endforeach
                                 
                                  
                                </tbody>
                              </table>
                            </div>

                              <!--End Section-->
                            <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                            <h2>Data Kompetensi Pendukung</h2><hr>
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Deskripsi Kompetensi Pendukung</th>
                                    <th>Bukti</th>                                  
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($pendukung as $pendukung)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $pendukung->deskripsi_kompetensi }}</td>
                                      <td>{{ $pendukung->bukti }}</td>
                                     
                                  </tr>
                                  @endforeach
                                 
                                  
                                </tbody>
                              </table><hr>
                              <h2>Data Kelengkapan</h2><hr>
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nama Berkas</th>
                                    <th>Bukti</th>                                  
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($kelengkapan as $kelengkapan)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $kelengkapan->nama_berkas }}</td>
                                      <td>{{ $pendukung->bukti }}</td>
                                     
                                  </tr>
                                  @endforeach
                                 
                                  
                                </tbody>
                              </table><hr>
                              <!-- <h2>Data APL02</h2><hr>
                              <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nama Asesor</th>
                                    <th>Skema</th>  
                                    <th>Rekomendasi Asesor</th>  
                                    <th>Catatan</th>                                 
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($apl02 as $apl02)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $apl02->nama }}</td>
                                      <td>{{ $apl02->nama_skema }}</td>
                                      <td>{{ $apl02->rekomendasi_asesor }}</td>
                                      <td>{{ $apl02->catatan }}</td>
                                  </tr>
                                  @endforeach
                                 
                                  
                                </tbody>
                              </table>-->
                            </div> 

                          <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
                            <h2>Status Skema Kompetensi</h2><hr>
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nama Skema</th>
                                    <th>Hasil</th>
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($skema as $skema)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $skema->nama_skema }}</td>
                                      <td>{{ $skema->hasil }}</td>
                                      
                                  </tr>
                                  @endforeach
                                 
                                </tbody>
                              </table>
                              <h2>Status Unit Kompetensi</h2><hr>
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nomor Unit Kompetensi</th>
                                    <th>Nama Unit Kompetensi</th>
                                    <th>Hasil</th>
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($unit_kompetensi as $unit_kompetensi)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $unit_kompetensi->nomor_unit_kompetensi }}</td>
                                      <td>{{ $unit_kompetensi->nama_unit_kompetensi }}</td>
                                      <td>{{ $unit_kompetensi->hasil }}</td>
                                      
                                  </tr>
                                  @endforeach
                                 
                                </tbody>
                              </table>
                            <h2>Ujikom yang Telah Diikuti</h2><hr>
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Skema</th>
                                    <th>Jadwal Pelaksanaan</th>
                                    <th>Nomor Registrasi</th>
                                    <th>Hasil</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($ujikom as $ujikom)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $ujikom->nama_skema}}</td>
                                      <td>{{ $ujikom->judul }}</td>
                                      <td>{{ $ujikom->nomor_registrasi }}</td>
                                      <td>{{ $ujikom->hasil }}</td>
                                      <td>
                                        <a class="btn btn-success" href="/dashboard/ujikom-hasil/detail/{{$ujikom->id}}">Lihat Detail</a>
                                       </td>
                                  </tr>
                                  @endforeach
                                 
                                </tbody>
                              </table>
                            </div>

                    <!-- The Modal -->
                    <div class="modal fade" id="myModal">
                      <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Konfirmasi</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <h5>Apakah anda yakin ingin menghapus?</h5>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <form action="{{ route('asesi.destroy',$asesi[0]->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        </div>

                      </div>
                    </div>
                  </div>
					
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="/vendors/raphael/raphael.min.js"></script>
    <script src="/vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/vendors/moment/min/moment.min.js"></script>
    <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

@endsection