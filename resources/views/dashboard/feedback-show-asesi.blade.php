@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Feedback Uji Kompetensi</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <a class="btn btn-primary" href="/dashboard/feedback"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Uji Kompetensi</a>
                   
            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Feedback Uji Kompetensi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    
                    <form method="POST" action="{{ route('feedback.store') }}" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">ID Uji Kompetensi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <p style='margin-top:10px'>{{$feedback_general[0]->id_ujikom}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Judul Uji Kompetensi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <p style='margin-top:10px'>{{$feedback_general[0]->judul}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Asesi<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <p style='margin-top:10px'>{{$feedback_general[0]->nama}}</p>
                        </div>
                      </div>
                      
                                            
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Feedback Umum <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <p style='margin-top:10px'>{{$feedback_general[0]->deskripsi_feedback}}</p>
                        </div>
                      </div>
                      <h2>Komponen Evaluasi</h2><hr>
                      
                      @foreach($detail as $detail)
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">{{++$loop->index}}.{{$detail->pernyataan}} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        {{$detail->hasil}}
                          <hr>
                        </div>
                      </div>
                      @endforeach
                        
                      <div class="ln_solid"></div>
                     

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src="/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/build/js/custom.min.js"></script>
@endsection