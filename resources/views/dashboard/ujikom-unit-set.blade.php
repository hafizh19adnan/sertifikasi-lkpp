@extends('dashboard.layout')
@section('content')
       <!-- page content -->
       <div class="right_col" role="main">
          <div class="">
          
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Atur Unit Kompetensi per Skema</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                 
                  <div class="x_content">
                    <br />
                    <form method="POST" action="/dashboard/ujikom-skema-store/" data-parsley-validate class="form-horizontal form-label-left">
                      @csrf
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_skema">ID Ujikom <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="id_ujikom" name="id_ujikom" value="{{$ujikom->id}}" readonly required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="judul">Judul Uji Kompetensi<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="judul" name="judul" value="{{$ujikom->judul}}" disabled required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Skema Uji Kompetensi<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" id='id_skema' name="id_skema">
                            @foreach ($skema as $skema)
                              <option  value="{{ $skema->id }}">{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>                    
                   
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" id='submit_data' class="btn btn-success">Simpan Data</button>
                        </div>
                      </div>

                    </form>
                  
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              
       
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Unit Kompetensi untuk {{$ujikom->judul}}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Skema</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($skema_ujikom as $skema_ujikom)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $skema_ujikom->nama_skema }}</td>
                            <td>
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                                Hapus 
                              </button>
                            </td>
                        </tr>
                        @endforeach
                      
                      </tbody>
                    </table>
                    <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Konfirmasi</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <h5>Apakah anda yakin ingin menghapus?</h5>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                        @if($count_skema_ujikom>0)
                        <form action="/dashboard/ujikom-skema-destroy/{{$skema_ujikom->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type='hidden' name='id_ujikom_redirect' value="{{$skema_ujikom->id_ujikom}}">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        @endif
                        </div>


                      </div>
                    </div>
                  </div>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- jQuery -->
        <script src="/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="/vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="/vendors/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="/build/js/custom.min.js"></script>
      
        <script>
        $(document).ready(function() {
          if($('#id_skema').val() == null){
            $('#submit_data').attr("disabled", true);
            $('#id_skema').attr("disabled", true);

          }
        });
        </script>
  
@endsection