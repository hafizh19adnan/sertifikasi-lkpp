@extends('dashboard.layout')
@section('content')

<!-- page content -->
<div class='right_col' role='main'>
	<div class=''>
		<div class='page-title'>
			<div class='title_left'>
				<h3>Form APL02 </h3>
			</div>
			<div class='title_right'>
				<div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
			</div>
		</div>
		<div class='clearfix'></div>
		<a class='btn btn-primary' href='/dashboard/asesi'><i class='fa fa-arrow-left'></i> Kembali ke Daftar Asesi</a>                   
		<div class='row'>
			<div class='col-md-12 col-sm-12 col-xs-12'>
				<div class='x_panel'>
					<div class='x_title'>
						<h2>Masukan Data APL02</h2>
						<ul class='nav navbar-right panel_toolbox'>
							<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
						</ul>
						<div class='clearfix'></div>
					</div>
					<div class='x_content'>
						<!-- Smart Wizard -->                    
						<form method='POST' action="{{ route('apl02.store') }}" data-parsley-validate class='form-horizontal form-label-left'>
							@csrf                      
							<div class='form-group'>
								<label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
								<div class='col-md-6 col-sm-6 col-xs-12'>
									<select class='select2_single form-control' name='id_asesi' id='id_asesi'>
									@foreach ($asesi as $asesi)
										<option  value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>								
									@endforeach
									</select>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
								<div class='col-md-6 col-sm-6 col-xs-12'>
									<select class='select2_single form-control' name='id_asesor'>
									@foreach ($asesor as $asesor)
										<option  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>                              
									@endforeach
									</select>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
								<div class='col-md-6 col-sm-6 col-xs-12'>
									<select class='select2_single form-control' id='id_skema' name='id_skema'>
									@foreach ($skema as $skema)
										<option  value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>                              
									@endforeach
									</select>
								</div>
							</div>                  
							<div class='form-group'>
								<label class='control-label col-md-3 col-sm-3 col-xs-12'>Rekomendasi Asesor<span class='required'>*</span></label>
								<div class='col-md-6 col-sm-6 col-xs-12'>
									<select class='select2_single form-control' name='rekomendasi_asesor'>                          
										<option>Dapat dilanjutkan</option>
										<option>Tidak dapat dilanjutkan</option>                        
									</select>
								</div>
							</div>
							<div class='form-group'>
								<label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Catatan <span class='required'>*</span></label>
								<div class='col-md-6 col-sm-6 col-xs-12'>
									<textarea rows="2" cols="50" id='tempat_uji' name='catatan' required="required" class="form-control col-md-7 col-xs-12"></textarea>
								</div>
							</div><br>
							<h4>Pertanyaan Unit Kompetensi</h4><hr>
							<input type='hidden' id='out' name='out' value='{{ count($unit) }}'/>
							<input type='hidden' id='in' name='in'value='{{ count($unit_pertanyaan) }}'/>                      
							<div id='pertanyaan-container'></div>
							<div class='ln_solid'></div>
							<div class='form-group'>
								<div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
									<button class='btn btn-primary' type='reset'>Reset</button>
									<button type='submit' id='submit' class='btn btn-success'>Simpan Data</button>
								</div>
							</div>
						</form>
                    </div>                   
				</div>
			</div>
		</div>
	</div>
</div>

<!-- /page content -->
<!-- jQuery -->
<script src='/vendors/jquery/dist/jquery.min.js'></script>

<!-- Bootstrap -->
<script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>

<!-- FastClick -->
<script src='/vendors/fastclick/lib/fastclick.js'></script>

<!-- NProgress -->
<script src='/vendors/nprogress/nprogress.js'></script>

<!-- jQuery Smart Wizard -->
<script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>

<!-- Custom Theme Scripts -->
<script src='/build/js/custom.min.js'></script>
<script>
$(document).ready(function(){
	var id_skemas = $('#id_skema').val();
    var id_asesi = $('#id_asesi').val();
    if(id_asesi==null){
		$('#submit').attr('disabled', 'disabled');
	}
    
	$('#id_asesi').change(function(){
		$('#submit').attr('disabled', false);
	});
           
    jQuery.ajax({
		url : '/dashboard/getSkemaUnitPertanyaan/' +id_skemas,
        type : "GET",
        dataType : "json",
        success:function(data)
        {
			console.log(data);
			$('#jumlah-unit').empty();
			$('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
			$('#pertanyaan-container').empty();
            var helper=0;
            var count_unit=0;
            var count_pertanyaan=data.length;
            for(var i=0;i<data.length;i++){
				if(i==0||data[i]['nomor_unit_kompetensi']!=data[i-1]['nomor_unit_kompetensi']){
					$('#pertanyaan-container').append("<h4>"+data[i]['nomor_unit_kompetensi']+" || "+data[i]['nama_unit_kompetensi']+"</h4> <input type='hidden' name='id_unit"+i+"' value='"+data[i]['id']+"' /> <table id='"+i+"' class='table table-striped table-bordered dt-responsive nowrap' cellspacing='0' width='100%'> <thead> <tr> <th>Daftar Pertanyaan</th> <th>Penilaian</th> <th>Bukti Pendukung</th> <th>Diisi Asesor</th> </tr> </thead> <tbody>");
					helper=i;
					count_unit+=1;
				}
                    
                if(i==0||data[i]['nomor_unit_kompetensi']==data[i-1]['nomor_unit_kompetensi']){
					$("#"+helper+" tbody").append("<tr>  <td><input type='hidden' name='id_unit"+i+"'value='"+data[i]['id']+"'/>"+data[i]['pertanyaan']+"  <input type='hidden' name='id_pertanyaan"+i+"' value='"+data[i]['id_pertanyaan']+"' /> </td> <td><select class='select2_single form-control' name='rekomendasi_asesor"+i+"'> <option>Kompeten</option> <option>Belum Kompeten</option> </select> </td> <td> <input type='text' id='tempat_uji' name='bukti_pendukung"+i+"'  class='form-control col-md-7 col-xs-12'> </td> <td><select class='select2_single form-control' name='isian_asesor"+i+"'> <option>V</option> <option>A</option> <option>T</option> <option>M</option> </select> </td> <tr>");                                          
				}else{
					$("#"+i+" tbody").append("<tr>  <td><input type='hidden' name='id_unit"+i+"'value='"+data[i]['id']+"'/>"+data[i]['pertanyaan']+"  <input type='hidden' name='id_pertanyaan"+i+"' value='"+data[i]['id_pertanyaan']+"' /> </td> <td><select class='select2_single form-control' name='rekomendasi_asesor"+i+"'> <option>Kompeten</option> <option>Belum Kompeten</option> </select> </td> <td> <input type='text' id='tempat_uji' name='bukti_pendukung"+i+"'  class='form-control col-md-7 col-xs-12'> </td> <td><select class='select2_single form-control' name='isian_asesor"+i+"'> <option>V</option> <option>A</option> <option>T</option> <option>M</option> </select> </td> <tr>");                      
				}                  
			}
			
			$('#out').val(count_unit);
            $('#in').val(count_pertanyaan);
		}
	});
	
	$('#tes').click(function(){
		alert($('#out').val());
		alert($('#in').val());
	});
            
    jQuery('select[name="id_skema"]').on('change',function(){
		var id_skema = jQuery(this).val();
		if(id_skema){
			jQuery.ajax({
				url : '/dashboard/getAsesiSkemaAPL02/' +id_skema,
                type : "GET",
                dataType : "json",
                success:function(data){
					console.log(data);
                    jQuery('select[name="id_asesi"]').empty();
                    jQuery.each(data, function(key,value){
						$('select[name="id_asesi"]').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
					});
                    var id_asesis = $('#id_asesi').val();
                    if(id_asesis==null){
						$('#submit').attr('disabled', 'disabled');
					}else{
						$('#submit').attr('disabled', false);
					}
				}
			});
            
			jQuery.ajax({
				url : '/dashboard/getSkemaUnitPertanyaan/' +id_skema,
                type : "GET",
                dataType : "json",
                success:function(data){
					console.log(data);
					$('#jumlah-unit').empty();
                    $('#jumlah-unit').append(" <input type='hidden' name='jumlah_unit' value="+data.length+">")
                    $('#pertanyaan-container').empty();
                    var helper=0;
                    var count_unit=0;
                    var count_pertanyaan=data.length;
                    for(var i=0;i<data.length;i++){
						if(i==0||data[i]['nomor_unit_kompetensi']!=data[i-1]['nomor_unit_kompetensi']){
							$('#pertanyaan-container').append("<h4>"+data[i]['nomor_unit_kompetensi']+" || "+data[i]['nama_unit_kompetensi']+"</h4> <input type='hidden' name='id_unit"+i+"' value='"+data[i]['id']+"' /> <table id='"+i+"' class='table table-striped table-bordered dt-responsive nowrap' cellspacing='0' width='100%'> <thead> <tr> <th>Daftar Pertanyaan</th> <th>Penilaian</th> <th>Bukti Pendukung</th> <th>Diisi Asesor</th> </tr> </thead> <tbody>");
							helper=i;
                            count_unit+=1;
						}else{
							$('#pertanyaan-container').append(" ");
						}
                            
                        if(i==0||data[i]['nomor_unit_kompetensi']==data[i-1]['nomor_unit_kompetensi']){
							$("#"+helper+" tbody").append("<tr><td><input type='hidden' name='id_unit"+i+"'value='"+data[i]['id']+"'/>"+data[i]['pertanyaan']+"  <input type='hidden' name='id_pertanyaan"+i+"' value='"+data[i]['id_pertanyaan']+"' /> </td> <td><select class='select2_single form-control' name='rekomendasi_asesor"+i+"'> <option>Kompeten</option> <option>Belum Kompeten</option> </select> </td> <td> <input type='text' id='tempat_uji' name='bukti_pendukung"+i+"'  class='form-control col-md-7 col-xs-12'> </td> <td><select class='select2_single form-control' name='isian_asesor"+i+"'> <option>V</option> <option>A</option> <option>T</option> <option>M</option> </select> </td> <tr>");                                                          
						}else{
							$("#"+i+" tbody").append("<tr>  <td><input type='hidden' name='id_unit"+i+"'value='"+data[i]['id']+"'/>"+data[i]['pertanyaan']+"  <input type='hidden' name='id_pertanyaan"+i+"' value='"+data[i]['id_pertanyaan']+"' /> </td> <td><select class='select2_single form-control' name='rekomendasi_asesor"+i+"'> <option>Kompeten</option> <option>Belum Kompeten</option> </select> </td> <td> <input type='text' id='tempat_uji' name='bukti_pendukung"+i+"'  class='form-control col-md-7 col-xs-12'> </td> <td><select class='select2_single form-control' name='isian_asesor"+i+"'> <option>V</option> <option>A</option> <option>T</option> <option>M</option> </select> </td> <tr>");
						}                          
					}
                    $('#out').val(count_unit);
                    $('#in').val(count_pertanyaan);
				}
			});                     
		}
        else{
			$('select[name="id_kota"]').empty();
		}
	});
});
</script>
@endsection