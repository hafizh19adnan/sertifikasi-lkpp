@extends('dashboard.layout')
@section('content')

<meta name="instansi" content="{{ $instansis }}" >
<!-- page content -->
	<div class='right_col' role='main'>
		<div class=''>
			<div class='page-title'>
				<div class='title_left'>
					<h3>Data asesor Baru</h3>
				</div>
				<div class='title_right'>
					<div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>
						<div class='input-group'></div>
					</div>
				</div>
            </div>
            <div class='clearfix'></div>
            <div class='row'>
				<div class='col-md-12 col-sm-12 col-xs-12'>
					<div class='x_panel'>
						<div class='x_title'>
							<h2>Ubah Data Asesor </h2>
							<ul class='nav navbar-right panel_toolbox'>
								<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
							</ul>
							<div class='clearfix'></div>
						</div>
						<div class='x_content'>
							<!-- Smart Wizard -->
							<form  method="POST" action="{{ route('asesor.update', $asesor->id) }}" class='form-horizontal form-label-left' id='asesor_new' >
								@csrf
								@method('PUT')
								<input type="hidden" name="id" value="{{$asesor->id}}">
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' name='nama' id='nama' required='required' value="{{$asesor->nama}}" class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Jenis Kelamin</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select class='select2_single form-control' id='jenis_kelamin' name='jenis_kelamin' tabindex='-1'>
											<option {{ $asesor->jenis_kelamin == 'L' ? 'selected' : '' }}  value='L' >Laki-laki</option>
											<option {{ $asesor->jenis_kelamin == 'P' ? 'selected' : '' }}  value='P' >Perempuan</option>
										</select>
									</div>                            
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tempat Lahir <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='tempat_lahir' name='tempat_lahir' value="{{$asesor->tempat_lahir}}" required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Tanggal Lahir <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input id='tanggal_lahir' name='tanggal_lahir' value="{{$asesor->tanggal_lahir}}" class='date-picker form-control col-md-7 col-xs-12' required='required' type='date'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIK </label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nik' maxlength='16' name='nik' value="{{$asesor->nik}}" class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIP</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nip' maxlength='18' name='nip' value="{{$asesor->nip}} " class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NPWP</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='npwp' maxlength='18' name='npwp' value="{{$asesor->npwp}}" class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Golongan <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select id='golongan' class='select2_single form-control' name='golongan' tabindex='-1'>
											<option {{ $asesor->golongan == 'IA' ? 'selected' : '' }}>IA</option>
											<option {{ $asesor->golongan == 'IB' ? 'selected' : '' }}>IB</option>
											<option {{ $asesor->golongan == 'IC' ? 'selected' : '' }}>IC</option>
											<option {{ $asesor->golongan == 'ID' ? 'selected' : '' }}>ID</option>
											<option {{ $asesor->golongan == 'IIA' ? 'selected' : '' }}>IIA</option>
											<option {{ $asesor->golongan == 'IIB' ? 'selected' : '' }}>IIB</option>
											<option {{ $asesor->golongan == 'IIC' ? 'selected' : '' }}>IIC</option>
											<option {{ $asesor->golongan == 'IID' ? 'selected' : '' }}>IID</option>
											<option {{ $asesor->golongan == 'IIIA' ? 'selected' : '' }}>IIIA</option>
											<option {{ $asesor->golongan == 'IIIB' ? 'selected' : '' }}>IIIB</option>
											<option {{ $asesor->golongan == 'IIIC' ? 'selected' : '' }}>IIIC</option>
											<option {{ $asesor->golongan == 'IIID' ? 'selected' : '' }}>IIID</option>
											<option {{ $asesor->golongan == 'IVA' ? 'selected' : '' }}>IVA</option>
											<option {{ $asesor->golongan == 'IVB' ? 'selected' : '' }}>IVB</option>
											<option {{ $asesor->golongan == 'IVC' ? 'selected' : '' }}>IVC</option>
											<option {{ $asesor->golongan == 'IVD' ? 'selected' : '' }}>IVD</option>
											<option {{ $asesor->golongan == 'IVE' ? 'selected' : '' }}>IVE</option>
										</select>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Pangkat <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='pangkat' value='{{ $asesor->pangkat }}' name='pangkat' required='required' readonly class='form-control col-md-7 col-xs-12'>
									</div>
								</div>                         
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='jabatan' name='jabatan' required='required' value="{{$asesor->jabatan}}" class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select id='id_instansi' multiple="multiple" class='js-example-basic-multiple form-control' name='id_instansi' tabindex='-1'>
										@foreach ($instansis as $instansi)
											<option  value='{{ $instansi->id }}' {{ $instansi->id == $asesor->id_instansi ? 'selected' : '' }}>{{ $instansi->nama_instansi }}</option>
										@endforeach
										</select>
									</div>
								</div>                                   
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor Telepon <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nomor_telepon' name='nomor_telepon' value="{{$asesor->nomor_telepon}}" required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Email <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='email' name='email' required='required' value="{{$asesor->email}}" class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Asesor</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="select2_single form-control" name="jenis_asesor" id="jenis_asesor" name="jenis_asesor" tabindex="-1">                          
											<option  value="Eksternal" {{ $asesor->jenis_asesor == "Eksternal" ? "selected" : '' }} >Eksternal</option>
											<option  value="Internal" {{$asesor->jenis_asesor == "Internal" ? "selected" : ''}}>Internal</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat-lahir">Aktif Sampai <span class="required">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="masa_berlaku" name="masa_berlaku" required="required" value="{{$asesor->masa_berlaku}}" class="form-control col-md-7 col-xs-12">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Status Keaktifan</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="select2_single form-control" name="is_active" id="is_active" name="is_active " tabindex="-1">                          
											<option  value="1" {{ $asesor->is_active == '1' ? 'selected' : '' }}>Aktif</option>
											<option  value="0" {{ $asesor->is_active == '0' ? 'selected' : '' }}>Non-Aktif</option>
										</select>
									</div>
								</div>
								<div class="ln_solid"></div>
								<div class="ln_solid"></div>
								<h2>Data Pekerjaan</h2>
								<div class="ln_solid"></div>
								<div id='pekerjaan-container'>
									<input type='hidden' name='count_pekerjaan' id='count_pekerjaan' value='{{count($asesor_pekerjaan)}}' />
									<div id='row-pekerjaan1'> 
									@foreach($asesor_pekerjaan as $work)
										<div class='col-md-2'></div>
										<div class='col-md-10'>
											<input type='hidden' name='id_pekerjaan{{++$loop->index}}' id='id_pekerjaan{{$loop->index}}' value='{{$work->id}}' />
											<strong><h4>Pekerjaan {{$loop->index}}</h4></strong><br>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<select id='id_instansi1' class='select2_single form-control' name='id_instansi{{$loop->index}}' tabindex='-1'>
												@foreach ($instansis as $instansi)
													<option  value='{{ $instansi->id }}' {{ $work->id_instansi == $instansi->id  ? 'selected' : '' }}>{{ $instansi->nama_instansi }}</option>
												@endforeach
												</select>
											</div>
										</div>           
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' id='jabatan_prev1' value='{{$work->jabatan_prev}}' name='jabatan_prev{{++$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' id='periode1' name='periode{{$loop->index}}'  value='{{$work->periode}}' required='required' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
									@endforeach
									</div>
								</div><br>
								<div class='col-md-3'></div>
								<div class='col-md-9'>
									<button class='btn btn-primary' id='add_pekerjaan'>Tambah Data Pekerjaan</button>
								</div><br><br>
								<div class="ln_solid"></div>
								<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
										<button class="btn btn-primary" type="reset">Reset</button>
										<button type="submit" class="btn btn-success">Simpan Data</button>
									</div>
								</div>
							</form>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
	
	<!-- /page content -->
    <!-- jQuery -->
    <script src='/vendors/jquery/dist/jquery.min.js'></script>
    
	<!-- Bootstrap -->
    <script src='/vendors/bootstrap/dist/js/bootstrap.js'></script>
    <script src='/build/js/custom.js'></script>
    
	<!-- jQuery Smart Wizard -->
    <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
    
	<!-- Custom Theme Scripts -->
    <script src='/build/js/asesor-edit.js'></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
    <script>
		$(document).ready(function(){
			$('.js-example-basic-multiple').select2({
				maximumSelectionLength: 1,
				allowClear: true
            });
            
			jQuery('select[name="golongan"]').on('change',function(){
				switch ($('#golongan').val()) {
					case 'IA':
						$('#pangkat').val('Juru Muda');
					break;
					case 'IB':
						$('#pangkat').val('Juru Muda Tingkat I');
					break;
					case 'IC':
						$('#pangkat').val('Juru');
					break;
					case 'ID':
						$('#pangkat').val('Juru Tingkat I');
					break;
					case 'IIA':
						$('#pangkat').val('Pengatur Muda');
					break;
					case 'IIB':
						$('#pangkat').val('Pengatur Muda Tingkat I');
					break;
					case 'IIC':
						$('#pangkat').val('Pengatur');
					break;
					case 'IID':
						$('#pangkat').val('Pengatur Tingkat I');
					break;
					case 'IIIA':
						$('#pangkat').val('Penata Muda');
					break;
					case 'IIIB':
						$('#pangkat').val('Penata Muda Tingkat I');
					break;
					case 'IIIC':
						$('#pangkat').val('Penata');
					break;
					case 'IIID':
						$('#pangkat').val('Penata Tingkat I');
					break;
					case 'IVA':
						$('#pangkat').val('Pembina');
					break;
					case 'IVB':
						$('#pangkat').val('Pembina Tingkat I');
					break;
					case 'IVC':
						$('#pangkat').val('Pembina Utama Muda');
					break;
					case 'IVD':
						$('#pangkat').val('Pembina Utama Madya');
					break;
					case 'IVE':
						$('#pangkat').val('Pembina Utama');
					break;  
				}
            });
		});
		
		$("input[name='nomor_telepon']").keypress(function(e){
			if(e.charCode < 48 || e.charCode > 57) {
				return false;
			}
		});
		
		$("input[name='masa_berlaku']").keypress(function(e){
			if(e.charCode < 48 || e.charCode > 57) {
				return false;
			}
		});
</script>
@endsection