@extends('dashboard.layout')

@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
    
          <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">2</div>
                  <h3>Total Peserta</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-building"></i></div>
                  <div class="count">2</div>
                  <h3>Asesi Kompeten</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                  <div class="count">179</div>
                  <h3>Asesi Tidak Kompeten</h3>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                  <div class="count">20%</div>
                  <h3>Persentase Kelulusan</h3>
                  
                </div>
              </div>
            <div class="page-title">
              <div class="title_left">
                <h3>Detail Ujikom</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group"></div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>
            @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a class="btn btn-primary" href="/dashboard/ujikom"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Pelaksanaan Ujikom</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Ujikom</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Daftar Asesor </a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Daftar Peserta   </a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Hasil Peserta   </a>
                          </li>
                         
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          
                            
                            <table class="data table no-margin">
                                <tbody>
                                    <tr>
                                        <th>Judul Kompetensi</th>
                                        <th></th>
                                        
                                    <th>
                                    <tr>
                                        <th>TUK</th>
                                        <th></th>
                                    <th>
                                    <tr>
                                        <th>Tanggal Mulai</th>
                                        <th></th>
                                    <th>
                                    <tr>
                                        <th>Tanggal Selesai</th>
                                        <th></th>
                                    <th>
                                    <tr>
                                        <th>Lokasi Pelaksanaan</th>
                                        <th></th>
                                    <th>
                                    <tr>
                                        <th>Kota</th>
                                        <th></th>
                                    <th>
                                  
                                    <tr>
                                        <th>Skema Kompetensi</th>
                                        <th> </th>
                                    <th>
                                   
                                    
                                </tbody>
                            </table>
                            
                         
                            <!-- end recent activity -->

                          </div>

                         
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Instansi</th>
                                  <th>Jenis Asesor</th>
                                  <th>Masa Berlaku</th>
                                  <th>Status</th>
                                
                                </tr>
                              </thead>
                              <tbody>
                             
                              
                              </tbody>
                            </table>
                           
                            <!-- end user projects -->
                            
                          </div>
                         
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>Instansi</th>
                          <th>Pangkat</th>
                          <th>Golongan</th>
                          <th>Jabatan</th>
                          <th>Kota</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                    
                        
                        
                      
                      </tbody>
                    </table>
                            </div>

                              <!--End Section-->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                          
                          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Instansi</th>
                                  <th>Pangkat</th>
                                  <th>Golongan</th>
                                  <th>Hasil</th>
                            
                              
                                </tr>
                              </thead>
                              <tbody>
                             
                                
                                
                              
                              </tbody>
                            </table>
                            </div>
                             <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    
                                </ul>

                               
                                
                                <br />

                                
                              </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="/vendors/raphael/raphael.min.js"></script>
    <script src="/vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/vendors/moment/min/moment.min.js"></script>
    <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

@endsection