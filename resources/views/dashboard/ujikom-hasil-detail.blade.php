@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          
            <div class="clearfix"></div>
            <a class="btn btn-primary" href="/dashboard/asesi/{{$hasil[0]->id_asesi}}"><i class="fa fa-arrow-left"></i> Kembali ke Data Asesi</a>
                   
            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Data Hasil Uji Kompetensi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    
                    <table class="data table no-margin">
                        <tbody>
                            <tr>
                                <th>Judul Uji Kompetensi</th>
                                <th>{{$hasil[0]->judul}}</th>
                                
                            <th>
                            <tr>
                                <th>Nama Asesi</th>
                                <th>{{$hasil[0]->asesi}}</th>
                            <th>
                            <tr>
                                <th>Nama Asesor</th>
                                <th>{{$hasil[0]->asesor}}</th>
                            <th>
                            <tr>
                                <th>Nomor Registrasi</th>
                                <th>{{$hasil[0]->nomor_registrasi}}</th>
                            <th>
                            <tr>
                                <th>Aspek Positif</th>
                                <th>{{$hasil[0]->aspek_positif}}</th>
                            <th>
                            <tr>
                                <th>Aspek Negatif</th>
                                <th>{{$hasil[0]->aspek_negatif}}</th>
                            <th>
                            <tr>
                                <th>Hasil</th>
                                <th>@if ($hasil[0]->hasil === 'K')
                                  Kompeten
                                @else
                                  Belum Kompeten
                                @endif</td>
                            </tr>
                            
                            
                        </tbody>
                    </table>
                            
                      <br>
                      <h4>Penilaian Unit Kompetensi</h4><hr>
                      <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nama Unit Kompetensi</th>
                                    <th>Nomor Unit Kompetensi</th>
                                   
                                    <th>Hasil</th>
                                 
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($hasil_detail as $hasil)
                                  <tr>
                                      <td>{{ ++$loop->index }}</td>
                                      <td>{{ $hasil->nama_unit_kompetensi }}</td>
                                      <td>{{ $hasil->nomor_unit_kompetensi }}</td>
                                      <td>{{ $hasil->hasil }}</td>
                                      
                                  </tr>
                                  @endforeach
                                 
                                </tbody>
                              </table>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src="/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/build/js/custom.min.js"></script>
    

   

@endsection