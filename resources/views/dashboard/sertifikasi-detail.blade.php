@extends('dashboard.layout')

@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Detail Sertifikat</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group"></div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a class="btn btn-primary" href="/dashboard/sertifikat"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Sertifikat</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
                    

                      
                      <br />

                      
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Sertifikat</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Pengiriman </a>
                          </li>
                         
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          
                            
                            <table class="data table no-margin">
                                <tbody>
                                    <tr>
                                        <th>Nama Asesi</th>
                                        <th>{{$sertifikat[0]->nama}}</th> 
                                    <th>
                                    <tr>
                                        <th>Judul Ujikom</th>
                                        <th>{{$sertifikat[0]->judul}}</th> 
                                    <th>
                                    <tr>
                                        <th>Tanggal Mulai</th>
                                        <th>{{$sertifikat[0]->tanggal_mulai}}</th> 
                                    <th>
                                    <tr>
                                        <th>Tanggal Selesai</th>
                                        <th>{{$sertifikat[0]->tanggal_selesai}}</th> 
                                    <th>
                                    <tr>
                                        <th>Nomor Sertifikat</th>
                                        <th>{{$sertifikat[0]->nomor_sertifikat}}</th> 
                                    <th>
                                    <tr>
                                        <th>Catatan</th>
                                        <th>{{$sertifikat[0]->catatan}}</th> 
                                    <th>
                                    <tr>
                                        <th>Tanggal Keluar</th>
                                        <th>{{$sertifikat[0]->tanggal_keluar}}</th> 
                                    <th>
                                    <tr>
                                        <th>Berlaku Sampai</th>
                                        <th>{{$sertifikat[0]->masa_berlaku}}</th> 
                                    <th>
                                
                                    
                                </tbody>
                            </table>
                            
                         
                            <!-- end recent activity -->

                          </div>

                         
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <!-- start user projects -->
                            <table class="data table no-margin">
                                <tbody>
                                    <tr>
                                        <th>Tanggal Kirim</th>
                                        <th>@if(count($pengiriman)>0)
                                              {{$pengiriman[0]->tanggal_kirim}}
                                            @endif
                                        </th> 
                                    <th>
                                    <tr>
                                        <th>Alamat Pengiriman</th>
                                        <th>@if(count($pengiriman)>0)
                                            {{$pengiriman[0]->alamat_pengiriman}}
                                            @endif
                                        </th> 
                                    <th>
                                    <tr>
                                        <th>Nomor Telepon PIC</th>
                                        <th>@if(count($pengiriman)>0)
                                            {{$pengiriman[0]->nomor_telepon_pic}}
                                            @endif
                                        </th> 
                                    <th>
                                    <tr>
                                        <th>Nomor Resi</th>
                                        <th> @if(count($pengiriman)>0)
                                              {{$pengiriman[0]->nomor_resi}}
                                            @endif
                                        </th> 
                                    <th>
                                   
                                
                                    
                                </tbody>
                            </table>
                            
                           
                            <!-- end user projects -->
                            
                          </div>
                         
                            <!--End Section-->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Jabatan</th>
                                    <th>Masa Jabatan</th>
                                    <th>Nomor SK</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>Kepala Pengadaan Dinas Pendidikan</td>
                                    <td>2010-2012</td>
                                    <td  >SK/1212/22/MS/21</td>
                                  
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td>Seksi Pengadaan Dinas Pendidikan</td>
                                    <td>2010-2012</td>
                                    <td  >SK/1212/22/MS/21</td>
                                  
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>Seksi Pengadaan Dinas Pendidikan</td>
                                    <td>2010-2012</td>
                                    <td>SK/1212/22/MS/21</td>
                                  
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td>Anggota Pengadaan Dinas Pendidikan</td>
                                    <td>2010-2012</td>
                                    <td>SK/1212/22/MS/21</td>
                                  
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>

                              <!--End Section-->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                            <table class="data table table-striped no-margin">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Judul Sertifikasi</th>
                                    <th>Nomor</th>
                                    <th>Tujuan Asesmen</th>
                                    <th>Status</th>
                                    <th>Keterangan</th>
                                    <th>Sertifikat</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>SERTIFIKASI  KOMPETENSI PENGELOLA PBJ MADYA</td>
                                    <td>SKM/D3.3/SSS/SIS/03</td>
                                    <td>Sertifikasi</td>
                                    <td>Kompeten</td>
                                    <td>SKKNI</td>
                                    <td><a class='btn btn-primary'>Lihat Sertifikat</a></td>
                                    <td><a class='btn btn-success'>Detail Ujian</a></td>

                                  
                                  </tr>
                                  <tr>
                                  <td>2</td>
                                    <td>SERTIFIKASI  KOMPETENSI PENGELOLA PBJ MADYA</td>
                                    <td>SKM/D3.3/SSS/SIS/03</td>
                                    <td>Sertifikasi</td>
                                    <td>Kompeten</td>
                                    <td>SKKNI</td>
                                    <td><a class='btn btn-primary'>Lihat Sertifikat</a></td>
                                    <td><a class='btn btn-success'>Detail Ujian</a></td>
                                  
                                  </tr>
                                
                                </tbody>
                              </table>
                            </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="/vendors/raphael/raphael.min.js"></script>
    <script src="/vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/vendors/moment/min/moment.min.js"></script>
    <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

@endsection