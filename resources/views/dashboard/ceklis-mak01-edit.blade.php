@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Form Ceklis Mengases Kompetensi (MAK-01) </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href='/dashboard/ceklis-kompetensi'><i class='fa fa-arrow-left'></i> Kembali ke Rekap MAK01</a>
                   
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Ubah Data Formulir MAK-01</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('ceklis-kompetensi.update', $ceklis[0]->id) }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      @method('PUT')
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Uji Kompetensi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_ujikom' name='id_ujikom'>
                            @foreach ($ujikom as $ujikom)
                              <option {{ $ujikom->judul == $ceklis[0]->judul ? 'selected' : '' }} value='{{ $ujikom->id }}'>{{ $ujikom->judul }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' id='id_skema' name='id_skema'>
                            @foreach ($skema as $skema)
                              <option  {{ $skema->nama_skema == $ceklis[0]->nama_skema ? 'selected' : '' }}  value='{{ $skema->id }}'>{{ $skema->nama_skema }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesi' id='id_asesi'>
                            @foreach ($asesi as $asesi)
                              <option  {{ $asesi->nama == $ceklis[0]->asesi ? 'selected' : '' }} value='{{ $asesi->id }}'>{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select class='select2_single form-control' name='id_asesor'>
                            @foreach ($asesor as $asesor)
                              <option  {{ $asesor->nama == $ceklis[0]->asesor ? 'selected' : '' }}  value='{{ $asesor->id }}'>{{ $asesor->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Waktu (dalam menit) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' value='{{$ceklis[0]->waktu_menit }}'id='tempat_uji' name='waktu_menit' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='tempat_uji' value='{{$ceklis[0]->rekomendasi}}' name='rekomendasi' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>
                     
                      <br>
                      <div class='ln_solid'></div>
                      <h4>Ceklis Kegiatan</h4><hr>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Kegiatan</th>
                          <th>Hasil</th>
                          <th>Catatan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <input type='hidden' name='jumlah_kegiatan' value='{{ $count }}' />
                        @foreach($detail as $kegiatan)
                          <tr>
                            <td>
                            <input type='hidden' value='{{$kegiatan->id}}' name='id_detail{{$loop->index}}'>
                              
                              <input type='hidden' value='{{$kegiatan->id_kegiatan}}' name='id_kegiatan{{$loop->index}}'>
                              {{$kegiatan->nama_kegiatan}}
                            </td>
                            <td>
                            <select class='select2_single form-control' name='hasil{{$loop->index}}'>
                                <option  {{ $kegiatan->hasil == 'Ya' ? 'selected' : '' }} >Ya</option>
                                <option  {{ $kegiatan->hasil == 'Tidak' ? 'selected' : '' }} >Tidak</option>
                            </select>
                            </td>
                            <td>
                            <input type='text' id='tempat_uji' value='{{$kegiatan->catatan}}' name='catatan{{$loop->index}}' required='required' class='form-control col-md-7 col-xs-12'>

                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                      
                      <div id='jumlah-unit'>
                     
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary' type='reset'>Reset</button>
                          <button type='submit' class='btn btn-success'>Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        <script>
          $(document).ready(function(){
            var id_asesi = $('#id_asesi').val();
            if(id_asesi==null){
              alert(id_asesi);
            }
          });
        </script>
       

@endsection