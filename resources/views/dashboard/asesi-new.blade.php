@extends('dashboard.layout')
@section('content')

<meta name='instansi' content='{{ $instansis }}' >
<!-- page content -->
	<div class='right_col' role='main'>
    <div class=''>
		<div class='page-title'>
			<div class='title_left'>
				<h3>Data Asesi Baru</h3>
			</div>
			<div class='title_right'>
				<div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
			</div>
		</div>
        <div class='clearfix'></div>
            <div class='row'>
				<div class='col-md-12 col-sm-12 col-xs-12'>
					<div class='x_panel'>
						<div class='x_title'>
							<h2>Masukan Data Asesi </h2>
							<ul class='nav navbar-right panel_toolbox'>
								<li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
							</ul>
							<div class='clearfix'></div>
						</div>
						<div class='x_content'>
						<!-- Smart Wizard -->                                      
							<form  method='POST' action="{{ route('asesi.store') }}" class='form-horizontal form-label-left' id='asesi_new' >
								@csrf
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' name='nama' id='nama' required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>                         
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Jenis Kelamin</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type="radio" id='jenis_kelamin' name='jenis_kelamin'  value="L"> Laki-Laki<br>
										<input  type="radio"  id='jenis_kelamin' name='jenis_kelamin' value="P"> Perempuan
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tempat Lahir <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='tempat_lahir' name='tempat_lahir' required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Tanggal Lahir <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input id='tanggal_lahir' name='tanggal_lahir'  class='date-picker form-control col-md-7 col-xs-12' required='required' type='date'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIK</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nik' name='nik' maxlength='16'  class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIP</label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nip' name='nip' maxlength='18'  class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Golongan <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select id='golongan' class='select2_single form-control' name='golongan' id='golongan' tabindex='-1'>
											<option value=null>--Pilih Pangkat--</option>
											<option>IIIA</option>
											<option>IIIB</option>
											<option>IIIC</option>
											<option>IIID</option>
											<option>IVA</option>
											<option>IVB</option>
											<option>IVC</option>
											<option>IVD</option>
											<option>IVE</option>
										</select>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Pangkat <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='pangkat' name='pangkat' id='pangkat' required='required' readonly class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='jabatan' name='jabatan' required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select id='id_instansi'  class=' form-control' name='id_instansi' tabindex='-1'>
										@foreach ($instansis as $instansi)
											<option  value='{{ $instansi->id }}'>{{ $instansi->nama_instansi }}</option>
										@endforeach
										</select>
									</div>
								</div>          
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Alamat <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<textarea rows="2" cols="50" id="last-name" name="alamat" required="required" class="form-control col-md-7 col-xs-12"></textarea>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Provinsi<span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select class='js-example-basic-single form-control' name='id_provinsi' id='id_provinsi' tabindex='-1'>
											<option value='default' readonly>--Pilih Provinsi--</option>
											@foreach ($provinsis as $provinsi)
												<option  value='{{ $provinsi->id }}'>{{ $provinsi->nama_provinsi }}</option>                              
											@endforeach
										</select>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12'>Kota<span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<select class='select2_single form-control' name='id_kota' id='id_kota' tabindex='-1'>
											<option value='default'>--Pilih Kota--</option>
										</select>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor Telepon <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='nomor_telepon' name='nomor_telepon' required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<div class='form-group'>
									<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Email <span class='required'>*</span></label>
									<div class='col-md-6 col-sm-6 col-xs-12'>
										<input type='text' id='email' name='email' required='required' class='form-control col-md-7 col-xs-12'>
									</div>
								</div>
								<input type='hidden' id='count_edu' name='count_edu' value='1'/>
								<input type='hidden' id='count_work' name='count_work' value='1'/>
								<input type='hidden' id='count_exp' name='count_exp' value='1'/>
								<input type='hidden' id='count_pendukung' name='count_pendukung' value='1'/>
								<input type='hidden' id='count_kelengkapan' name='count_kelengkapan' value='1'/>
								<div class='ln_solid'></div>
								<h2> Data Pendidikan</h2>
								<div class='ln_solid'></div>
								<div id='pendidikan-container'>
									<br>
									<div id=row_pendidikan1>
										<div class='col-md-2'></div>
										<div class='col-md-10'>
											<hr>
											<strong><h4>Riwayat Pendidikan 1</h4></strong><br>
										</div>                             
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Strata Pendidikan <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<select id='strata' class='select2_single form-control' name='strata1' tabindex='-1'>
													<option>D3</option>
													<option>D4</option>
													<option>S1</option>
													<option>S2</option>
													<option>S3</option>
												</select>
											</div>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Perguruan Tinggi <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' data-validator='required' id='nama_lembaga' name='nama_lembaga1' required='required' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jurusan <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' data-validator='required' id='jurusan' name='jurusan1' required='required' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tahun Lulus <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' data-validator='required' id='tahun_lulus' name='tahun_lulus1' required='required' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Gelar</label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
												<input type='text' id='gelar' name='gelar1' class='form-control col-md-7 col-xs-12'>
											</div>
										</div>
									</div>
								</div>
								<div class='col-md-3'></div>
									<div class='col-md-9'>
										<button class='btn btn-primary' id='add_pendidikan'>Tambah Data Pendidikan</button>
									</div>
									<br>
									<div class='ln_solid'></div>
									<h2>Data Pekerjaan</h2>
									<div class='ln_solid'></div>
									<div id='pekerjaan-container'>                        
										<div id='row-pekerjaan1'>                        
											<div class='col-md-2'></div>                            
											<div class='col-md-10'>
												<strong><h4>Pekerjaan 1</h4></strong><br>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<select id='id_instansi1' class='select2_single form-control' name='id_instansi1' tabindex='-1'>
													@foreach ($instansis as $instansi)
														<option  value='{{ $instansi->id }}'>{{ $instansi->nama_instansi }}</option>
													@endforeach
													</select>
												</div>
											</div>           
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='jabatan_prev1' name='jabatan_prev1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='periode1' name='periode1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
										</div>
									</div>
									<br>
									<div class='col-md-3'></div>
									<div class='col-md-9'>
										<button class='btn btn-primary' id='add_pekerjaan'>Tambah Data Pekerjaan</button>
									</div>
									<br><br>
									<div class='ln_solid'></div>
									<h2>Data Pengalaman</h2>
									<div class='ln_solid'></div>
									<div id='pengalaman-container'>                       
										<div id='row-pengalaman1'>
											<div class='col-md-2'></div>
											<div class='col-md-10'>
												<strong><h4>Pengalaman 1</h4></strong><br>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Pengalaman <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='deskripsi_pengalaman'  name='deskripsi_pengalaman1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
											<div class='form-group'>                      
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Jabatan <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<select id='jabatan1' class='select2_single form-control' name='jabatan1' tabindex='-1'>
														<option>PPK</option>
														<option>Pokja PP</option>
														<option>Pokja P</option>
														<option>PJPHP</option>
													</select>
												</div>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Masa Jabatan <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='masa_jabatan' name='masa_jabatan1'  required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor SK <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='nomor_sk' name='nomor_sk1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>               
										</div>
									</div>
									<div class='col-md-3'></div>
									<div class='col-md-9'>
										<button class='btn btn-primary' id='add_pengalaman'>Tambah Data Pengalaman</button>
									</div><br>                      
									<div class='ln_solid'></div>
									<h2>Data Pendukung</h2>
									<div class='ln_solid'></div>
									<div id='skema-container'>
										<div class='col-md-2'></div>
										<div class='col-md-10'>
											<strong><h4>Skema Kompetensi</h4></strong><br>
										</div>
										<div class='form-group'>
											<label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Skema Kompetensi <span class='required'>*</span></label>
											<div class='col-md-6 col-sm-6 col-xs-12'>
											@foreach($skema as $skema)
												<div class='checkbox'>
													<label><input class='flat' type='checkbox' id='id_skema' name='id_skema[]' value='{{$skema->id}}'> {{$skema->nama_skema}}</label>
												</div>
											@endforeach
											</div>
										</div>              
									</div>
									<br><br>
									<div class='ln_solid'></div>
									<div id='kelengkapan-container'>
										<div id='row-kelengkapan1'>
											<div class='col-md-2'></div>
											<div class='col-md-10'>
												<strong><h4>Bukti Kelengkapan 1</h4></strong><br>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama Berkas Persyaratan <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='kelengkapan1' name='nama_berkas1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
											<div class='form-group'>                      
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Kelengkapan <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<select id='bukti' class='select2_single form-control' name='bukti1' tabindex='-1'>
														<option>Ada(Memenuhi Syarat)</option>
														<option>Ada(Tidak Memenuhi Syarat)</option>
														<option>Tidak Ada</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class='col-md-3'></div>
									<div class='col-md-9'>
										<button class='btn btn-primary' id='add_kelengkapan'>Tambah Data Bukti Kelengkapan</button>
									</div><br>
									<div class='ln_solid'></div>
									<div id='pendukung-container'>
										<div id='row-pendukung1'>
											<div class='col-md-2'></div>
											<div class='col-md-10'>
												<strong><h4>Bukti Kompetensi yang Relevan 1</h4></strong><br>
											</div>
											<div class='form-group'>
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Deskripsi Kompetensi Pendukung <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<input type='text' id='deskripsi_kompetensi' name='deskripsi_kompetensi1' required='required' class='form-control col-md-7 col-xs-12'>
												</div>
											</div>
											<div class='form-group'>                      
												<label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Bukti Pendukung <span class='required'>*</span></label>
												<div class='col-md-6 col-sm-6 col-xs-12'>
													<select id='bukti' class='select2_single form-control' name='bukti_lengkap1' tabindex='-1'>
														<option value='Ada'>Ada</option>
														<option value='Tidak Ada'>Tidak Ada</option>
													</select>
												</div>
											</div>                       
										</div>
									</div>
									<div class='col-md-3'></div>
									<div class='col-md-9'>
										<button class='btn btn-primary' id='add_pendukung'>Tambah Data Kompetensi Pendukung</button>
									</div><br>                        
									<div class='ln_solid'></div>
									<div class='form-group'>
										<div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
											<button class='btn btn-primary' type='reset'>Reset</button>
											<button type='submit' id='submit' class='btn btn-success'>Simpan Data</button>
										</div>
									</div>
							</form>
						</div>
					</div>
                </div>
			</div>
        </div>
	</div>
</div>
<!-- /page content -->

<!-- jQuery -->
<script src='/vendors/jquery/dist/jquery.min.js'></script>

<!-- Bootstrap -->
<script src='/vendors/bootstrap/dist/js/bootstrap.js'></script>
<script src='/build/js/custom.js'></script>

<!-- jQuery Smart Wizard -->
<script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
<script src='/build/js/jquery.form-validation.js'></script>
<link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css' rel='stylesheet' />
<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js'></script>

<!-- Custom Theme Scripts -->
<script src='/build/js/asesi-new.js'></script>
<script>
	$(document).ready(function(){
		var id_provinsi=$('#id_provinsi').val();
        var id_kota = $('#id_kota').val();
        if(id_provinsi=='default'||id_kota=='default'){
			$('#submit').attr('disabled', 'disabled');
		}
	});
</script>
@endsection