@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Masukan Hasil Surveilans </h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class='clearfix'></div>
            <a class='btn btn-primary' href="{{ route('surveilans.index') }}"><i class='fa fa-arrow-left'></i> Kembali ke Rekap Surveilans</a>
            <br>       
            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Detail Data Hasil Surveilans</h2>
                    <ul class='nav navbar-right panel_toolbox'>
                      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
                    </ul>
                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                    
                    <form method='POST' action="{{ route('surveilans.store') }}" data-parsley-validate class='form-horizontal form-label-left'>
                      @csrf
                      
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Skema<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$surveilans[0]->nama_skema}}</P>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesi<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$surveilans[0]->asesi}}</P>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>Asesor<span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$surveilans[0]->asesor}}</P>
                        </div>
                      </div>
                                            
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Tanggal <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$surveilans[0]->tanggal}}</P>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='nama_provinsi'>Rekomendasi 
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                        <P style='margin-top:10px'>{{$surveilans[0]->rekomendasi}}</P>
                        </div>
                      </div>
                      
                      <br>
                      <h4>Penilaian Unit Kompetensi</h4><hr>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Unit Kompetensi</th>
                          <th>Hasil</th>
                        
                        </tr>
                      </thead>
                      <tbody id='unit-container'>
                        @foreach($detail as $detail)
                          <tr>
                            <td>{{$detail->nama_unit_kompetensi}}</td>
                            <td>{{$detail->hasil}}</td>
                        
                          </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                      
                      <div class='ln_solid'></div>
                      
                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.min.js'></script>
        <!-- FastClick -->
        <script src='/vendors/fastclick/lib/fastclick.js'></script>
        <!-- NProgress -->
        <script src='/vendors/nprogress/nprogress.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/custom.min.js'></script>
        
   

@endsection