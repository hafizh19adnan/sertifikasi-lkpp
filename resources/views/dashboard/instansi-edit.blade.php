@extends('dashboard.layout')
@section('content')
       
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Ubah Data Instansi</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@if ($message = Session::get('success'))
							<div class="alert alert-success">
								<p>{{ $message }}</p>
							</div>
						@endif
						@if ($errors->any())
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<br />
						<form method="POST" action="{{ route('instansi.update', $instansi->id) }}"  id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Instansi <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="first-name" name="nama_instansi" value="{{ $instansi->nama_instansi }}" required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea rows="4" cols="50" id="last-name" name="alamat" required="required" class="form-control col-md-7 col-xs-12">{{ $instansi->alamat }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="last-name" name="email" value="{{ $instansi->email }}" required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Telepon <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" name="nomor_telepon" value="{{ $instansi->nomor_telepon }}" required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode Pos <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="last-name" name="kode_pos" value="{{ $instansi->kode_pos }}" required="required" class="form-control col-md-7 col-xs-12" maxlength="5">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Provinsi<span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="select2_single form-control" name="id_provinsi" tabindex="-1">
										@foreach ($provinsi as $provinsi)
											<option  value="{{ $provinsi->id }}"  {{ $provinsi->id == $selected_prov[0]->id ? 'selected' : '' }}>{{ $provinsi->nama_provinsi }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Kota<span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select class="select2_single form-control" name="id_kota" tabindex="-1">
										@foreach ($kotas as $kota)
											<option  value="{{ $kota->id }}"  {{ $kota->id == $instansi->id_kota ? 'selected' : '' }}>{{ $kota->nama_kota }}</option>                              
										@endforeach
									</select>
								</div>
							</div>                     
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<button class="btn btn-primary" type="reset">Reset</button>
									<button type="submit" class="btn btn-success">Simpan Data</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- /page content -->

<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>

<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>

<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>

<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="/build/js/custom.min.js"></script>
<script>
    $(document).ready(function(){
      jQuery('select[name="id_provinsi"]').on('change',function(){
         var countryID = jQuery(this).val();
         if(countryID)
         {
            jQuery.ajax({
               url : '/dashboard/getkota/' +countryID,
               type : "GET",
               dataType : "json",
               success:function(data)
               {
                  console.log(data);
                  jQuery('select[name="id_kota"]').empty();
                  jQuery.each(data, function(key,value){
                     $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                  });
               }
            });
         }
         else
         {
            $('select[name="id_kota"]').empty();
         }
      });
    });
	
	$( "input[name='kode_pos']" ).keypress(function(e){
		if(e.charCode < 48 || e.charCode > 57) {
			return false;
		}
	});     
</script>
@endsection