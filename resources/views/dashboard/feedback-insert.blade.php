@extends('dashboard.layout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{{$ujikom[0]->judul}} </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <a class="btn btn-primary" href="/dashboard/feedback"><i class="fa fa-arrow-left"></i> Kembali ke Daftar Uji Kompetensi</a>
                   
            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Masukan Feedback Uji Kompetensi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    
                    <form method="POST" action="{{ route('feedback.store') }}" data-parsley-validate class="form-horizontal form-label-left">
                      @csrf
                      <input type="hidden" value="{{$count_komponen}}" name="count_komponen" />
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">ID Uji Kompetensi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="judul" name="id_ujikom" value="{{$ujikom[0]->id}}" readonly required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Judul Uji Kompetensi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"  value="{{$ujikom[0]->judul}}" readonly required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Asesi<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="id_asesi">
                            @foreach ($asesi as $asesi)
                              <option  value="{{ $asesi->id }}">{{ $asesi->nama }}</option>
                              
                            @endforeach
                          </select>
                        </div>
                      </div>
                      
                                            
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">Feedback Umum <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="tempat_uji" name="deskripsi_feedback" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <h2>Komponen Evaluasi</h2><hr>
                      @foreach($komponen as $komponen)
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_provinsi">{{$loop->index}}.{{$komponen->pernyataan}} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="hidden" name="id_komponen{{$loop->index}}" value="{{$komponen->id}}" />
                          <select class="select2_single form-control" name="hasil{{$loop->index}}">
                            <option>Ya</option>
                            <option>Tidak</option>
                          </select>
                          <hr>
                        </div>
                      </div>
                      @endforeach
                        
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Simpan Data</button>
                        </div>
                      </div>

                    </form>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src="/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/vendors/nprogress/nprogress.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/build/js/custom.min.js"></script>
@endsection