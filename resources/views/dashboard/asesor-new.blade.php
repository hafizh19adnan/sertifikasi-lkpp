@extends('dashboard.layout')

@section('content')
        <meta name="instansi" content="{{ $instansis }}" >
        <!-- page content -->
        <div class='right_col' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Data asesor Baru</h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'></div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Masukan Data Asesi Baru</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class='x_content'>


                    <!-- Smart Wizard -->
                     
                        <form  method="POST" action="{{ route('asesor.store')}}" class='form-horizontal form-label-left' id='asesor_new' >
                        @csrf
                        <input type='hidden' name='count_pekerjaan' id='count_pekerjaan' value='1' />
                  
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor Registrasi <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nomor_registrasi" name="nomor_registrasi" required="required" class="form-control col-md-7 col-xs-12">
                            <span id="error_instansi"></span>
                          </div>
                        </div>
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='first-name'>Nama <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' name='nama' id='nama' required='required'  class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                         
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12'>Jenis Kelamin</label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                            <input type="radio" id='jenis_kelamin' name='jenis_kelamin'  value="L"> Laki-Laki<br>
                            <input  type="radio"  id='jenis_kelamin' name='jenis_kelamin' value="P"> Perempuan
                          
                          </div>
                            
                          </div>
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Tempat Lahir <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='tempat_lahir' name='tempat_lahir' required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>

                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12'>Tanggal Lahir <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input id='tanggal_lahir' name='tanggal_lahir'  class='date-picker form-control col-md-7 col-xs-12' required='required' type='date'>
                            </div>
                          </div>

                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIK 
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='nik' maxlength='16' name='nik'  class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NIP
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='nip' maxlength='18' name='nip'  class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>NPWP
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='npwp' maxlength='18' name='npwp' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>

                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Golongan <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                            <select id='golongan' class='select2_single form-control' name='golongan' tabindex='-1'>
                             
                              <option>IIIA</option>
                              <option>IIIB</option>
                              <option>IIIC</option>
                              <option>IIID</option>
                              <option>IVA</option>
                              <option>IVB</option>
                              <option>IVC</option>
                              <option>IVD</option>
                              <option>IVE</option>
                            </select>
                            </div>
                          </div>
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Pangkat <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='pangkat' name='pangkat' required='required' readonly class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                         
                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='jabatan' name='jabatan' required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='form-group'>
                          <label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
                          <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select id='id_instansi' multiple="multiple" class='js-example-basic-multiple form-control' name='id_instansi' tabindex='-1'>
                              @foreach ($instansis as $instansi)
                                <option  value='{{ $instansi->id }}' >{{ $instansi->nama_instansi }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>          
                         
                         

                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Nomor Telepon <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='number' id='nomor_telepon' name='nomor_telepon'  required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>

                          <div class='form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Email <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='text' id='email' name='email' required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Asesor</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="jenis_asesor" id="jenis_asesor" name="jenis_kelamin " tabindex="-1">                          
                                  <option  value="Eksternal">Eksternal</option>
                                  <option  value="Internal">Internal</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat-lahir">Aktif Sampai <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" id="masa_berlaku" name="masa_berlaku" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status Keaktifan</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="is_active" id="is_active" name="is_active " tabindex="-1">                          
                                  <option  value="1" >Aktif</option>
                                  <option  value="0" >Non-Aktif</option>
                              </select>
                            </div>
                          </div>
                          <div class="ln_solid"></div>

                          <div class="ln_solid"></div>
                      <h2>Data Pekerjaan</h2>
                      <div class="ln_solid"></div>

                      <div id='pekerjaan-container'>
                     
                          <div id='row-pekerjaan1'>
                          <div class='col-md-2'></div>
                                <div class='col-md-10'>
                                <strong><h4>Pekerjaan 1</h4></strong><br>
                                </div>
                            <div class='form-group'>
                              <label class='control-label col-md-3 col-sm-3 col-xs-12'>Nama Instansi<span class='required'>*</span></label>
                              <div class='col-md-6 col-sm-6 col-xs-12'>
                                <select id='id_instansi1' class='select2_single form-control' name='id_instansi1' tabindex='-1'>
                                  @foreach ($instansis as $instansi)
                                    <option  value='{{ $instansi->id }}'>{{ $instansi->nama_instansi }}</option>
                                  @endforeach
                                </select>
                              </div>
                              </div>           
                              <div class='form-group'>
                                  <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Jabatan <span class='required'>*</span>
                                  </label>
                                  <div class='col-md-6 col-sm-6 col-xs-12'>
                                      <input type='text' id='jabatan_prev1'  name='jabatan_prev1' required='required' class='form-control col-md-7 col-xs-12'>
                                  </div>
                              </div>
                              <div class='form-group'>
                                  <label class='control-label col-md-3 col-sm-3 col-xs-12' for='tempat-lahir'>Periode <span class='required'>*</span>
                                  </label>
                                  <div class='col-md-6 col-sm-6 col-xs-12'>
                                      <input type='text' id='periode1' name='periode1'  required='required' class='form-control col-md-7 col-xs-12'>
                                  </div>
                              </div>
                      
                          </div>
                        </div>
                        <br>
                          <div class='col-md-3'>
                            <h3></h3>
                            </div>
                            <div class='col-md-9'>
                            <button class='btn btn-primary' id='add_pekerjaan'>Tambah Data Pekerjaan</button>
                            </div>
                            <br><br>
                      
                      <div class="ln_solid"></div>


                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Simpan Data</button>
                        </div>
                      </div>
                        </form>

                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <!-- jQuery -->
        <script src='/vendors/jquery/dist/jquery.min.js'></script>
        <!-- Bootstrap -->
        <script src='/vendors/bootstrap/dist/js/bootstrap.js'></script>
        <script src='/build/js/custom.js'></script>
        <!-- jQuery Smart Wizard -->
        <script src='/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>
        <!-- Custom Theme Scripts -->
        <script src='/build/js/asesor-new.js'></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
        <script>
          $(document).ready(function(){
            $('.js-example-basic-multiple').select2({
              maximumSelectionLength: 1,
              allowClear: true
            });
            jQuery('select[name="golongan"]').on('change',function(){
              switch ($('#golongan').val()) {
                case 'IA':
                  $('#pangkat').val('Juru Muda');
                  break;
                case 'IB':
                  $('#pangkat').val('Juru Muda Tingkat I');
                  break;
                case 'IC':
                  $('#pangkat').val('Juru');
                  break;
                case 'ID':
                  $('#pangkat').val('Juru Tingkat I');
                  break;
                case 'IIA':
                  $('#pangkat').val('Pengatur Muda');
                  break;
                case 'IIB':
                  $('#pangkat').val('Pengatur Muda Tingkat I');
                  break;
                case 'IIC':
                  $('#pangkat').val('Pengatur');
                  break;
                case 'IID':
                  $('#pangkat').val('Pengatur Tingkat I');
                  break;
                case 'IIIA':
                  $('#pangkat').val('Penata Muda');
                  break;
                case 'IIIB':
                  $('#pangkat').val('Penata Muda Tingkat I');
                  break;
                case 'IIIC':
                  $('#pangkat').val('Penata');
                  break;
                case 'IIID':
                  $('#pangkat').val('Penata Tingkat I');
                  break;
                case 'IVA':
                  $('#pangkat').val('Pembina');
                  break;
                case 'IVB':
                  $('#pangkat').val('Pembina Tingkat I');
                  break;
                case 'IVC':
                  $('#pangkat').val('Pembina Utama Muda');
                  break;
                case 'IVD':
                  $('#pangkat').val('Pembina Utama Madya');
                  break;
                case 'IVE':
                  $('#pangkat').val('Pembina Utama');
                  break;  
              }
            });
            $('#nomor_registrasi').blur(function(){
							var error_instansi = '';
							var nomor_registrasi = $('#nomor_registrasi').val();
						
							$.ajax({
								url:"/dashboard/registrasiCheck",
								method:"POST",
								data:{nomor_registrasi:nomor_registrasi},
								beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));},
								success:function(result)
								{
								console.log(result);
								if(result == 'unique')
								{
									$('#error_instansi').html('<label class="text-success">Nama Registrasi Unik </label>');
									$('#nomor_registrasi').removeClass('has-error');
									$('#submit').attr('disabled', false);
								}
								else
								{
									$('#error_instansi').html('<label class="text-danger">Nama Registrasi Tidak Unik </label>');
									$('#nomor_registrasi').addClass('has-error');
									$('#submit').attr('disabled', 'disabled');
								}
								}
							})
						
						});
          });
        </script>
@endsection