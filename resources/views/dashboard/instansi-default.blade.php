@extends('dashboard.layout')
@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Masukan Data Instansi</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					@if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
					@endif
					@if ($message = Session::get('error'))
						<div class="alert alert-danger">
							<p>{{ $message }}</p>
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <br />
                    <form method="POST" action="{{ route('instansi.store') }}"  id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
						@csrf
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Instansi <span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" id="nama_instansi" name="nama_instansi" required="required" class="form-control col-md-7 col-xs-12">
								<span id="error_instansi"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat <span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<textarea rows="2" cols="50" id="last-name" name="alamat" required="required" class="form-control col-md-7 col-xs-12"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Provinsi<span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="select2_single form-control" name="id_provinsi" tabindex="-1">
									<option value="-">-PILIH-</option>
									@foreach ($provinsis as $provinsi)
										<option  value="{{ $provinsi->id }}">{{ $provinsi->nama_provinsi }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Kota<span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="select2_single form-control" name="id_kota" id='id_kota' tabindex="-1"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email <span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" id="last-name" name="email" required="required" class="form-control col-md-7 col-xs-12">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Telepon <span class="required">*</span></label>
							<div class="col-md-1 col-sm-1 col-xs-12">
								<input type="text" id="last-name" name="kode_area" placeholder="Kode Area" required="required" class="form-control col-md-7 col-xs-12">
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12">
								<input type="text" id="last-name" name="nomor_telepon" required="required" class="form-control col-md-7 col-xs-12">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode Pos <span class="required">*</span></label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" id="last-name" name="kode_pos" required="required" class="form-control col-md-7 col-xs-12" maxlength="5">
							</div>
						</div>
                    </div>                     
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button class="btn btn-danger" type="button">Batalkan</button>
							<button class="btn btn-primary" type="reset">Reset</button>
							<button type="submit" class="btn btn-success" id="submit">Simpan Data</button>
                        </div>
					</div>
					</form>
                </div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">          
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Instansi</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#1</th>
								<th>Nama Instansi</th>
								<th>Alamat</th>
								<th>Email</th>
								<th>Nomor Telepon</th>
								<th>Kode Pos</th>
								<th>Kota</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($instansis as $instansi)
								<tr>
									<td>{{ ++$loop->index }}</td>
									<td>{{ $instansi->nama_instansi }}</td>
									<td>{{ $instansi->alamat }}</td>
									<td>{{ $instansi->email }}</td>
									<td>{{ $instansi->nomor_telepon }}</td>
									<td>{{ $instansi->kode_pos }}</td>
									<td>{{ $instansi->nama_kota }}</td>
									<td>
										<a class="btn btn-primary" href="{{ route('instansi.edit',$instansi->id) }}">Edit</a>
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
											Hapus 
										</button>
									</td>
								</tr>
							@endforeach
						</tbody>
                    </table>
                    <!-- The Modal -->
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
							<div class="modal-content">
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title">Konfirmasi</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<!-- Modal body -->
								<div class="modal-body">
									<h5>Apakah anda yakin ingin menghapus?</h5>
								</div>
								<!-- Modal footer -->
								<div class="modal-footer">
									@if($count>0)
										<form action="{{ route('instansi.destroy',$instansi->id) }}" method="POST">
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger">Delete</button>
										</form>
									@endif
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>

<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>

<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>

<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="/build/js/custom.min.js"></script>
<script>
	$(document).ready(function(){
		$( "input[name='kode_pos']" ).keypress(function(e){
			if(e.charCode < 48 || e.charCode > 57) {
				return false;
			}
		});
		
		$( "input[name='nomor_telepon']" ).keypress(function(e){
			if(e.charCode < 48 || e.charCode > 57) {
				return false;
			}
		});
	
        $('#nama_instansi').blur(function(){
			  var error_instansi = '';
			  var nama_instansi = $('#nama_instansi').val();
			 
			  $.ajax({
				url:"/dashboard/instansiCheck",
				method:"POST",
				data:{nama_instansi:nama_instansi},
				beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));},
				success:function(result)
				{
				 
				if(result == 'unique')
				{
				  $('#error_instansi').html('<label class="text-success">Nama Instansi Tersedia</label>');
				  $('#nama_instansi').removeClass('has-error');
				  $('#submit').attr('disabled', false);
				}
				else
				{
				  $('#error_instansi').html('<label class="text-danger">Nama Instansi Sudah Ada</label>');
				  $('#nama_instansi').addClass('has-error');
				  $('#submit').attr('disabled', 'disabled');
				}
				}
			  })        
        });
		
        jQuery('select[name="id_provinsi"]').on('change',function(){
               var countryID = jQuery(this).val();
               $('#id_kota').attr("disabled", false);
               $('#submit').attr("disabled", false);
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/dashboard/getkota/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="id_kota"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="id_kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="id_kota"]').empty();
               }
            });
            if($('#id_kota').val() == null){
              $('#submit').attr("disabled", true);
              $('#id_kota').attr("disabled", true);
            }
      });       
</script>
@endsection